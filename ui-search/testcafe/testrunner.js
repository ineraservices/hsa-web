const config = require('./config').config
const createTestCafe = require('testcafe');
const fs = require('fs');
const REPORT_DIR = './reports';
const SCREENSHOT_DIR = `${REPORT_DIR}/screenshots`;
fs.stat(REPORT_DIR, (_, stat) => stat || fs.mkdirSync(SCREENSHOT_DIR, { recursive: true }));

let testcafe = null;

createTestCafe('localhost', 1337, 1338)
  .then(tc => {
    testcafe = tc;
    let runner;
    if (config.live) {
      runner = testcafe.createLiveModeRunner();
    } else {
      runner = testcafe.createRunner();
    }

    console.log('========= Test configuration ==========')
    console.log(config);
    console.log('=======================================')

    const reportStream = fs.createWriteStream(`${REPORT_DIR}/report.xml`);

    return runner
      .browsers([config.browser])
      .src(['testcafe/*.test.ts'])
      .filter((testName, fixture) => testName.includes(config.testName) && fixture.includes(config.testFixture))
      .reporter(['spec', {
        name: 'xunit',
        output: reportStream
      }, {
        name: 'html',
        output: `${REPORT_DIR}/index.html`
      }])
      .screenshots(SCREENSHOT_DIR, true)
      .run();
  })
  .then(failedCount => {
    console.log('Tests failed: ' + failedCount);
    testcafe.close();
  });
