let config = {
  // Property: Default value
  baseUrl: 'http://localhost:8080',
  browser: 'chrome',
  testName: '', // All test names that include this value will be run
  testFixture: '', // All test fixtures that include this value will be run
  debug: false, // Print debug stuff
  live: false // Testcafé live mode.
}

function removeTrailingSlash(str) {
  return str.lastIndexOf('/') === str.length - 1 ? str.substring(0, str.lastIndexOf('/')) : str
}

function getArg(arg) {
  return process.argv.filter(a => a.startsWith(`--${arg}=`))
    .map(a => a.substr(arg.length + 3)) // Size of argument
    .map(removeTrailingSlash)
    .pop();
}

config = Object.keys(config).reduce((acc, curr) => {
  acc[curr] = getArg(curr) || config[curr];
  return acc;
}, {})

module.exports = { config }
