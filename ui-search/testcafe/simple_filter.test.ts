import Page from './pages/SimpleSearch';
import Filter from './pages/Filter';
import ResultView, { ObjectCategory } from './pages/ResultView'
import { expectExactlyOne, expectNo, expectAll, expectSameResultAgain } from './helpers/expect_helpers'
import { matchesName, matchesCounty} from './domain/rules';
import { uncheck, checkbox, check} from './helpers/element_helper';
import { Parent, createUnit, createEmployee, revertApiCalls } from './testapi/orgapi';

const page = new Page();
const resultView = new ResultView()
const filter = new Filter();
let tussilago: Parent = { county: 'Norrbottens län', org: 'Tussilagon' };

/**
 * Testning av simpel sök-gränssnittet och dess möjligheter att filtrera.
 * interaktion från användaren. Detta betyder inte uttömmande testning av fritext-APIt.
 **/
fixture `SOK-005: Enkel sök: Filter`
  .page(page.URL)
  .afterEach(revertApiCalls);

/**
 * FSOK-001: Filtrera sökning menyn ska gå att nå via flera olika knappar
 **/
test('FSOK-001: Filtrera sökning menyn ska gå att nå via flera olika knappar', async t => {
  //Test to open left sidebar filter view by click on arrow to the left in main window.
  await t.click(page.sidebarToggler)
  .expect(checkbox(Filter.EXACT_SEARCH_LABEL).visible).ok()
  .click(page.sidebarToggler)
  .expect(checkbox(Filter.EXACT_SEARCH_LABEL).exists).notOk()

  //Test to open left sidebar filter view by click on "Filtrera" below search button.
  .click(page.filtreraText)
  .expect(checkbox(Filter.EXACT_SEARCH_LABEL).visible).ok()
  .click(page.filtreraText)
  .expect(checkbox(Filter.EXACT_SEARCH_LABEL).exists).notOk()

  //Test to open left sidebar filter view by click on "Symbol" to teh left of search field.
  .click(page.filterSymbol)
  .expect(checkbox(Filter.EXACT_SEARCH_LABEL).visible).ok()
  .click(page.filterSymbol)
  .expect(checkbox(Filter.EXACT_SEARCH_LABEL).exists).notOk()
});

/**
 * FSOK-002: Sökområde ska kunna väljas genom att bläddra i listan 
 **/
// ska enablas efter HSA-666 är klar
test.skip.before(async _ => {
  await createUnit(tussilago, { name: 'Nordic Unit' });
})('FSOK-002: Sökområde ska kunna väljas genom att bläddra i listan', async t => {
  await t.click(page.sidebarToggler);
  await filter.selectSearchAreaByScrolling('Norrbottens län');
  await t.typeText(page.searchInput, 'Nordic Unit')
    .click(page.sokButton)
    .click(resultView.orgUnitTab);

  let results = await resultView.getResults();
  await expectAll(results, matchesCounty, 'Norrbottens län');
  await t.expect(resultView.searchArea.innerText).eql('Sverige » Norrbottens län')
    .expect(resultView.orgUnitTab.itemCount).eql(results.length)
    .expect(resultView.hitsLabel.itemCount).eql(results.length);
});

/**
 * FSOK-002: Sökområde ska kunna väljas genom att användaren 
 * skriver i sökrutan
 **/
// ska enablas efter HSA-666 är klar
test.skip.before(async _ => {
  await createUnit(tussilago, { name: 'Nordic Unit' });
})('FSOK-002: Sökområde ska kunna väljas genom att skriva', async t => {
  await t.click(page.sidebarToggler);
  await filter.selectSearchAreaByTyping('Norrbottens län');
  await t.typeText(page.searchInput, 'Nordic Unit')
    .click(page.sokButton)
    .click(resultView.orgUnitTab);

  let results = await resultView.getResults();
  await expectAll(results, matchesCounty, 'Norrbottens län');
  await t.expect(resultView.searchArea.innerText).eql('Sverige » Norrbottens län')
    .expect(resultView.orgUnitTab.itemCount).eql(results.length)
    .expect(resultView.hitsLabel.itemCount).eql(results.length);
});

/**
 * FSOK-002: Det ska vara möjligt att filtrera på sökområde.
 * Enheter ska dyka upp förs efter att 3 bokstäver dykt upp
 **/
test.before(async _ => {
  await createUnit(tussilago, { name: 'xyz enhet 1' });
  await createUnit(tussilago, { name: 'xyz enhet 2' });
})('FSOK-002: Enheter ska dyka upp i sökområdet först efter att 3 bokstäver skrivits', async t => {
  await t.click(page.sidebarToggler);
  await filter.openSearchAreaSelector();
  let options = await filter.getSearchAreaOptions();

  // Innan man skrivit in något ska enheterna inte dyka upp
  await expectNo(options, matchesName, 'Norrbottens län » Tussilagon » xyz enhet');

  await filter.typeSearchArea('xyz');
  options = await filter.getSearchAreaOptions();
  await expectExactlyOne(options, matchesName, 'Norrbottens län » Tussilagon » xyz enhet 1');
  await expectExactlyOne(options, matchesName, 'Norrbottens län » Tussilagon » xyz enhet 2');
  await expectAll(options, matchesName, 'Norrbottens län » Tussilagon » xyz enhet');
});

/**
 * FSOK-003: Det ska vara möjligt att filtrera sökning på typ av objekt. I detta test person
 * SOK-007: Som användare ska jag kunna spara sökningar för att använda igen
 * Genom att spara undan URL'en när man fyllt i sökfälten kan man spara ett bookmark med 
 * sökinformationen. Både det som skrivits i sökfältet och de alternativ som kryssats i
 * filtret ska följa med i urlen 
 **/
test.before(async _ => {
  await createEmployee(tussilago, { name: 'Gondar' });
  await createUnit(tussilago, { name: 'Vårdenhet viol' });
})('FSOK-003: Filtrera på objekttyper - Person', async t => {
  await t.click(page.sidebarToggler);
  await uncheck(checkbox(Filter.COMMISSION_LABEL));
  await uncheck(checkbox(Filter.ORG_UNIT_LABEL));

  await t.typeText(page.searchInput, 'Tussilago')
    .click(page.sokButton)
    .click(resultView.personTab);

  let results = await resultView.getResults(ObjectCategory.Person);
  await t.expect(resultView.personTab.itemCount).eql(results.length)
    .expect(resultView.hitsLabel.itemCount).eql(results.length)
    .expect(resultView.orgUnitTab.itemCount).eql(0)
    .expect(resultView.commissionTab.itemCount).eql(0);
  //Repetera sökningen baserad på URL:en, med all sökinformation inkluderad
  await expectSameResultAgain(await t.eval(() => window.location.href), results);
});

/**
 * FSOK-003: Det ska vara möjligt att filtrera sökning på typ av objekt. I detta test Organisation eller enhet
 * SOK-007: Som användare ska jag kunna spara sökningar för att använda igen
 * Genom att spara undan URL'en när man fyllt i sökfälten kan man spara ett bookmark med 
 * sökinformationen. Både det som skrivits i sökfältet och de alternativ som kryssats i
 * filtret ska följa med i urlen 
 **/
test.before(async _ => {
  await createEmployee(tussilago, { name: 'Gondar' });
  await createUnit(tussilago, { name: 'Vårdenhet viol' });
})('FSOK-003: Filtrera på objekttyper - Organisation eller enhet', async t => {
  await t.click(page.sidebarToggler);
  await uncheck(checkbox(Filter.COMMISSION_LABEL));
  await uncheck(checkbox(Filter.PERSON_LABEL));

  await t.typeText(page.searchInput, 'Tussilago')
    .click(page.sokButton)
    .click(resultView.orgUnitTab);

  let results = await resultView.getResults(ObjectCategory.OrgEllerEnhet);
  await t.expect(resultView.orgUnitTab.itemCount).eql(results.length)
    .expect(resultView.hitsLabel.itemCount).eql(results.length)
    .expect(resultView.personTab.itemCount).eql(0)
    .expect(resultView.commissionTab.itemCount).eql(0);

  //Repetera sökningen baserad på URL:en, med all sökinformation inkluderad
  await expectSameResultAgain(await t.eval(() => window.location.href), results);
});
/**
 * FSOK-003: Det ska vara möjligt att filtrera sökning på typ av objekt. I detta test Medarbetaruppdrag
 * SOK-007: Som användare ska jag kunna spara sökningar för att använda igen
 **/
test.skip.before(async _ => {
})('FSOK-003, SOK-007: Filtrera på objekttyper - Medarbetaruppdrag, Ge användaren möjlighet att spara sökning', async _ => {

});

/**
 * FSOK-004: Det ska vara möjligt att filtrera sökning på exakt stavning
 * SOK-007: Som användare ska jag kunna spara sökningar för att använda igen
 * Genom att spara undan URL'en när man fyllt i sökfälten kan man spara ett bookmark med 
 * sökinformationen. Både det som skrivits i sökfältet och de alternativ som kryssats i
 * filtret ska följa med i urlen 
 **/
test.before(async _ => {
  await createUnit(tussilago, { name: 'Vårdenhet viol' });
  await createUnit(tussilago, { name: 'Violas barnsjukhus' });
})
('FSOK-004, SOK-007: Sökning med "Exakt sökning" ska endast returnera svar med hela ord, Ge användaren möjlighet att spara sökning', async t => {
  await t.click(page.sidebarToggler);
  await check(checkbox(Filter.EXACT_SEARCH_LABEL));
  await t.typeText(page.searchInput, 'Viol')
    .click(page.sokButton)
    .click(resultView.orgUnitTab);

  let results = await resultView.getResults();
  await expectNo(results, matchesName, 'Viola');
  await expectExactlyOne(results, matchesName, 'Vårdenhet viol');
  //Spara URL från sökningen, vilken kommer att innehålla sökparametrar och vara möjlig 
  //att spara som bokmärke i din webläsare
  var searchQuery_url = await t.eval(() => window.location.href);
  //Kontrollera resultatet från sökningen vilken borde innehålla några få träffar på Viol
  await t.expect(results.length).notEql(0);
  await expectAll(results, matchesName, 'Viol');
 
  //Repetera sökningen baserad på URL:en, med all sökinformation inkluderad
  await expectSameResultAgain(searchQuery_url, results);
  await t.expect(searchQuery_url).contains('exactMatch=true','the url contains exactMatch=true')
    .click(page.sidebarToggler)
    .expect(checkbox(Filter.EXACT_SEARCH_LABEL).checked).ok();
});
