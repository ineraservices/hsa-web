import { IResult, IName } from "../helpers/custom_selectors";

/**
 * A Rule is a function that returns true if a given
 * item follows the Rule, otherwise false
 */
export type Rule<T> = (r: T) => boolean

/**
 * Checks if a string is present either in name or path of the Result.
 * @param str
 */
export function matchesNameOrPath(...str: string[]): Rule<IResult> {
  return (r) => str.every(s =>
    r.name.toLowerCase().includes(s.toLowerCase()) ||
    r.path.toLowerCase().includes(s.toLowerCase()));
}

/**
 * Checks if the path matches a county, by checking if the path
 * starts with the given string.
 * @param str 
 */
export function matchesCounty(str: string): Rule<IResult> {
  return (r) => r.path.toLowerCase().startsWith(str.toLowerCase());
}

/**
 * Checks if the path contains an organization, by matching the given
 * string against the first or second segment of the path.
 * @param str 
 */
export function matchesOrganization(str: string): Rule<IResult> {
  return (r) => r.path.toLowerCase().startsWith(str.toLowerCase()) ||
    r.path.substring(r.path.indexOf('»') + 1).trim().toLowerCase().startsWith(str.toLowerCase());
}

/**
 * Checks if a given string is present in the name of the Result
 * @param str 
 */
export function matchesName(str: string): Rule<IName> {
  return (r) => r.name.toLowerCase().includes(str.toLowerCase());
}

/**
 * Checks if a given string is present in the path of the Result
 * @param str 
 */
export function matchesPath(str: string): Rule<IResult> {
  return (r) => r.path === str;
}
