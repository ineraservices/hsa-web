let fs = require('fs');
let path = require('path');
let testcafe = require('testcafe');

let ClientFunction = testcafe.ClientFunction;

let AXE_DIR_PATH = path.dirname(require.resolve('axe-core'));
let AXE_SCRIPT = fs.readFileSync(path.join(AXE_DIR_PATH, 'axe.min.js')).toString();

function AxeError(message) {
  Error.call(this, message);

  this.name = 'AxeError';
  this.message = message;

  if (typeof Error.captureStackTrace === 'function') {
    Error.captureStackTrace(this, AxeError);
  } else {
    this.stack = (new Error(message)).stack;
  }
}

AxeError.prototype = Object.create(Error.prototype);

let hasAxe = ClientFunction(() => !!(window.axe && window.axe.run));

let injectAxe = ClientFunction(function () {
  eval(AXE_SCRIPT);
}, { dependencies: { AXE_SCRIPT: AXE_SCRIPT } });

let runAxe = ClientFunction(function (context, options) {
  return new Promise(function (resolve) {
    //Default körs alla regler för AXE
    axe.run(context || document, options || {}, function (err, results) {
      if (err)
        return resolve(err.message);

      let errors = '';

      if (results.violations.length !== 0) {
        results.violations.forEach(function (violation) {
          errors += violation.help + '\n\tnodes:\n';

          violation.nodes.forEach(function (node) {
            let targetNodes = node.target.map(function (target) {
              return '"' + target + '"';
            }).join(', ');

            errors += '\t\t' + targetNodes + '\n';
          });
        });
      }

      return resolve(errors);
    });
  })
});

export default function axeCheck(t, context, options) {
  return hasAxe.with({ boundTestRun: t })()
    .then(function (result) {
      if (!result)
        return injectAxe.with({ boundTestRun: t })();

      return Promise.resolve();
    })
    .then(function () {
      return runAxe.with({ boundTestRun: t })(context, options);
    })
    .then(function (error) {
      if (error)
        throw new AxeError('\n' + error);
    });
};
