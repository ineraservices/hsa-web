# TestCafé test suite

Folder structure of the test suite

```
testcafe
├── axe           // Adoptation of AXE to TestCafe for WCAG checks
├── domain        // Logic and constants deriving from HSA requirements
├── helpers       // General helper functions useful to any test
├── pages         // Representations of the different pages (PageObject pattern)
├── testapi       // APIs to modify test data
├── *.test.ts     // Top level test suites
├── testrunner.js // The TestCafé test runner
├── tsconfig.json // Configuration for TypeScript
└── config.js     // Test configuration module (target url etc)
```

### Developing tests

Before developing tests, read through the "Getting Started" guide on the TestCafé website: https://devexpress.github.io/testcafe/documentation/getting-started/. Always keep the API reference at handy when developing.
med
Visual Studio Code is our recommended IDE for TestCafé development, as its TypeScript support is very good. WebStorm is probably good as well. Install extensions for **TypeScript, Javascript and TSLint** as well as **Prettier - Code Formatter**. Use **Prettier** with the configuration called "Javascript and Typescript Language Features". And use it!

#### Test case design

The test cases are located at test root level in files with the pattern *.test.ts.

A test case should be kept short, clean, concise and refer directly to a requirement. It should only contain language that is available to a user familiar with HSA. That means any implementation details, like internal structure of the web page or logic not obvious to such a user has to be hidden by abstractions.

For example:
```
t.click(Selector('#button1'))
```
HTML element attributes are internal to the web page structure, and should be abstracted using a Page Object as such:

```
t.click(page.searchButton)
```

##### TestCafé live

TestCafé can be setup to automatically listen to changes in the test source files, and rerun tests accordingly. To enable this, run testcafe as such:

```
npm run testcafe -- --live=true
```

Can be used together with `--testName` to limit the amount of tests watched.

##### Test data

Test data should as much as possible be prepared and cleaned up by the tests themselves. Use `.before()` and `.after()` on tests and fixtures for this. To modify data, use the organization api located in the `testapi`-folder.

##### Debug information

If the `--debug=true` flag is set, the test suite should print additional information. Use the following code:

```
if (config.debug) {
  console.debug('...');
}
```

Try to keep blocks like these in helper methods, away from the top level test cases

##### Skipping tests

Tests may only be marked as skipped if the feature it tests is covered by a JIRA, either bug or feature to be implemented. It can be a good idea to comment the test with the issue ID.

#### Selecting objects

A general rule of thumb is that Selectors should be designed so they are unlikely to break from changes to the website structure or visual appearance. For Selecting single objects, an ID is preferred:

```
Selector('#searchField')
```

Classes are OK to use in selectors if they describe WHAT the object is in a domain-meaningful way, not HOW it looks:

```
Selector('.result-item')   // This is fine. Regardless of styling, result items will 
                           // still be result items and can be refered to from requirements.
Selector('.large-border')  // Will break when design changes to a small border
Selector('.div-separator') // Though it describes WHAT it is, it is still not good since 
                           // it refers to a structural element that is not meaningful
                           // in a requirement-context
```

Don't be clever:
```
Selector('a > ul > li[attr=text] a') // Nope. This will break sooner rather than later.
```
Don't be afraid to add or modify the website solely for testing purposes to be able to Select the elements you need.
