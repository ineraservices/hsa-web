export interface IUnit {
  name: string;
  type?: string;
  hsaIdentity?: string;
  c?: string; //(countryName)
  countyCode?: string;
  description?: string;
  displayOption?: string;
  endDate?: string;
  /**
   * Geografiska koordinater (enligt RT90) som anger enhetens fysiska placering.
   * Exempel på en koordinat i Stockholm (Djurgårdsbron) är?: "X?:6581164,Y?:1630250";
   */
  geographicalCoordinates?: any;
  /**
   * Geografiska koordinater enligt SWEREF 99 TM som anger enhetens fysiska placering.
   * Exempel på en koordinat i Linköping (E4-bron över Stångån) är?: N?:6477155,E?:536352;
   */
  geographicalCoordinatesSweref99TM?: any;
  hsaAdminComment?: string;
  hsaAltText?: string;
  hsaDestinationIndicator?: string;
  hsaDirectoryContact?: string;
  hsaHealthCareArea?: string;
  hsaHealthCareUnitManager?: string;
  hsaJpegLogotype?: string;
  hsaResponsibleHealthCareProvider?: string;
  hsaSwitchboardNumber?: string;
  hsaVisitingRuleReferral?: string;
  hsaVisitingRules?: string;
  hsaVpwInformation1?: string;
  hsaVpwInformation2?: string;
  hsaVpwInformation3?: string;
  hsaVpwInformation4?: string;
  hsaVpwWebPage?: string;
  jpegPhoto?: string;
  labeledURI?: string;
  localityName?: string; // (l);;

  mail?: string;
  municipalityCode?: string;
  orgNo?: string;
  postalAddress?: IPostalAdress;
  postalCode?: string;
  route?: string;
  smsTelephoneNumber?: string;
  startDate?: string;
  street?: string // (streetAddress);;

  businessClassificationCode?: string[];
  careType?: string[];
  dropInHours?: any[];
  facsimileTelephoneNumber?: string[];
  financingOrganization?: string[];
  hsaBusinessType?: string[];
  hsaHealthCareUnitMember?: string[];
  hsaSyncId?: string[];
  hsaTelephoneNumber?: string[];
  hsaTextTelephoneNumber?: string[];
  hsaVisitingRuleAge?: string[];
  hsaVpwNeighbouringany?: string[];
  management?: string[];
  mobile?: string[]; //(mobileTelephoneNumber);;
  ouShort?: string[]; // alternative name/s;
  pager?: string[]; //(pagerTelephoneNumber);;
  seeAlso?: string[];
  surgeryHours?: any[];
  telephoneHours?: any[];
  telephoneNumber?: string[];
  unitPrescriptionCode?: string[];
  visitingHours?: any[];
  
  // if true then organizationNumber (orgNo) is required
  hsaHealthCareProvider?: boolean;
  hsaHealthCareUnit?: boolean;
}

export interface IOrganization {
  name: string;
  type?: string;

  hsaHpt?: string;

  hsaIdentity?: string;
  mail?: string; //(rfc822Mailbox);;


  orgNo?: string;

  postalAddress?: IPostalAdress[];

  telephoneNumber?: string;

  archivingTimePerson?: string;
  c?: string; // (countryName);;
  countyCode?: string;
  description?: string;
  displayOption?: string;
  endDate?: string;
  geographicalCoordinates?: any[];
  geographicalCoordinatesSweref99TM?: any[];

  hsaAdminComment?: string;
  hsaAltText?: string;
  hsaDestinationIndicator?: string;
  hsaDirectoryContact?: string;
  hsaHealthCareArea?: string;
  hsaHealthCareUnitManager?: string;
  hsaIdCounter?: string;
  hsaIdPrefix?: string;
  hsaJpegLogotype?: string;
  hsaResponsibleHealthCareProvider?: string;
  hsaSwitchboardNumber?: string;
  hsaVisitingRuleReferral?: string;
  hsaVisitingRules?: string;
  hsaVpwInformation1?: string;
  hsaVpwInformation2?: string;
  hsaVpwInformation3?: string;
  hsaVpwInformation4?: string;
  hsaVpwWebPage?: string;

  jpegPhoto?: string;
  localityName?: string; // (l)
  labeledURI?: string;
  municipalityCode?: string;
  postalCode?: string;
  route?: string;
  smsTelephoneNumber?: string;
  startDate?: string;
  street?: string; //(streetAddress)

  //Multivalue attributes
  businessClassificationCode?: string[];
  careType?: string[];
  dropInHours?: any[];
  facsimileTelephoneNumber?: string[];
  financingOrganization?: string[];
  hsaBusinessType?: string[];
  hsaHealthCareUnitMember?: string[];
  hsaSyncId?: string[];
  hsaTelephoneNumber?: string[];
  hsaTextTelephoneNumber?: string[];
  hsaVisitingRuleAge?: string[];
  hsaVpwNeighbouringany?: string[];
  management?: string[];
  mobile?: string[]; //(mobileTelephoneNumber)
  ouShort?: string[];
  pager?: string[]; // (pagerTelephoneNumber)
  seeAlso?: string[];
  surgeryHours?: any[];
  telephoneHours?: any[];
  unitPrescriptionCode?: string[];
  visitingHours?: any[];

  hsaHealthCareProvider?: boolean;
}

export interface IHSAFunction {
  name: string;
  type?: string;

  hsaIdentity?: string;

  c?: string;  // (countryName)
  countyCode?: string;

  description?: string;
  displayOption?: string;

  endDate?: string;

  geographicalCoordinates?: any[];
  geographicalCoordinatesSweref99TM?: any[];

  hsaAdminComment?: string;
  hsaAltText?: string;
  hsaDestinationIndicator?: string;
  hsaHealthCareArea?: string;
  hsaJpegLogotype?: string;
  hsaSwitchboardNumber?: string;
  hsaVisitingRuleReferral?: string;
  hsaVisitingRules?: string;
  hsaVpwInformation1?: string;
  hsaVpwInformation2?: string;
  hsaVpwInformation3?: string;
  hsaVpwInformation4?: string;
  hsaVpwWebPage?: string;

  jpegPhoto?: string;

  localityName?: string;  //(l);
  labeledURI?: string;

  mail?: string;  // (rfc822Mailbox)
  municipalityCode?: string;

  orgNo?: string;
  ou?: string;  // (organizationalUnitName)
  postalAddress?: IPostalAdress;
  postalCode?: string;
  route?: string;
  smsTelephoneNumber?: string;
  startDate?: string;
  street?: string;  // (streetAddress);

  businessClassificationCode?: string[];
  careType?: string[];
  dropInHours?: any[];
  facsimileTelephoneNumber?: string[];
  financingOrganization?: string[];
  hsaBusinessType?: string[];
  hsaSyncId?: string[];
  hsaTelephoneNumber?: string[];
  hsaTextTelephoneNumber?: string[];
  hsaVisitingRuleAge?: string[];
  hsaVpwNeighbouringany?: string[];
  management?: string[];
  mobile?: string[];  //(mobileTelephoneNumber)
  pager?: string[];  //(pagerTelephoneNumber)
  seeAlso?: string[];
  surgeryHours?: any[];
  telephoneHours?: any[];
  telephoneNumber?: string[];
  unitPrescriptionCode?: string[];
  visitingHours?: any[];
}

export interface ICountry {
  isoCode: string;
  name: string;
  type: string;
}

export interface ICounty {
  name: string;
  type: string;
  description: string;
  hsaIdCounter: string;
  hsaIdPrefix: string;
}

export interface IEmployee {
  type?: string;
  fullName?: string;
  hsaIdentity?: string;
  sn?: string;
  name?: string;

  personalIdentityNumber?: string;
  middleName?: string;
  userPrincipalName?: string;
  hsaPassportBirthDate?: string;
  personalPrescriptionCode?: string;
  hsaPassportValidThru?: string;
  hospIdentityNumber?: string;
  hsaAdminComment?: string;
  hsaPassportNumber?: string;
  hsaEmigratedPerson?: string;
  hsaProtectedPerson?: string;
  endDate?: string;
  startDate?: string;
  hsaVerifiedIdentityNumber?: string;
  hsaDestinationIndicator?: string;
  hsaAltText?: string;
  description?: string;
  street?: string;
  jpegPhoto?: string;
  hsaManagerCode?: string;
  mail?: string;
  initials?: string;
  postalAddress?: IPostalAdress;
  nickName?: string;
  givenName?: string;
  title?: string;
  hsaSwitchboardNumber?: string;
  serialNumber?: string[];
  telephoneNumber?: string[];
  hsaSosNursePrescriptionRight?: string[];
  validNotBefore?: string[];
  validNotAfter?: string[];
  hsaGroupPrescriptionCode?: string[];
  userCertificate?: string[];
  hsaSystemRole?: string[];
  hsaSyncId?: string[];
  cardNumber?: string[];
  hsaSosTitleCodeSpeciality?: string[];
  hsaTitle?: string[];
  hsaMifareSerialNumber?: string[];
  mobile?: string[];
  seeAlso?: string[];
  telephoneHours?: any[];
  hsaTextTelephoneNumber?: string[];
  hsaTelephoneNumber?: string[];
  occupationalCode?: string[];
  paTitleCode?: string[];
}

export interface ICode {
  code: string,
  name: string,
  codeDescription?: string
}

interface IPostalAdress {
  addressLine: string[]
}
