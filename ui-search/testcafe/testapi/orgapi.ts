import * as http from 'typed-rest-client/RestClient'
import { ICountry, IUnit, IHSAFunction, IEmployee } from './model';
import { config } from '../config.js';
import { randomHSAID } from '../helpers/random_data';

/* This is not pretty, but it is easy and it works. To make this pretty
 * we should modify the pipeline template and the testrunner-template to
 * accept arbitrary environment variables which will be available
 * in the test environment.
 */
let orgApiBaseUrl = 'http://localhost:9080';
if (config.baseUrl === 'http://hsa-web:8080') { // If running in a test pod
  orgApiBaseUrl = 'http://hsa-organization:9080';
} else if (config.baseUrl === 'https://hsa-web-dhsa.ind-ocpt1a-app.ocp.sth.basefarm.net') { // If running outside openshift
  orgApiBaseUrl = 'https://hsa-organization-dhsa.ind-ocpt1a-app.ocp.sth.basefarm.net';
}
const ORG_API_URL = `${orgApiBaseUrl}/organization/resources/v0.3`;

const orgApi = new http.RestClient('org-api');

let undoApiCalls: (() => Promise<any>)[] = [];

export interface Parent {
  county?: string;
  org: string;
  units?: string[];
}

export async function revertApiCalls() {
  // Chain promises together with reduce
  return undoApiCalls.reverse().reduce((prev, curr) => prev.then(curr), Promise.resolve())
    .then(() => undoApiCalls = []);
}

export function buildParent(parent: Parent, ...units: IUnit[]) {
  let newParent = { ...parent };
  newParent.units = units.reduce((acc, curr) => acc.concat(curr.name), newParent.units || []);
  return newParent;
}

export async function getCountry(name: string): Promise<http.IRestResponse<ICountry>> {
  return orgApi.get<ICountry>(`${ORG_API_URL}/countries/${name}`);
}

export async function createUnit(parent: Parent, unit: IUnit): Promise<http.IRestResponse<IUnit>> {
  unit = Object.assign({}, unit);

  //Default values
  unit.type = 'Unit';
  unit.hsaIdentity = unit.hsaIdentity || randomHSAID();

  let path = buildParentPath(parent) + '/units';
  path = encodeURI(path);
  if (config.debug) {
    console.log('============= CREATE UNIT =============');
    console.log('POST ' + ORG_API_URL + path);
    console.log(unit);
  }
  let response = await orgApi.create<IUnit>(ORG_API_URL + path, unit)
  if (config.debug) {
    console.log('RESPONSE CODE: ' + response.statusCode);
  }
  if (response.statusCode > 300) {
    throw Error(`POST ${path} failed response code: ${response.statusCode}`);
  }
  undoApiCalls.push(deleteUnit.bind(null, parent, unit));
  return response;
}

export async function deleteUnit(parent: Parent, unit: IUnit): Promise<http.IRestResponse<IUnit>> {
  let path = buildParentPath(parent);
  path += `/units/${unit.name}`;
  path = encodeURI(path);
  if (config.debug) {
    console.log('============= DELETE UNIT =============');
    console.log('DELETE ' + path);
  }
  let response = await orgApi.del<IUnit>(ORG_API_URL + path);
  if (config.debug) {
    console.log('RESPONSE CODE: ' + response.statusCode);
  }
  if (response.statusCode > 300) {
    throw Error(`DELETE ${path} failed response code: ${response.statusCode}`);
  }
  return response;
}

export async function createEmployee(parent: Parent, employee: IEmployee): Promise<http.IRestResponse<IEmployee>> {
  employee = Object.assign({}, employee);
  
  // Default values
  employee.type = 'Employee';
  employee.name = employee.name || employee.givenName;
  employee.hsaIdentity = employee.hsaIdentity || randomHSAID();
  employee.sn = employee.sn || 'Testsson';
  employee.fullName = employee.fullName || (employee.name + ' ' + (employee.sn ? employee.sn : 'Testsson'));
  
  let path = buildParentPath(parent) + '/employees';
  path = encodeURI(path);
  if (config.debug) {
    console.log('============= CREATE EMPLOYEE =============');
    console.log('POST ' + path);
    console.log(employee);
  }
  let response = await orgApi.create<IEmployee>(ORG_API_URL + path, employee);
  if (config.debug) {
    console.log('RESPONSE CODE: ' + response.statusCode);
  }
  if (response.statusCode > 300) {
    throw Error(`POST ${path} failed response code: ${response.statusCode}`);
  }
  undoApiCalls.push(deleteEmployee.bind(null, parent, employee));
  return response;
}

export async function deleteEmployee(parent: Parent, employee: IEmployee): Promise<http.IRestResponse<IEmployee>> {
  employee.name = employee.name || employee.givenName;
  let path = buildParentPath(parent);
  path += `/employees/${employee.name}`;
  path = encodeURI(path);
  if (config.debug) {
    console.log('============= DELETE EMPLOYEE =============');
    console.log('DELETE ' + path);
  }
  let response = await orgApi.del<IEmployee>(ORG_API_URL + path);
  if (config.debug) {
    console.log('RESPONSE CODE: ' + response.statusCode);
  }
  if (response.statusCode > 300) {
    throw Error(`DELETE ${path} failed response code: ${response.statusCode}`);
  }
  return response;
}

export async function createFunction(parent: Parent, func: IHSAFunction): Promise<http.IRestResponse<IHSAFunction>> {
  func = Object.assign({}, func);

  // Default values
  func.type = 'Function';
  func.hsaIdentity = func.hsaIdentity || randomHSAID();
  
  let path = buildParentPath(parent) + '/functions';
  path = encodeURI(path);
  if (config.debug) {
    console.log('============= CREATE FUNCTION =============');
    console.log('POST ' + path);
    console.log(func);
  }
  let response = await orgApi.create<IHSAFunction>(ORG_API_URL + path, func);
  if (config.debug) {
    console.log('RESPONSE CODE: ' + response.statusCode);
  }
  if (response.statusCode > 300) {
    throw Error(`POST ${path} failed response code: ${response.statusCode}`);
  }
  undoApiCalls.push(deleteFunction.bind(null, parent, func));
  return response;
}

export async function deleteFunction(parent: Parent, func: IHSAFunction): Promise<http.IRestResponse<IHSAFunction>> {
  let path = buildParentPath(parent);
  path += `/functions/${func.name}`;
  path = encodeURI(path);
  if (config.debug) {
    console.log('============= DELETE FUNCTION =============');
    console.log('DELETE ' + path);
  }
  let response = await orgApi.del<IHSAFunction>(ORG_API_URL + path);
  if (config.debug) {
    console.log('RESPONSE CODE: ' + response.statusCode);
  }
  if (response.statusCode > 300) {
    throw Error(`DELETE ${path} failed response code: ${response.statusCode}`);
  }
  return response;
}

function buildParentPath(parent: Parent): string {
  let path = '/countries/Sverige';
  if (parent.county) {
    path += `/counties/${parent.county}`;
  }
  path += `/organizations/${parent.org}`;
  if (parent.units) {
    for (let i = 0; i < parent.units.length; i++) {
      path += `/units/${parent.units[i]}`;
    }
  }
  return path;
}
