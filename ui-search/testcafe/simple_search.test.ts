// @ts-ignore

import axeCheck from './axe';
import { expectAll, expectNo, expectSameResultAgain, expectAtLeastOne, expectExactlyOne } from './helpers/expect_helpers';
import ResultView, { ObjectCategory } from './pages/ResultView';
import Page from './pages/SimpleSearch';
import { IEmployee} from './testapi/model';
import {
  createUnit,
  Parent,
  createFunction,
  buildParent,
  revertApiCalls,
  createEmployee
} from './testapi/orgapi';
import {
  matchesNameOrPath,
  matchesName,
  matchesCounty,
  matchesOrganization
} from './domain/rules';
import { str, getKeys } from './helpers/helpers';
import { uncheck, checkbox } from './helpers/element_helper';
import Filter from './pages/Filter';

const page = new Page();
const resultView = new ResultView();
let empeLoy: IEmployee;

/**
 * SOK-001: Testning av Enkel sök-gränssnittet.
 * Här testas gränssnittets alla tillstånd beroende på svar från backend och
 * interaktion från användaren. Detta betyder inte uttömmande testning av fritext-APIt.
 **/
fixture`SOK-001, SOK-002: Enkel sök`.page(page.URL).afterEach(revertApiCalls);

let tussilago: Parent = { county: 'Norrbottens län', org: 'Tussilagon' };
let snowdrop: Parent = { county: 'Norrbottens län', org: 'Snödroppen' };

test('WCAG Enkel sök', async t => {
    await t.click(page.filterSymbol)
      
    await axeCheck(t);
  });

//test('WCAG Enkel sök', async t => await axeCheck(t));

/**
 * SOK-007: Som användare ska jag kunna spara enkla sökningar för att använda igen
 * Genom att spara undan URL'en när man fyllt i sökfältet kan man spara t.ex ett bookmark 
 * med sökinformationen som skall vara möjlig att använda igen när man vill
 **/
test('SOK-007: Som användare ska jag kunna spara enkla sökningar för att använda igen', async t => {
  //Start with a basic search for "Nordic" with expected hits
  await t
    .typeText(page.searchInput, 'Nordic')
    .click(page.sokButton);
 
  //Save the URL from the search which should contain search parameters and be possible to
  //e.g save as a bookmark in your browser
   var searchQuery_url = await t.eval(() => window.location.href);
  //Evaluate result from the search which should be a few hits contaoning Nordic
  let result1 = await resultView.getResults();
  await t.expect(result1.length).notEql(0);
  await expectAll(result1, matchesOrganization, 'Nordic');
  
  //Repeat the search based on the URL with all serach information included
  await expectSameResultAgain(searchQuery_url, result1);
});

/**
 * ESOK-002: Det ska vara möjligt att göra en enkel sökning med flera söktermer i kombination
 * Sökning ska ske enligt principen OCH, inte ELLER. 
 **/
test.before(async _ => {
  await createUnit(tussilago, { name: 'Skogsstjärnans sjukhus' });
  await createUnit(snowdrop, { name: 'Liljekonvaljens sjukhus' });
})('ESOK-002: Sökning enligt principen "OCH" mellan söktermer', async t => {
  await t
    .typeText(page.searchInput, 'Tussilagon sjukhus')
    .click(page.sokButton)
    .click(resultView.orgUnitTab);

  let results = await resultView.getResults();
  await expectAll(results, matchesNameOrPath, 'Tussilagon', 'sjukhus');
  await expectNo(results, matchesName, 'Liljekonvaljens sjukhus');
});

/**
 * ESOK-001: Det ska vara möjligt att göra en enkel sökning med en sökterm
 * Enkel sökning ska alltid ske enligt principen innehåller
 **/
// Aktiveras när HSA-464 är klar
test.skip('ESOK-001, ESOK-003: Sökning enligt principen "innehåller" med automatiska wildcard före och efter sökterm', async t => {
  await t
    .typeText(page.searchInput, 'ordi')
    .click(page.sokButton)
    .click(resultView.orgUnitTab);

  let results = await resultView.getResults();
  await expectAll(results, matchesNameOrPath, 'ordi');
});

/**
 * ESOK-003: Enkel sökning ska alltid ske enligt principen "innehåller":
 * Wildcards eller andra tecken som "förstör" ska rensas
 **/
test('ESOK-003: Wildcard-tecken (*) ska rensas bort från sökfältet', async t =>
  t
    .typeText(page.searchInput, '*ordi*')
    .expect(page.searchInput.value)
    .eql('ordi'));

// Aktiveras när HSA-420 är klar
test.skip('ESOK-004: Sökning ska alltid ske "fonetiskt" enligt principen "liknande stavning"', async t => {
  await t.typeText(page.searchInput, 'Nordik').click(page.sokButton);

  let results = await resultView.getResults();
  await expectAll(results, matchesNameOrPath, 'Nordic');
});

/**
 * ESOK-005: Sidan ska vara utformad på ett speciellt sätt:
 * Detta test kontrollerar att valt sökområdet visas under sökrutan
 **/
test('ESOK-005: De nuvarande inställningarna i filtrera ska summeras i enkel sök-vyn', async t => {
  let activeFilters = await page.getActiveFilters();
   await t
    .expect(activeFilters).contains('Sverige')
    .expect(activeFilters).contains('Person')
    .expect(activeFilters).contains('Organisation eller enhet')
    .expect(activeFilters).contains('Medarbetaruppdrag')
    .click(page.sidebarToggler);
  await uncheck(checkbox(Filter.COMMISSION_LABEL));

  activeFilters = await page.getActiveFilters();
  await t
    .click(page.sidebarToggler)
    .expect(activeFilters).contains('Sverige')
    .expect(activeFilters).contains('Person')
    .expect(activeFilters).contains('Organisation eller enhet')
    .expect(activeFilters).notContains('Medarbetaruppdrag')
    .click(page.sidebarToggler);
  await uncheck(checkbox(Filter.ORG_UNIT_LABEL));

  activeFilters = await page.getActiveFilters();
  await t
    .click(page.sidebarToggler)
    .expect(activeFilters).contains('Sverige')
    .expect(activeFilters).contains('Person')
    .expect(activeFilters).notContains('Organisation eller enhet')
    .expect(activeFilters).notContains('Medarbetaruppdrag')
    .click(page.sidebarToggler);
  await uncheck(checkbox(Filter.PERSON_LABEL));

  activeFilters = await page.getActiveFilters();
  await t
    .click(page.sidebarToggler)
    .expect(activeFilters).contains('Sverige')
    .expect(activeFilters).notContains('Person')
    .expect(activeFilters).notContains('Organisation eller enhet')
    .expect(activeFilters).notContains('Medarbetaruppdrag')

});

/**
 * ESOK-005: Sidan ska vara utformad på ett speciellt sätt:
 * Detta test kontrollerar så att stödtext finns i sökrutan
 * Att rätt Rubrik finns, och att det finns länkar till både
 * detaljerad sök och filtermenyn                           
 **/
test('ESOK-005: Enkel sök texter runtom och i sökrutan', async t => {
  let placeHolderText = (await page.searchInput.attributes).placeholder;
  let linkText = page.linkDetailSearch.textContent;
  await t.expect(placeHolderText).eql('Namn, titel, arbetsplats, ort och/eller HSA-id')
    .expect(page.headingSearchArea.textContent).eql('Hitta person eller enhet')
    .expect(linkText).contains('detaljerad sökning');
  
  //Säkerställer att jag klickar och öppnar filtermenyn och kan klicka ur ett fält
  await t.click(page.filtreraText);
  await uncheck(checkbox(Filter.COMMISSION_LABEL));
  await t.expect( await page.getActiveFilters()).notContains('Medarbetaruppdrag');
  });

/**
 * EOK-001: Som användare ska jag kunna söka i HSA                   
 **/
test('SOK-001: Sökning på något som inte ger några träffar', async t => {
  await t
    .typeText(page.searchInput, 'qjaiefjg')
    .click(page.sokButton)
    .expect(page.warnings.withText('Inga träffar').visible)
    .ok();

  let results = await resultView.getResults();
  await t.expect(results.length).eql(0);
});

/**
 * ESOK-006: Sökning på Enhet                   
 **/
test.before(async t => {
  t.ctx.objectName = 'Skogsstjärnans Enhet';
  await createUnit(tussilago, {
    name: t.ctx.objectName,
    hsaIdentity: 'ABC-123'
  });
})('ESOK-006: Sökning på Enhet', performSingleNameSearch);

/**
 * ESOK-006: Sökning på Person                   
 **/
test.before(async t => {
  t.ctx.objectName = 'Razzil';
  await createEmployee(tussilago, { givenName: t.ctx.objectName });
})('ESOK-006: Sökning på Person', performSingleNameSearch);

/**
 * ESOK-006: Sökning på Funktion                   
 **/
test.before(async t => {
  t.ctx.objectName = 'Skogsstjärnans Funktion';
  await createFunction(tussilago, { name: t.ctx.objectName });
})('ESOK-006: Sökning på Funktion', performSingleNameSearch);

/**
 * ESOK-006: Sökning på Organisation                   
 **/
test.before(async t => {
  t.ctx.objectName = 'Tussilagon';
})('ESOK-006: Sökning på Organisation', performSingleNameSearch);

/**
 * ESOK-007: Sökning på län ska hitta objekt inom sökta länet                  
 **/
test.before(async t => {
  await createUnit(tussilago, { name: 'Skogsstjärnans sjukhus' });
  t.ctx.skogsSjukhus = buildParent(tussilago, {
    name: 'Skogsstjärnans sjukhus'
  });
  await createUnit(t.ctx.skogsSjukhus, { name: 'Ortopedavdelningen' });
  await createFunction(t.ctx.skogsSjukhus, { name: 'Majskrok' });
  await createUnit(snowdrop, { name: 'Liljekonvaljens sjukhus' });
  await createEmployee(t.ctx.skogsSjukhus, { givenName: 'Mr. Midas' });
})('ESOK-007: Sökning på län ska hitta objekt inom det länet', async t => {
  await t
    .typeText(page.searchInput, 'Norrbottens län')
    .click(page.sokButton)
    .click(resultView.orgUnitTab)
  let results = await resultView.getResults();

  await expectAll(results, matchesCounty, 'Norrbottens län');
  await expectExactlyOne(results, matchesName, 'Skogsstjärnans sjukhus');
  await expectExactlyOne(results, matchesName, 'Ortopedavdelningen');
  await expectExactlyOne(results, matchesName, 'Liljekonvaljens sjukhus');
  await expectExactlyOne(results, matchesName, 'Majskrok');
  await expectExactlyOne(results, matchesName, 'Mr. Midas');
});

/**
 * ESOK-007: Det ska vara möjligt att söka på följande attribut
 * Tilltalsnamn, Mellannamn,Efternamn, Organisationsnam, Enhetsnamn
 * Organisationsnamn,  Enhetsnamn, Objektnamn, Titel, legitimerad yrkesgrupp
 * Befattning, Geografisk plats, Kommun, Län, Organisationstillhörighet, HSA-ID
 **/
 //When HSA-713 is done this test  will be activated. CountyCode for Värmlandslän  and municipalityCode added for Tussilagon.
 test.skip('ESOK-007: sökning på: Kommun och länskod', async t => {

  await t.typeText(page.searchInput, 'Tussilagon Värmlands län')
    .click(page.sokButton)
    .click(resultView.orgUnitTab);

  let results = await resultView.getResults(ObjectCategory.OrgEllerEnhet);
  await expectAtLeastOne(results, matchesNameOrPath, 'Tussilagon');
  await t.click(resultView.newSearchLink)
    .typeText(page.searchInput, 'Tussilagon Karlstad', { replace: true })
    .click(page.sokButton)
    .click(resultView.orgUnitTab);
  let theResult = await resultView.getResults(ObjectCategory.OrgEllerEnhet);
  await expectAtLeastOne(theResult, matchesNameOrPath, 'Tussilagon');
});

/**
 * ESOK-007: Det ska vara möjligt att söka på följande attribut
 * Tilltalsnamn, Mellannamn,Efternamn, Organisationsnam, Enhetsnamn
 * Organisationsnamn,  Enhetsnamn, Objektnamn, Titel, legitimerad yrkesgrupp
 * Befattning, Geografisk plats, Kommun, Län, Organisationstillhörighet, HSA-ID
 **/
/*Detta test är bortkommenterat eftersom title, middleName och mail inte är sökbart. Invänta databas-byte
Detta test behöver också kompletteras pga paTitleCode, MunicipalityCode,CountyCode*/
test.skip.before(async _ => {
  empeLoy = {
    givenName: 'Empe',
    sn: 'Yee',
    hsaIdentity: 'ESOK-007',
    mail: 'empeloyee@tusengpm.se',
    title: 'Endokriniolog',
    middleName: 'Lo'
  };
  await createEmployee(tussilago, empeLoy);
})('ESOK-007: Sökning på attributen namn hsaId, mail, titel', async t => {

  for (let prop of getKeys(empeLoy)) {
    await t.typeText(page.searchInput, str(empeLoy[prop]))
      .click(page.sokButton);
    let results = await resultView.getResults(ObjectCategory.Person);
    await expectExactlyOne(results, matchesNameOrPath, 'Empe');
    await t
      .click(resultView.newSearchLink)
      .selectText(page.searchInput)
      .pressKey('delete');
  }
});

/**
 * ESOK-007: Det ska vara möjligt att söka på Kommun
 * Detta test kan med fördel läggas till testet ovan när MinicipalityCode är på plats efter databas-byte
 **/
test.skip('Sökning på kommun', async t => {
  await t
    .typeText(page.searchInput, 'Karlstad')
    .click(page.sokButton)
    .click(resultView.orgUnitTab);

  let results = await resultView.getResults();
  await expectExactlyOne(results, matchesName, 'Nordic MedTest');
  await expectExactlyOne(results, matchesName, 'Nordic Organization Maximum');
});

/**
 * ESOK-007: Det ska vara möjligt att söka på Organisationstillhörighet
 **/
test.before(async t => {
  await createUnit(tussilago, { name: 'Skogsstjärnans sjukhus' });
  t.ctx.skogsSjukhus = buildParent(tussilago, {
    name: 'Skogsstjärnans sjukhus'
  });
  await createUnit(t.ctx.skogsSjukhus, { name: 'Ortopedavdelningen' });
  await createFunction(t.ctx.skogsSjukhus, { name: 'Majskrok' });
})('ESOK-007: Sökning på Organisationstillhörighet', async t => {
  await t
    .typeText(page.searchInput, 'Tussilagon')
    .click(page.sokButton)
    .click(resultView.orgUnitTab);

  let results = await resultView.getResults();
  await expectAll(results, matchesOrganization, 'Tussilagon');
  await expectExactlyOne(results, matchesName, 'Skogsstjärnans sjukhus');
  await expectExactlyOne(results, matchesName, 'Ortopedavdelningen');
  await expectExactlyOne(results, matchesName, 'Majskrok');
});

/**
 * ESOK-007: Det ska vara möjligt att söka på geografisk plats
 **/
test.skip('ESOK-007: Sökning på geografisk plats', async t => {
  return t.typeText(page.searchInput, 'Stockholm').click(page.sokButton);
});

/**
 * ESOK-007: Det ska vara möjligt att söka på befattning
 * FÄRDIGSTÄLLS NÄR CODESYSTEM ÄR PÅ PLATS I NY DATABAS
 **/
test.skip('ESOK-007: Sökning på Befattning', async t =>
  t
    .typeText(page.searchInput, 'Verksamhetschef')
    .click(page.sokButton)
    .expect(page.resultsFound.count)
    .eql(0));

/**
 * SOK-001: Som användare ska jag kunna söka i HSA
 **/
test.skip('SOK-001: Gränssnittet för enkel sök ska vara tillgängligt för alla roller', async t =>
  null);

/**
 * ANVG-013: Som en användare (anonym eller administratör) ska jag kunna komma åt hjälptexter.
 **/
test.skip('ANVG-013: Hjälptexter ska finnas tillgängliga', async t => null);

/**
 * ANVG-010: Som anonym användare ska jag informeras om att gränssnittet använder cookies
 **/
test.skip('ANVG-010: Som anonym användare ska jag informeras om att gränssnittet använder cookies', async t =>
  null);

/**
 * Hjälpfunktion: performSingleNameSearch används bla i ESOK-006
 **/
async function performSingleNameSearch(t: TestController) {
  await t.typeText(page.searchInput, t.ctx.objectName).click(page.sokButton);

  let results = await resultView.getResults();
  await expectAll(results, matchesNameOrPath, ...t.ctx.objectName.split(' '));
  await expectExactlyOne(results, matchesName, t.ctx.objectName);
}
