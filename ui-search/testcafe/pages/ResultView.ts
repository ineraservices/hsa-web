import { IResult, IResultSelector, ICountableSelector } from '../helpers/custom_selectors';
import { Selector } from 'testcafe';

export enum ObjectCategory {
  Person,
  OrgEllerEnhet,
  Medarbetaruppdrag
}

export default class ResultView {
  private _paginationButtonSelector = '#app2 .page-item';
  searchArea = Selector('i').withText('Sökområde:').find('b');
  heading = Selector('h1, h2, h3, h4, h5');
  personTab = <ICountableSelector>Selector('#person-tab').addCustomDOMProperties({ itemCount: this._getTabResultCount });
  orgUnitTab = <ICountableSelector>Selector('#org-unit-tab').addCustomDOMProperties({ itemCount: this._getTabResultCount });
  commissionTab = <ICountableSelector>Selector('#commission-tab').addCustomDOMProperties({ itemCount: this._getTabResultCount });

  nextPage = Selector(this._paginationButtonSelector).withExactText('›');
  prevPage = Selector(this._paginationButtonSelector).withExactText('‹');
  firstPage = Selector(this._paginationButtonSelector).withExactText('«');
  lastPage = Selector(this._paginationButtonSelector).withExactText('»');

  hitsLabel = <ICountableSelector>Selector('#results-found').addCustomDOMProperties({
    itemCount: el => {
      let match = el.innerHTML.match(/(\d+) träff/);
      if (match) {
        return Number.parseInt(match[1]);
      }
      throw Error('Tab does not have a valid hit count.');
    }
  });

  resultLabel = <ICountableSelector>Selector('#results-found').addCustomDOMProperties({
    itemCount: el => {
      let match = el.innerHTML.match(/(\d+) visas/);
      if (match) {
        return Number.parseInt(match[1]);
      }
      throw Error('Tab does not have a valid result count.');
    }
  });

  newSearchLink = Selector('a').withExactText('Gör om din sökning.');
  filtreraText = Selector('a').withText('Filtrera');

  async getResults(type?: ObjectCategory): Promise<IResult[]> {
    switch (type) {
      case ObjectCategory.Person: return await this._personResults.getResults();
      case ObjectCategory.OrgEllerEnhet: return await this._orgUnitResults.getResults();
      case ObjectCategory.Medarbetaruppdrag: return await this._commissionResults.getResults();
      default: return await this._results.getResults();
    }
  }
  getPage(page: number): Selector {
    return Selector(this._paginationButtonSelector).withExactText(`${page}`)
  }

  private _results = <IResultSelector>Selector('#results-tab').addCustomMethods({ getResults: this._getResults });
  private _personResults = <IResultSelector>Selector('#result-1-section').addCustomMethods({ getResults: this._getResults });
  private _orgUnitResults = <IResultSelector>Selector('#result-2-section').addCustomMethods({ getResults: this._getResults });
  private _commissionResults = <IResultSelector>Selector('#result-3-section').addCustomMethods({ getResults: this._getResults });
  /**
   * Given a container with search results, this function
   * returns an array of objects containing data from
   * each result. This code will run in the browser
   */
  private _getResults(div: Element): any[] {
    var spans = [];
    var spanElements = div.getElementsByTagName('a');
      for (var i = 0; i < spanElements.length; i++) {
        spans.push(spanElements[i]);
      }
      return spans
        .map(a => ({
            name: a.getElementsByTagName("h3")[0].innerHTML.trim(),
            type: null,
          path: a.getElementsByTagName("p")[0].innerHTML.trim()
      }));
  }

  private _getTabResultCount(el: Element): number {
    let match = el.innerHTML.match(/\((\d+)\)/);
    if (match) {
      return Number.parseInt(match[1]);
    }
    throw Error('Tab does not have a valid result count.');
  }
}
