import { Selector, t } from 'testcafe';
import { config } from '../config.js';
import { IEmployee, IOrganization, IHSAFunction, IUnit } from '../testapi/model';

type HSAObjectProperty = (keyof IEmployee | keyof IOrganization | keyof IHSAFunction | keyof IUnit);

export default class DetailedSearch {
  /**
   * For a given property on an object in HSA, returns the
   * Selector for the DOM-element that maps to this property
   */
  getPropElement(prop: HSAObjectProperty): Selector {
    if (this.propElementMap.has(prop)) {
      return this.propElementMap.get(prop) as Selector;
    }
    throw Error(`Key ${prop} not found.`);
  }

  selectVerksamhet(verksamhet: string) {
    return t.click(this.searchParameters.select_verksamhet.parent())
      .click(Selector('span').withText(verksamhet));
  }

  selectMedarbetaruppdragsAndamal(muAndamal: string) {
    return t.click(this.searchParameters.select_muAndamal.parent())
      .click(Selector('span').withText(muAndamal));
  }

  URL: string = config.baseUrl + '/#/detailedSearch/';
  btn_sok: Selector = Selector('#submit-button');
  label_warnings: Selector = Selector('.alert-warning');
  label_resultsFound: Selector = Selector('#results-found');

  searchParameters = new SearchParameters();

  propElementMap: Map<HSAObjectProperty, Selector> = (() => {
    let m = new Map<HSAObjectProperty, Selector>();
    // Employee
    m.set('givenName', this.searchParameters.text_tilltalsnamn);
    m.set('nickName', this.searchParameters.text_tilltalsnamn);
    m.set('sn', this.searchParameters.text_efternamn);
    m.set('middleName', this.searchParameters.text_efternamn);
    m.set('title', this.searchParameters.text_titel);
    m.set('hsaTitle', this.searchParameters.text_titel);
    m.set('hsaSosTitleCodeSpeciality', this.searchParameters.text_titel);
    // Organisation/Unit
    m.set('name', this.searchParameters.text_orgEnhetMu);
    // Shared
    m.set('postalAddress', this.searchParameters.text_adress);
    m.set('hsaIdentity', this.searchParameters.text_hsaId);
    m.set('telephoneNumber', this.searchParameters.text_telefonnr);
    m.set('mail', this.searchParameters.text_epost);
    m.set('mobile', this.searchParameters.text_telefonnr);
    m.set('hsaSwitchboardNumber', this.searchParameters.text_telefonnr);
    m.set('street', this.searchParameters.text_adress);
    return m;
  })();

  link_enkelSok: Selector = Selector('a').withExactText('enkel sök');
  link_filter: Selector = Selector('a').withExactText('Filtrera');

  sidebarToggler = Selector('#sidebar-toggler');
  filtreraText = Selector('a').withText('Filtrera');
  searchArea = Selector('#search-area-path').find('i');
};

class SearchParameters {
  text_tilltalsnamn: Selector = Selector('#givenName');
  text_efternamn: Selector = Selector('#lastName');
  text_orgEnhetMu: Selector = Selector('#organizationOrUnitName');
  text_hsaId: Selector = Selector('#hsaId');
  text_titel: Selector = Selector('#title');
  text_plats: Selector = Selector('#place');
  text_telefonnr: Selector = Selector('#telephone');
  select_verksamhet: Selector = Selector('#businessClassificationCode');
  text_adress: Selector = Selector('#address');
  select_muAndamal: Selector = Selector('#hsaCommissionMemberCode');
  text_epost: Selector = Selector('#email');
};
