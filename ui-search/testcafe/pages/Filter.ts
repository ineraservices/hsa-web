import { Selector, t } from 'testcafe';
import { ISearchAreaSelector, ISearchAreaOption } from '../helpers/custom_selectors';

export default class Filter {

  static PERSON_LABEL: string = 'Person';
  static ORG_UNIT_LABEL: string = 'Organisation eller enhet';
  static COMMISSION_LABEL: string = 'Medarbetaruppdrag';
  static EXACT_SEARCH_LABEL: string = 'Exakt sökning';

  searchArea = Selector('#search-area');
  
  openSearchAreaSelector() {
    return t.click(this.searchArea.parent());
  }

  selectSearchAreaByScrolling(searchAreaText: string) {
    return this.openSearchAreaSelector()
      .click(Selector('span').withText(searchAreaText));
  }

  selectSearchAreaByTyping(searchAreaText: string) {
    return this.typeSearchArea(searchAreaText)
      .click(Selector('span').withText(searchAreaText).nth(0));
  }

  typeSearchArea(searchAreaText: string) {
    return this.openSearchAreaSelector()
      .typeText(this.searchArea, searchAreaText);
  }

  clearAndTypeSearchAreaText(searchAreaText: string) {
      return this.openSearchAreaSelector()
        .typeText(this.searchArea, searchAreaText, { replace: true });
  }

  async getSearchAreaOptions(): Promise<ISearchAreaOption[]> {
    return this._searchAreaOptions.getSearchAreaOptions();
  }
  
  private _searchAreaOptions = <ISearchAreaSelector>Selector('ul.multiselect__content').addCustomMethods({
   /**
   * Given a container with search results, this function
   * returns an array of objects containing data from
   * each result. This code will run in the browser
   */
    getSearchAreaOptions: function(div: Element): ISearchAreaOption[] {
      var spans = [];
      var spanElements = div.getElementsByTagName('span');
      for (var i = 0; i < spanElements.length; i++) {
        spans.push(spanElements[i]);
      }
      return spans
          .filter(a => a.childElementCount === 0)
          .filter(a => a.parentElement ? a.parentElement.style.display !== 'none' : false) // Only return elements whose parent is visible
          .map(a => ({
            name: a.innerHTML.trim(),
            segments: a.innerHTML.trim().split(' » ')
          }));
      }
    });

}
