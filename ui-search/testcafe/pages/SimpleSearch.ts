import { Selector } from 'testcafe';
import { config } from '../config.js';


export default class SimpleSearch {
    URL = config.baseUrl + '/';
    searchInput = Selector('#omniboxSearch01');
    sokButton = Selector('#submit-button');
    warnings = Selector('.no-results-info');
    noResultsFound = Selector('#no-results-found');
    resultsFound = Selector('#results-found');
    sidebarToggler = Selector('#sidebar-toggler');
    filtreraText = Selector('a').withText('Filtrera');
    filterSymbol = Selector('#filter-button');
    exactSearchButton =Selector('#exactMatch');  // Selector('label').withText('Ja');
    searchAreaList = Selector('.multiselect__select');
    searchAreaItems = Selector('#searchArea > ul > li > span');
    headingSearchArea = Selector('.py-1');
    linkDetailSearch = Selector('a').withText('detaljerad sökning');
    selectedSearchArea = Selector('#search-filters');
    selectedEmployee = Selector('.filter-search-within-org');
    choosingPerson = Selector('form-check-input');

    async getActiveFilters() {
      return (await this.selectedSearchArea.innerText)
        .split('\n')
        .map(a => a.replace('\r','')) // IE also has a \r character for carriage returns, remove it
        .filter(a => a !== '')
        .map(a => a.trim());
    }     
};
