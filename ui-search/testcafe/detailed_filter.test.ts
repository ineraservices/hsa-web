import DetailedSearch from './pages/DetailedSearch';
import Filter from './pages/Filter';
import { check, uncheck, checkbox } from './helpers/element_helper';
import { expectExactlyOne, expectNo, expectAll, expectSameResultAgain } from './helpers/expect_helpers'
import { matchesName ,matchesCounty} from './domain/rules';
import { createUnit, createEmployee, revertApiCalls } from './testapi/orgapi';
import ResultView, { ObjectCategory } from './pages/ResultView';

const page = new DetailedSearch();
const filter = new Filter();
const searchParams = page.searchParameters;
const resultView = new ResultView();

/**
 * Testning av detailed sök-gränssnittet och dess möjligheter att filtrera sökresultatet.
 * Vänsterpanelen ger möjlighet att begränsa sökområde, objekttyper och att exakt stavning.
 **/
fixture `SOK-005: Detaljerad sök: Filter`
  .page(page.URL)
  .afterEach(revertApiCalls);

/**
 * FSOK-001: Filtrera detaljerad sökning menyn ska gå att nå via flera olika knappar
 * 2 ställen finns i detaljerad sök vyn för att få fram vänsterpanelen med filter
 **/
test('FSOK-001: Filtrera detaljerad sökning menyn ska gå att nå via flera olika knappar', async t => {
  //Test to open left sidebar filter view by click on arrow to the left in main window.
  await t.click(page.sidebarToggler)
    .expect(checkbox(Filter.EXACT_SEARCH_LABEL).visible).ok()
    .click(page.sidebarToggler)
    .expect(checkbox(Filter.EXACT_SEARCH_LABEL).exists).notOk();

  //Test to open left sidebar filter view by click on "Filtrera" bottom of page.
  await t.click(page.filtreraText)
    .expect(checkbox(Filter.EXACT_SEARCH_LABEL).visible).ok()
    .click(page.filtreraText)
    .expect(checkbox(Filter.EXACT_SEARCH_LABEL).exists).notOk();

  //Make a search just to get to the resultview to test next clickable toggler "Filtrera text".
  await t.click(page.btn_sok)
    .click(resultView.filtreraText)
    .expect(checkbox(Filter.EXACT_SEARCH_LABEL).visible).ok()
    .click(resultView.filtreraText)
    .expect(checkbox(Filter.EXACT_SEARCH_LABEL).exists).notOk();
});

/**
 * FSOK-002: Detaljerat sökområde ska kunna väljas genom att bläddra i listan
 * Skapar en dublett av Nordic Unit i Norrbotten och sök på den med/utan filter på Norrbotten
 **/
test.before(async _ => {
  await createUnit({ county: 'Norrbottens län', org: 'Tussilagon' }, { name: 'Nordic Unit' });
})('FSOK-002: Detaljerat sökområde ska kunna väljas genom att bläddra i listan', async t => {
  await t.click(page.sidebarToggler)
    .typeText(searchParams.text_orgEnhetMu, 'Nordic Unit');
  await filter.selectSearchAreaByScrolling('Norrbottens län');
  await t.click(page.btn_sok);

  let results = await resultView.getResults();
  await expectAll(results, matchesCounty, 'Norrbottens län');
  await t.expect(resultView.searchArea.innerText).eql('Sverige » Norrbottens län')
    .expect(resultView.orgUnitTab.itemCount).eql(results.length);
    //.expect(resultView.hitsLabel.itemCount).eql(results.length); //Jira HSA-700
});

/**
 * FSOK-002: Enheter ska dyka upp i sökområdet först efter att 3 bokstäver skrivits in
 **/
test.before(async _ => {
  await createUnit({ county: 'Värmlands län', org: 'Nordic MedTest' }, { name: 'abc maskros 100' });
  await createUnit({ county: 'Värmlands län', org: 'Nordic MedTest' }, { name: 'abc maskros 200' });
})('FSOK-002: Enheter ska dyka upp i sökområdet först efter att 3 bokstäver skrivits', async t => {
  await t.click(page.sidebarToggler);
  await filter.openSearchAreaSelector();
  let options = await filter.getSearchAreaOptions();

  // Innan man skrivit in något ska enheterna inte dyka upp
  await expectNo(options, matchesName, 'Värmlands län » Nordic MedTest » abc maskros');

  // Efter 2 tecken ska enheterna inte kommit upp än
  await filter.typeSearchArea('ab');
  options = await filter.getSearchAreaOptions();
  await expectNo(options, matchesName, 'Värmlands län » Nordic MedTest » abc maskros');

  // Efter 3 tecken ska enheterna dyka upp i listan
  await filter.clearAndTypeSearchAreaText('abc');
  options = await filter.getSearchAreaOptions();
  await expectExactlyOne(options, matchesName, 'Värmlands län » Nordic MedTest » abc maskros 100');
  await expectExactlyOne(options, matchesName, 'Värmlands län » Nordic MedTest » abc maskros 200');
  await expectAll(options, matchesName, 'abc');
});

/**
 * FSOK-003: Detaljerad sök filtrerad på objekttyper - Person
 * Skapar en enhet och en person med samma postaAddress och filtrerar på Person
 * Endast personen bör synas i sökresultatet.
 * DETTA TEST BORDE FUNKA NU MEN PERSONEN HITTAS INTE PGA att adressLine inte är indexerat
 **/
test.skip.before(async _ => {
  let matte = {
    givenName: 'Mattias',
    sn: 'Persson',
    postalAddress: { addressLine: ['testvägen 666'] }
  };
  let matteEnheten = {
    name: 'Mattes testenhet',
    postalAddress: { addressLine: ['testvägen 666'] }
  };
  await createEmployee({ county: 'Värmlands län', org: 'Nordic MedTest' }, matte);
  await createUnit({ county: 'Värmlands län', org: 'Nordic MedTest' }, matteEnheten);
})('FSOK-003: Detaljerad sök filtrerad på objekttyper - Person', async t => {
  await t.click(page.sidebarToggler);
  await uncheck(checkbox(Filter.COMMISSION_LABEL));
  await uncheck(checkbox(Filter.ORG_UNIT_LABEL));

  // Detta test gör en sökning på adressen
  await t.typeText(searchParams.text_adress, 'testvägen 666').click(page.btn_sok);

  // Förväntar oss en träff på personen "Matte" och inget mer
  let results = await resultView.getResults(ObjectCategory.Person);
  await t.expect(resultView.personTab.itemCount).eql(results.length)
    .expect(resultView.hitsLabel.itemCount).eql(results.length);
  await expectExactlyOne(results, matchesName, 'Mattias');

  // Förväntar oss ingen träff på enheten "Mattes testenhet"
  results = await resultView.getResults(ObjectCategory.OrgEllerEnhet);
  await expectNo(results, matchesName, 'Mattes testenhet');
  await t.expect(resultView.orgUnitTab.itemCount).eql(0)
    // Förväntar oss inget träff på Medarbetaruppdrag
    .expect(resultView.commissionTab.itemCount).eql(0)
    .click(resultView.newSearchLink);
});

/**
 * FSOK-003: Detaljerad sök filtrerad på objekttyper - Enhet
 * Skapar en enhet och en person med samma mailadress och filtrerar på Organisation och enhet
 * Endast enheten bör synas i sökresultatet.
 **/
test.before(async _ => {
  let matte = {
    givenName: 'Mats',
    sn: 'Matsson',
    mail: "testmail@test.te"
  };
  let matteEnheten = {
    name: 'Mats testenhet',
    mail: "testmail@test.te"
  };
  await createEmployee({ county: 'Värmlands län', org: 'Nordic MedTest' }, matte);
  await createUnit({ county: 'Värmlands län', org: 'Nordic MedTest' }, matteEnheten);
})('FSOK-003: Detaljerad sök filtrerad på objekttyper - Enhet', async t => {
  await t.click(page.sidebarToggler);
  await uncheck(checkbox(Filter.COMMISSION_LABEL));
  await uncheck(checkbox(Filter.PERSON_LABEL));

  // Detta test gör en sökning på adressen
  await t.typeText(searchParams.text_epost, 'testmail@test.te').click(page.btn_sok);

  // Förväntar oss en träff på enheten "MatteEnheten" och inget mer
  let results = await resultView.getResults(ObjectCategory.OrgEllerEnhet);
  await t.expect(resultView.orgUnitTab.itemCount).eql(results.length)
    .expect(resultView.hitsLabel.itemCount).eql(results.length);
  await expectExactlyOne(results, matchesName, 'Mats testenhet');

  // Förväntar oss ingen träff på personen "Matte"
  results = await resultView.getResults(ObjectCategory.Person);
  await expectNo(results, matchesName, 'Mats');
  await t.expect(resultView.personTab.itemCount).eql(0)
    // Förväntar oss inget träff på Medarbetaruppdrag
    .expect(resultView.commissionTab.itemCount).eql(0)
    .click(resultView.newSearchLink);
});

/**
 * FSOK-003: Detaljerad sök filtrerad på objekttyper - Medarbetaruppdrag
 **/
test.skip.before(async _ => {

})('FSOK-003: Detaljerad sök filtrerad på objekttyper - Medarbetaruppdrag', async _ => {

});

/**
 * FSOK-004: Detaljerad sökning med "Exakt sökning" ska endast returnera svar med hela ord
 **/
test.before(async _ => {
  await createUnit({ county: 'Värmlands län', org: 'Nordic MedTest' }, { name: 'Vårdenhet viol' });
  await createUnit({ county: 'Värmlands län', org: 'Nordic MedTest' }, { name: 'Violas barnsjukhus' });
})
('FSOK-004: Detaljerad sökning med "Exakt sökning" ska endast returnera svar med hela ord', async t => {
  await t.click(page.sidebarToggler);
  await check(checkbox(Filter.EXACT_SEARCH_LABEL))
  await t.typeText(searchParams.text_orgEnhetMu, 'Viol')
    .click(page.btn_sok);

  let results = await resultView.getResults();
  await expectNo(results, matchesName, 'Viola');
  await expectExactlyOne(results, matchesName, 'Vårdenhet viol');
});

/**
 * FSOK-005: Gränssnittet ska komma ihåg val av sökområde under samma browserssession
 * SOK-007: Genom att spara undan URL'en när man fyllt i sökfälten kan man spara ett bookmark med 
 * sökinformationen. Både det som skrivits i sökfältet och de alternativ som kryssats i
 * filtret ska följa med i urlen
 **/
test('FSOK-005, SOK-007: Gränssnittet ska komma ihåg val av sökområde under samma browserssession,Ge användaren möjlighet att spara sökning', async t => {
  await t.click(page.sidebarToggler);
  await filter.selectSearchAreaByScrolling('Värmlands län')
    //Kolla att sökområdet uppdateras i huvudfönstret direkt
    .expect(page.searchArea.innerText).eql('Sverige » Värmlands län');
  await t.click(page.filtreraText)
    .typeText(searchParams.text_tilltalsnamn, 'Sven')
     //Kolla att sökområdet består i sökfönstret efter sidopanelen är borta och innan sökning
    .expect(page.searchArea.innerText).eql('Sverige » Värmlands län')
    .click(page.btn_sok);
  //Kolla att sökområdet är rätt i resultatvyn
  await t.expect(resultView.searchArea.innerText).eql('Sverige » Värmlands län');
  var searchQuery_url = await t.eval(() => window.location.href);
  let results = await resultView.getResults();
  
  await t.click(resultView.newSearchLink)
    .click(page.sidebarToggler)
    //Kolla att sökområdet består även inför en omsökning
    .expect(page.searchArea.innerText).eql('Sverige » Värmlands län');
  //Repeterar samma sökning igen genom att kopiera och klistra in urlen och jämföra resultaten före och efter
  await expectSameResultAgain(searchQuery_url, results);

  //kontroll att sökområdet finns kvar efter att "bokmärke" klistrats in och sparad sökning besökts pånytt
  await t.expect(resultView.searchArea.innerText).contains('Sverige » Värmlands län')
    .click(resultView.newSearchLink)
    //.click(page.sidebarToggler)
    //Kolla att sökområdet består även inför en omsökning
    .expect(page.searchArea.innerText).eql('Sverige » Värmlands län');
 });

