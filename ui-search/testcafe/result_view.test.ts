import ResultView, { ObjectCategory } from './pages/ResultView'
import Page from './pages/SimpleSearch'
import { createUnit, Parent, buildParent, revertApiCalls, createEmployee } from './testapi/orgapi';
import { expectExactlyOne, expectNoIntersection, expectEqualArrays } from './helpers/expect_helpers';
import { matchesPath } from './domain/rules';
import axeCheck from './axe';

const page = new Page();
const resultView = new ResultView()

let tussilago: Parent = { county: 'Norrbottens län', org: 'Tussilagon' };

/**
 * Denna testsvit verifierar att den information och funktionalitet
 * som resultatvyn ska erbjuda finns och fungerar.
 *
 * "Givet ett visst resultat av en sökning ska resultatvyn presentera detta på rätt sätt"
 **/
fixture`Enkel sök: Resultatvy`
  .page(page.URL)
  .afterEach(revertApiCalls);

test
  .before(async _ => {
    await createUnit(tussilago, { name: 'Skogsstjärnans sjukhus' });
    await createUnit(tussilago, { name: 'Liljekonvaljens sjukhus' });
    await createEmployee(tussilago, { givenName: 'Razzil', sn: 'Darkbrew' });
  })
  ('WCAG Resultatvy', async t => {
    await t.typeText(page.searchInput, 'Tussilagon')
      .click(page.sokButton)
      .click(resultView.orgUnitTab);
    await axeCheck(t);
  });

/**
 * SOKR-001: Resultatvyn ska visa antalet träffar, och kategorisera resultaten i tabbar
 *  TODO: Ska kompletteras med övriga kategorier
 **/
 test
  .before(async _ => {
    await createUnit(tussilago, { name: 'Skogsstjärnans sjukhus' });
    await createUnit(tussilago, { name: 'Liljekonvaljens sjukhus' });
    await createUnit(tussilago, { name: 'Prästkragens sjukhus' });
  })
  ('SOKR-001: Resultatvyn ska visa antalet träffar, och kategorisera resultaten i tabbar', async t => {
    await t.typeText(page.searchInput, 'Tussilagon')
      .click(page.sokButton)
      .click(resultView.orgUnitTab);

    let results = await resultView.getResults(ObjectCategory.OrgEllerEnhet);
    await t.expect(resultView.orgUnitTab.itemCount).eql(results.length)
      .expect(resultView.hitsLabel.itemCount).eql(results.length);
  });

/**
 * SOKR-005: Organisationstillhörigheten ska visas som "bread crumbs
 * // TODO: Se nedan
 **/
test
  .before(async t => {
    await createUnit(tussilago, { name: 'Skogsstjärnans sjukhus' });
    t.ctx.skogsstjarnan = buildParent(tussilago, { name: 'Skogsstjärnans sjukhus' });
    await createUnit(t.ctx.skogsstjarnan, { name: 'Avdelning 1337' });
    t.ctx.avdelning1337 = buildParent(t.ctx.skogsstjarnan, { name: 'Avdelning 1337' });
    await createUnit(t.ctx.avdelning1337, { name: 'Avdelning 1338' });
  })
  ('SOKR-005: Organisationstillhörigheten ska visas som "bread crumbs"', async t => {
    await t.typeText(page.searchInput, 'Tussilago')
      .click(page.sokButton)
      .click(resultView.orgUnitTab);

    let results = await resultView.getResults();
    await expectExactlyOne(results, matchesPath, 'Norrbottens län » Tussilagon » Skogsstjärnans sjukhus » Avdelning 1337');
    // TODO: Följande expect för förkortning av långa bread crumbs bör
    // fallera när kravet är tydligt och det är implementerat
    await expectExactlyOne(results, matchesPath, 'Norrbottens län » Tussilagon » Skogsstjärnans sjukhus » Avdelning 1337 » Avdelning 1338');
  });

/**
 * SOKR-010: Pagineringens kontroller ska fungera och olika resultat ska presenteras på varje sida.
 **/
test
  .before(async _ => {
    for (let i = 1; i <= 25; i++) {
      await createUnit(tussilago, { name: `Skogsstjärnans sjukhus ${i}` });
    }
  })
  ('SOKR-010: Pagineringens kontroller ska fungera och olika resultat ska presenteras på varje sida.', async t => {
    await t.typeText(page.searchInput, 'Skogsstjärnans sjukhus')
      .click(page.sokButton)
	  .click(resultView.orgUnitTab);

    let resultsPage1 = await resultView.getResults();
    await t.click(resultView.getPage(2));
    let resultsPage2 = await resultView.getResults();

    await t.click(resultView.nextPage);
    let resultsPage3 = await resultView.getResults();
    await t.expect(resultsPage1.length).eql(10)
      .expect(resultsPage2.length).eql(10)
      .expect(resultsPage3.length).eql(5)
    await expectNoIntersection(resultsPage1, resultsPage2);
    await expectNoIntersection(resultsPage2, resultsPage3);
    await expectNoIntersection(resultsPage1, resultsPage3);

    await t.click(resultView.firstPage);
    await expectEqualArrays(resultsPage1, await resultView.getResults(), (result) => result.name);
    await t.click(resultView.lastPage);
    await expectEqualArrays(resultsPage3, await resultView.getResults(), (result) => result.name);
    await t.click(resultView.prevPage);
    await expectEqualArrays(resultsPage2, await resultView.getResults(), (result) => result.name);
  });

/**
 * SOKR-009: Sökresultatet ska vara prioriterat och sorterat
 **/
test
  .before(async _ => {
    await createUnit(tussilago, { name: 'Skogsstjärnans vårdcentral' });
    await createUnit(tussilago, { name: 'Skogsstjärnans sjukhus' });
    await createUnit(tussilago, { name: 'Skogsstjärnan' });
    await createUnit(tussilago, { name: 'Skogsstjärnans apotek' });
  })
  ('SOKR-009: Sökresultatet ska vara prioriterat och sorterat', async t => {
    await t.typeText(page.searchInput, 'Skogsstjärnan')
      .click(page.sokButton)
      .click(resultView.orgUnitTab);

    let results = await resultView.getResults();
    await t.expect(results[0].name).eql('Skogsstjärnan')
      .expect(results[1].name).eql('Skogsstjärnans apotek')
      .expect(results[2].name).eql('Skogsstjärnans sjukhus')
      .expect(results[3].name).eql('Skogsstjärnans vårdcentral')
  });

/**
 * SOKR-006: Vid antal träffar över 200 ska användaren informeras
 * SOKR-007: Vid fler än maximalt antal sökträffar ska de 200 högst 
 * prioriterade visas
 **/
test
  .before(async _ => {
    await createUnit(tussilago, { name: 'Skogs sjukhus' });
    for (let i = 1; i <= 201; i++) {
      await createUnit(tussilago, { name: `Skogsstjärnans sjukhus ${i}` });
    }
  })
  ('SOKR-006, SOKR-007: Vid antal träffar över 200 ska användaren informeras, och de mest relevanta resultaten visas', async t => {
    await t.typeText(page.searchInput, 'Skogs sjukhus')
      .click(page.sokButton)
      .click(resultView.orgUnitTab);

    let results = await resultView.getResults();
    await t.expect(resultView.orgUnitTab.itemCount).eql(200, 'Totalt antal presenterade resultat ska vara korrekt')
      .expect(resultView.resultLabel.itemCount).eql(200, 'Antal visade träffar ska vara korrekt')
      .expect(resultView.hitsLabel.itemCount).eql(202, 'Totalt antal träffar ska vara korrekt')
      .expect(resultView.heading.withText('Varför visas inte alla resultat?').visible).ok('Varningstext om trunkerat resultat ska visas')
      .expect(results[0].name).eql('Skogs sjukhus', 'Det mest relevanta sökresultat ska visas först')
      .expect(results[1].name).eql('Skogsstjärnans sjukhus 10', 'Resultat med samma relevans ska sorteras i bokstavsordning');
  });
/**
 * SOKR-012: Användaren ska kunna välja en träff och visa (den korta) detaljvyn
 **/
test.skip('SOKR-012: Användaren ska kunna välja en träff och visa (den korta) detaljvyn', async t => {

});
/**
 * SOKR-013: Användaren ska kunna välja en träff och visa objektets placering i Bläddravyn
 **/
test.skip('SOKR-013: Användaren ska kunna välja en träff och visa objektets placering i Bläddravyn', async t => {

});
