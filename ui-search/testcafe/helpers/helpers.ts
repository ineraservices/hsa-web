/**
 * Typesafe version of Object.keys()
 * @param obj
 */
export function getKeys<T extends object>(obj: T): (keyof T)[] {
  return Object.keys(obj) as (keyof T)[];
}

/**
 * Simple stringyfier. Flattens objects and arrays
 * @param obj 
 */
export function str(obj: any): string {
  const flatten = ((acc: string, curr: any) => acc + ' ' + curr);
  if (Array.isArray(obj)) {
    return obj.reduce(flatten);
  } else if (typeof obj === 'object') {
    return Object.keys(obj).map(it => str(obj[it])).reduce(flatten);
  } else {
    return '' + obj;
  }
}