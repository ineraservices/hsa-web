import { t, Selector } from 'testcafe';

async function uncheck(element: Selector) {
  if (await element.checked) {
    return t.click(element);
  }
  return Promise.resolve();
}

async function check(element: Selector) {
  if (!await element.checked) {
    return t.click(element);
  }
  return Promise.resolve();
}

/**
 * Returns the corresponding checkbox that is a child
 * of the label with the given exact text.
 * @param text The label on the checkbox
 */
function checkbox(text: string) {
  return Selector('label')
    .withExactText(text)
    .find('input[type=checkbox]');
}

export { uncheck, check, checkbox };
