import { IResult, IName } from './custom_selectors';
import { t } from 'testcafe';
import { Rule } from '../domain/rules';
import ResultView from '../pages/ResultView';

const resultView = new ResultView();

/**
 * Applies a rule function to each of the given (non-empty) results.
 * If any rule fail, the expect fails.
 * 
 * @param results Array of Results
 * @param rule The rule should be applied to each result.
 */
export function expectAll<T extends IName>(results: T[], rule: (...str: string[]) => Rule<T>, ...str: string[]): Promise<number> {
  let nonPassingResults = results.filter(r => !rule(...str)(r))
  let message = 'Non-matching results found:\n > ' + nonPassingResults.map(r => r.name)
    .join('\n > ') + `\n when asserting a rule with the following strings:\n'${str.join("', '")}'`;
  return t.expect(results.length).gt(0, "Could not find any results matching predicate")
    .expect(nonPassingResults.length).eql(0, message);
}

/**
 * Applies a Rule function to each of the given results.
 * Exactly one result must pass the Rule to pass the expect
 * 
 * @param results Array of Results
 * @param rule The rule should be applied to each result.
 * @param str The strings that the rule should apply to the Results
 */
export function expectExactlyOne<T>(results: T[], rule: (...str: string[]) => Rule<T>, ...str: string[]): Promise<number> {
  return t.expect(results.filter(r => rule(...str)(r)).length).eql(1, `Expected exactly one, zero or more than one result found when matching :\n'${str.join("', '")}'`);
}
/**
 * Applies a Rule function to each of the given results.
 * At least one result must pass the Rule to pass the expect
 * 
 * @param results Array of Results
 * @param rule The rule should be applied to each result.
 * @param str The strings that the rule should apply to the Results
 */
export function expectAtLeastOne<T>(results: T[], rule: (...str: string[]) => Rule<T>, ...str: string[]): Promise<number> {
  return t.expect(results.filter(r => rule(...str)(r)).length).gt(0, `No matching result when asserting a rule with the following strings:\n'${str.join("', '")}'`);
}
/**
 * Applies a Rule function to each of the given results.
 * No Result may pass the Rule to pass the expect
 * 
 * @param results Array of Results
 * @param rule The rule should be applied to each result.
 * @param str The strings that the Rule should apply to the Results
 */
export function expectNo<T extends IName>(results: T[], rule: (...str: string[]) => Rule<T>, ...str: string[]): Promise<number> {
  let nonPassingResults = results.filter(r => rule(...str)(r));
  let message = "Unexpected results found:\n" + nonPassingResults.map(r => r.name)
    .join("\n") + `\n when asserting a rule with the following strings:\n'${str.join("', '")}'`;
  return t.expect(nonPassingResults.length).eql(0, message);
}

/**
 * Expects the intersection of two given sets to be the empty set.
 * Comparison is made by Result name only.
 * @param results1 The first result set
 * @param results2 The second result set
 */
export function expectNoIntersection(results1: IResult[], results2: IResult[]): Promise<number> {
  let intersection = results1.filter(r1 => results2.map(r2 => r2.name).includes(r1.name));
  let message = `Expected completely different results, but equal members found: ${intersection}`;
  return t.expect(intersection.length).eql(0, message);
}

/**
 * Expects two arrays to have equal elements (in any order).
 * @param array1 
 * @param array2 
 * @param mapFn Mapping function that will be applied to each item before comparison
 */
export async function expectEqualArrays<T>(array1: T[], array2: T[], mapFn: (item: T) => any): Promise<number> {
  let nonMatchingItems =
    array1.filter(item => !array2.map(mapFn).includes(mapFn(item)))
      .concat(
        array2.filter(item => !array1.map(mapFn).includes(mapFn(item)))
      );

  let message = `Non matching items found: ${nonMatchingItems.map(mapFn)}`;
  return t.expect(nonMatchingItems.length).eql(0, message);
}

/**
 * SOK-007 -Som användare ska jag kunna spara sökningar för att använda igen
 * This function performs new search based on input URL and compare with previous result
 * @param searchQueryUrl URL from the search containing all info to redo the search
 * @param results1 The expected result
 */
export async function expectSameResultAgain(searchQueryUrl: string, results1: IResult[]){ 
  //Navigate away to empty page to restart from scratch
  await t.navigateTo(`about:blank`);

  //Reload the URL containing the search query again
  await t.navigateTo(searchQueryUrl);
  let results2 = await resultView.getResults();

  //Compare the results from first and second search
  return expectEqualArrays(results1, results2, result => result.path);
}
