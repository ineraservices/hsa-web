export interface IName {
  name: string
}

export interface ISearchAreaOption extends IName {
  segments: string[]
}

export interface IResultSelector extends Selector {
  getResults(): Promise<IResult[]>;
}

export interface ISearchAreaSelector extends Selector {
  getSearchAreaOptions(): Promise<ISearchAreaOption[]>;
}

export interface IResult extends IName {
  type: string
  path: string
}

export interface ICountableSelector extends Selector {
  itemCount: Promise<number>
}
