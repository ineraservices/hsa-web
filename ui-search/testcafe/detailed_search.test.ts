import DetailedSearch from './pages/DetailedSearch';
import { t } from 'testcafe';
import { checkbox, uncheck, check } from './helpers/element_helper'
// @ts-ignore
import axeCheck from './axe'
import { createEmployee, Parent, revertApiCalls, createUnit, createFunction } from './testapi/orgapi';
import { IEmployee, IUnit, IHSAFunction } from './testapi/model';
import { getKeys, str } from './helpers/helpers';
import ResultView, { ObjectCategory } from './pages/ResultView';
import { expectExactlyOne, expectNo, expectSameResultAgain } from './helpers/expect_helpers';
import { matchesName } from './domain/rules';
import Filter from './pages/Filter';
import { randomString } from './helpers/random_data';

const page = new DetailedSearch();
const resultView = new ResultView();
const searchParams = page.searchParameters;
let tussilago: Parent = { county: 'Norrbottens län', org: 'Tussilagon' };
let empRazzil: IEmployee;
let unitFlisan: IUnit;
let funcAddeman: IHSAFunction;

/**
 * Testning av detaljerad sök-gränssnittet.
 * Här testas gränssnittets alla tillstånd beroende på svar
 * från backend och interaktion från användaren.
 * Detta betyder inte uttömmande testning av sök-APIt.
 */

fixture `SOK-001, SOK-003: Detaljerad sök`
  .page(page.URL)
  .afterEach(revertApiCalls);

test('WCAG Detaljerad sök', async t => await axeCheck(t));

/**
 * AKTIVERAS NÄR INDEXERING FÖR EMPLOYEE FUNGERAR KORREKT.
 * DSOK-001, DSOK-008: En sökterm i ett sökfält - Person
 * Lägger till en person med diverse attribut och kontrollerar
 * att den är sökbar med respektive attribut och sökfält.
 */
test.skip.before(async _ => {
  empRazzil = {
    nickName: 'Alch',
    givenName: 'Razzil',
    middleName: 'FarmarN',
    sn: 'Darkbrew',
    hsaIdentity: 'DSOK-001',
    title: 'Alkemist',
    hsaSosTitleCodeSpeciality: ['LK;20100;internmedicin'],
    postalAddress: { addressLine: ['Digergatan 1337A'] },
    street: 'Digergatan 1337A',
    mail: 'razzil@tusengpm.se',
    telephoneNumber: ['+46541234567'],
    mobile: ['0731234556'],
    hsaSwitchboardNumber: '+4612346670'
  };
  await createEmployee(tussilago, empRazzil);
})('DSOK-001, DSOK-008: En sökterm i ett sökfält - Person', async t => {
  for (let prop of getKeys(empRazzil)) {
    let inputField = page.getPropElement(prop);
    let value = str(empRazzil[prop]);
    await t.typeText(inputField, value)
      .click(page.btn_sok);

    let results = await resultView.getResults(ObjectCategory.Person);
    await expectExactlyOne(results, matchesName, 'Razzil');

    await t.click(resultView.newSearchLink)
      .selectText(inputField)
      .pressKey('delete');
  }
});

/**
 * AKTIVERAS NÄR INDEXERING FÖR UNIT FUNGERAR KORREKT.
 **/
/**
 * DSOK-001: Det ska vara möjligt att göra en detaljerad sökning med en sökterm i ett sökfält
 * DSOK-008: Via detaljerad sökning ska en användare kunna hitta Personer
 * SOK-007: Genom att spara undan URL'en när man fyllt i sökfälten kan man spara ett bookmark med 
 * sökinformationen. Både Det som skrivits i sökfältet och de alternativ som kryssats i
 * filtret ska följa med i urlen
 * Lägger till en enhet med diverse attribut och kontrollerar
 * att den är sökbar med respektive attribut och sökfält.
 **/
test.skip.before(async _ => {
  unitFlisan = {
    name: 'Flisan',
    hsaIdentity: 'ABC-123',
    postalAddress: { addressLine: ['Stömnegatan 3'] },
    street: 'Stömnegatan 3',
    mail: 'skruttan@flisan.se',
    telephoneNumber: ['+46541234567'],
    mobile: ['0731234556'],
    hsaSwitchboardNumber: '+4612346670'
  }
  await createUnit(tussilago, unitFlisan);
})('DSOK-001, DSOK-008, SOK-007: En sökterm i ett sökfält - Enhet, Ge användaren möjlighet att spara sin sökning', async t => {
  for (let prop of getKeys(unitFlisan)) {
    let inputField = page.getPropElement(prop);
    let value = str(unitFlisan[prop]);
    await t.typeText(inputField, value)
      .click(page.btn_sok);
    var searchQuery_url = await t.eval(() => window.location.href);
    let results = await resultView.getResults(ObjectCategory.OrgEllerEnhet);
    await expectExactlyOne(results, matchesName, 'Flisan');
    //Repetera samma sökning igen
    await expectSameResultAgain(searchQuery_url, results);
    await t.click(resultView.newSearchLink)
      .selectText(inputField)
      .pressKey('delete');
  }
});

/**
 * AKTIVERAS NÄR INDEXERING FÖR FUNCTION FUNGERAR KORREKT.
 * DSOK-001, DSOK-008: En sökterm i ett sökfält - Enhet
 * Lägger till en enhet med diverse attribut och kontrollerar
 * att den är sökbar med respektive attribut och sökfält.
 **/
test.skip.before(async _ => {
  funcAddeman = {
    name: 'Skrut Bakut',
    hsaIdentity: 'ABC-123',
    postalAddress: { addressLine: ['Stömnegatan 3'] },
    street: 'Stömnegatan 3',
    mail: 'skrut@bakut.se',
    telephoneNumber: ['+46541234567'],
    mobile: ['0731234556'],
    hsaSwitchboardNumber: '+4612346670'
  }
  await createFunction(tussilago, funcAddeman);
})('DSOK-001, DSOK-008: En sökterm i ett sökfält - Funktion', async t => {
  for (let prop of getKeys(funcAddeman)) {
    let inputField = page.getPropElement(prop);
    let value = str(funcAddeman[prop]);
    await t.typeText(inputField, value)
      .click(page.btn_sok);

    let results = await resultView.getResults(ObjectCategory.OrgEllerEnhet);
    await expectExactlyOne(results, matchesName, 'Skrut Bakut');

    await t.click(resultView.newSearchLink);
  }
});

/**
 * AKTIVERAS NÄR INDEXERING FÖR EMPLOYEE FUNGERAR KORREKT
 * DSOK-002, DSOK-008: Flera söktermer i ett sökfält - Person
 *
 * Lägger till en person med flera ord som värde i diverse attribut.
 * Söker sedan på en delmängd av dessa ord och kontrollerar
 * att personen dyker upp i resultatet.
 **/
test.skip.before(async _ => {
  empRazzil = {
    givenName: 'Kro Kon Kramp',
    sn: 'Skrank Skrink Skronk',
    title: 'Bink Bank Bonk',
    postalAddress: { addressLine: ['Digra gatan 1337A'] },
  };
  await createEmployee(tussilago, empRazzil);
})('DSOK-002, DSOK-008: Flera söktermer i ett sökfält - Person', async t => {
  for (let prop of getKeys(empRazzil)) {
    let inputField = page.getPropElement(prop);
    let value = firstAndLastWord(str(empRazzil[prop]));
    await t.typeText(inputField, value)
      .click(page.btn_sok);

    let results = await resultView.getResults(ObjectCategory.Person);
    await expectExactlyOne(results, matchesName, 'Kro Kon Kramp');

    await t.click(resultView.newSearchLink);
  }
});

/**
 * AKTIVERAS NÄR INDEXERING FÖR EMPLOYEE FUNGERAR KORREKT
 * DSOK-003: En sökning med flera sökfält - Person
 *
 * Lägger till en person A med diverse attribut, samt ett antal
 * personer som är en kopia av A men som var och en saknar ett av As attribut.
 * Fyller i samtliga As attribut i respektive sökfält och kontrollerar
 * att A hittas. Samtidigt ska ingen av "nästan-kopiorna" av A hittas.
 **/
test.skip.before(async _ => {
  empRazzil = {
    givenName: 'Razzil',
    sn: 'Darkbrew',
    hsaIdentity: 'DSOK-001',
    title: 'Alkemist',
    postalAddress: { addressLine: ['Digergatan 1337A'] },
    mail: 'razzil@tusengpm.se',
    telephoneNumber: ['+46541234567']
  };
  await createEmployee(tussilago, empRazzil);
  for (let prop of getKeys(empRazzil)) {
    let emp = Object.assign({}, empRazzil);
    delete emp[prop];
    emp.name = 'Timber ' + randomString(5);
    await createEmployee(tussilago, emp);
  }
})('DSOK-003: En sökning med flera sökfält - Person', async t => {
  for (let prop of getKeys(empRazzil)) {
    let inputField = page.getPropElement(prop);
    let value = str(empRazzil[prop]);
    await t.typeText(inputField, value);
  }
  await t.click(page.btn_sok);
  let results = await resultView.getResults(ObjectCategory.Person);
  await expectNo(results, matchesName, 'Timber');
  await expectExactlyOne(results, matchesName, 'Razzil');

});

/**
 * AKTIVERAS NÄR SÖKNING MED PRINCIPEN 'INNEHÅLLER' ÄR IMPLEMENTERAD
 * DSOK-004: Detaljerad sökning ska alltid ske enligt principen "innehåller" 
 **/
test.skip.before(async _ => {
  empRazzil = {
    givenName: 'Korriborriporri',
    sn: 'Skippidippibippi',
    title: 'Rikkiflikkidikki',
    postalAddress: { addressLine: ['Mankistankiplanki 1337A'] },
  };
  await createEmployee(tussilago, empRazzil);
})('DSOK-004: Enligt principen innehåller - Person', async t => {
  for (let prop of getKeys(empRazzil)) {
    let inputField = page.getPropElement(prop);
    let value = randomSubstring(str(empRazzil[prop]));
    await t.typeText(inputField, value)
      .click(page.btn_sok);

    let results = await resultView.getResults(ObjectCategory.Person);
    await expectExactlyOne(results, matchesName, 'Kro Kon Kramp');

    await t.click(resultView.newSearchLink);
  }
});

/**
 * DSOK-004: Wildcard-tecken (*) ska inte vara tillåtna
 * Testet försöker fylla i en asterisk i varje fält och kontrollerar
 * att det inte går.
 **/
test('DSOK-004: Wildcard-tecken (*) ska inte vara tillåtna', async t => {
  for (const inputField of page.propElementMap.values()) {
    await t.typeText(inputField, '*')
      .expect(inputField.value).eql('');
  }
});

/**
 * DSOK-005: Detaljerad sökning ska ske "fonetiskt" 
 * (enligt principen "liknande stavning")
 **/
//** Aktiveras när HSA-420 är klar */ 
test.skip.before(async _ => {
  empRazzil = {
    givenName: 'Razzil',
    sn: 'Darkbrew',
    hsaIdentity: 'DSOK-001',
    title: 'Alkemist',
    postalAddress: { addressLine: ['Digervägen 1337A'] },
    mail: 'razzil@tusengpm.se'
  };
  await createEmployee(tussilago, empRazzil);
})('DSOK-005: Fonetisk sökning (liknande stavning)', async t => {
  for (let prop of getKeys(empRazzil)) {
    let inputField = page.getPropElement(prop);
    let value = phoneticReplacements(str(empRazzil[prop]));
    await t.typeText(inputField, value)
      .click(page.btn_sok);

    let results = await resultView.getResults(ObjectCategory.Person);
    await expectExactlyOne(results, matchesName, 'Razzil');

    await t.click(resultView.newSearchLink);
  }
});

/**
 * DSOK-006: Verksamhet ska väljas genom dropdown-lista
 * Testet lägger till enheter med olika verksamhetskoder.
 * Sedan väljs en verksamhet som bara finns i ena enheten
 * och bara denna enhet förväntas finnas i resultaten.
 **/
test.before(async _ => {
  await createUnit(tussilago, {
    name: 'Bästa Naprapatstället',
    businessClassificationCode: ['1409', '1343']
  });
  await createFunction(tussilago, {
    name: 'Bästa Naprapatfunktionen',
    businessClassificationCode: ['1409', '1343']
  });
  await createUnit(tussilago, {
    name: 'Bästa Översiktsvårdsstället',
    businessClassificationCode: ['1343']
  });
  await createFunction(tussilago, {
    name: 'Bästa Översiktsvårdsfunktionen',
    businessClassificationCode: ['1343']
  });
})('DSOK-006: Verksamhet ska väljas genom dropdown-lista', async t => {
  await page.selectVerksamhet('Naprapat');
  await t.click(page.btn_sok)
    .click(resultView.orgUnitTab);

  let results = await resultView.getResults();
  await expectExactlyOne(results, matchesName, 'Bästa Naprapatstället');
  await expectExactlyOne(results, matchesName, 'Bästa Naprapatfunktionen');
  await expectNo(results, matchesName, 'Bästa Översiktsvårdsstället');
  await expectNo(results, matchesName, 'Bästa Översiktsvårdsfunktionen');
});

/**
 * IMPLEMENTERAS NÄR MEDARBETARUPPDRAG FINNS
 * DSOK-006: Medarbetaruppdragets ändamål ska väljas genom dropdown-lista
 * Testet lägger till två medarbetaruppdrag med olika medarbetaruppdragskoder.
 * Sedan väljs ett ändamål som bara finns i ena medarbetaruppdraget
 * och bara detta medarbetaruppdraget förväntas finnas i resultaten.
 **/
test.skip.before(async _ => {

})('DSOK-006: Verksamhet ska väljas genom dropdown-lista', async _ => {
  await page.selectMedarbetaruppdragsAndamal('Administration');
  
});

/**
 * DSOK-009: Kodverksattribut i "Titel"-fältet
 * Testet lägger till en person med koder i attributen:
 * - paTitleCode
 * Och kontrollerar att de är sökbara via deras
 * klartext-värden genom titel-fältet,
 * även när man söker på en del av titeln.
 **/
test.skip.before(async _ => {
  empRazzil = {
    name: 'Razzil',
    paTitleCode: ['151016']
  };
  await createEmployee(tussilago, empRazzil);
})('DSOK-009: Kodverksattribut i "Titel"-fältet', async _ => {
  let inputField = page.searchParameters.text_titel;
  let value = 'Handläggare';
  await t.typeText(inputField, value)
    .click(page.btn_sok);

  let results = await resultView.getResults(ObjectCategory.Person);
  await expectExactlyOne(results, matchesName, 'Razzil');

  await t.click(resultView.newSearchLink);
});

/**
 * DSOK-009: Kodverksattribut i "Plats"-fältet
 * Testet lägger till en enhet med koder i fälten:
 * - municipalityCode
 * - countyCode
 * Och kontrollerar att de är sökbara via deras
 * klartext-värde i plats-fältet
 **/
test.skip.before(async _ => {
  unitFlisan = {
    name: 'Flisan',
    municipalityCode: '2580',
    countyCode: '25'
  };
  await createUnit(tussilago, unitFlisan);
})('DSOK-009: Kodverksattribut i "Plats"-fältet', async _ => {
  let places = ['Norrbottens län', 'Luleå'];
  for (let i = 0; i < places.length; i++) {
    let inputField = page.searchParameters.text_plats;
    let value = places[i];
    await t.typeText(inputField, value)
      .click(page.btn_sok);

    let results = await resultView.getResults(ObjectCategory.OrgEllerEnhet);
    await expectExactlyOne(results, matchesName, 'Flisan');

    await t.click(resultView.newSearchLink);
  }
});

/**
 * DSOK-0010: Endast relevanta fält för filter-typer ska vara 
 * aktiverade för aktuella typer.
 **/
[Filter.PERSON_LABEL, Filter.ORG_UNIT_LABEL, Filter.COMMISSION_LABEL].forEach(type =>
  test(`DSOK-010: Endast fält relevanta för "${type}" ska vara aktiverade för "${type}"`, async t => {
    await t.click(page.sidebarToggler)
    await uncheck(checkbox(Filter.PERSON_LABEL));
    await uncheck(checkbox(Filter.ORG_UNIT_LABEL));
    await uncheck(checkbox(Filter.COMMISSION_LABEL));

    await check(checkbox(type));

    return expectFieldsToBeEnabledForType(type);
  })
);

async function expectFieldsToBeEnabledForType(type: string) {
  for (let field of getInactiveFieldsFor(type)) {
    let attr = await field.attributes;
    await t.expect(attr.disabled).eql('disabled', `Expect ${attr.id} to be disabled`);
  }

  for (let field of getActiveFieldsFor(type)) {
    let attr = await field.attributes;
    await t.expect(attr.disabled).notEql('disabled', `Expect ${attr.id} to be enabled`);
  }
}

function getInactiveFieldsFor(type: string) {
  switch (type) {
    case 'Person':
      return [searchParams.select_verksamhet, searchParams.text_orgEnhetMu, searchParams.select_muAndamal]
    case 'Organisation eller enhet':
      return [searchParams.text_tilltalsnamn, searchParams.text_efternamn, searchParams.text_titel, searchParams.select_muAndamal];
    case 'Medarbetaruppdrag':
      return [searchParams.text_tilltalsnamn, searchParams.text_efternamn, searchParams.text_titel, searchParams.select_verksamhet, searchParams.text_telefonnr, searchParams.text_adress, searchParams.text_plats, searchParams.text_epost];
    default:
      throw `Checkbox with name ${type} is not defined.`
  }
}

function getActiveFieldsFor(type: string) {
  let searchFields: Array<Selector> = [searchParams.text_tilltalsnamn, searchParams.text_efternamn,
    searchParams.text_orgEnhetMu, searchParams.text_hsaId,
    searchParams.text_titel, searchParams.text_plats,
    searchParams.text_telefonnr, searchParams.select_verksamhet,
    searchParams.text_adress, searchParams.select_muAndamal, searchParams.text_epost];
    return searchFields.filter(field => !getInactiveFieldsFor(type).includes(field));
}

// Dumb helper functions
let firstAndLastWord = (str: string) =>
  str.split(' ')[0] + ' ' + str.split(' ')[str.split(' ').length - 1];

let randomSubstring = (str: string) =>
  str.substring(Math.floor(Math.random() * str.length/2), Math.ceil(Math.random() * str.length/2 + str.length/2));

let phoneticReplacements = (str: string) =>
  str.replace('z', 's').replace('k', 'c').replace('ä', 'e');
