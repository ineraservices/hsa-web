import { expect } from 'chai'
import { shallowMount, config } from '@vue/test-utils'
import moxios from 'moxios'
import Footer from '@/components/footer/footer.vue'
import { DetailedSearch } from '../../src/services/detailedSearch'
// mock vue translate i18n
config.mocks['$t'] = () => {}
let wrapper = null
const msg = 'Inera'
// const detailedSearch2 = new DetailedSearch()
window.Promise = require('promise')
// mount the Footer component
beforeEach(() => {
  wrapper = shallowMount(Footer, {
    propsData: { msg }
  })
  moxios.install()
})

describe('Testing Footer.vue', () => {
  it('renders props.msg when passed', () => {
    expect(wrapper.text()).to.include(msg)
  })
  it('renders a vue instance', () => {
    expect(wrapper.isVueInstance()).eq(true)
  })
})
describe('Testing detailedSearch Service', () => {
  it('renders props.msg when passed', () => {
    expect(wrapper.text()).to.include(msg)
  })
  it('renders a vue instance', () => {
    expect(wrapper.isVueInstance()).eq(true)
  })
  /* it('should call backend API with given params', function () {
    let fakeData = {}
   // let searchObjectTypes = ['unit', 'organization']
   // let name = 'stockholms län'
    this.axios.onGet('http://localhost:9080/organization/resources/v0.3/search/sub/parent-path/countries/Sverige').replyOnce(200, fakeData)
    return this.detailedSearch.detailedSearch().then(function (response) {
      expect(response).to.deep.equal(fakeData)
    })
  }) */
  /*
  it('Test if detailed Search fetches the right object ', done => {
    moxios.stubRequest('http://localhost:9080/organization/resources/v0.3/search/sub/parent-path/countries/Sverige', {
      status: 200,
      response: {
        'countries': ['Sverige'], 'counties': ['stockholms län'], 'organizations': ['org1'], 'units': ['unit1']
      }
    })

    // const vm = shallowMount(detailedSearch) vm.data().them
    moxios.wait(function () {
      detailedSearch2.detailedSearch()
        .then(data => {
          expect(JSON.stringify(data)).to.contain('Sverige')
          expect(JSON.stringify(data.organizations)).to.contain('org1')
          expect(JSON.stringify(data.units)).to.contain('unit1')
        })
      done()
    })
  }) */
})

// teardown the mounted component
afterEach(() => {
  wrapper.destroy()
  moxios.uninstall()
})
