module.exports = {
  'globals': {
    '$': true,
    'jQuery': true,
    '_': true
  },
  env: {
    mocha: true
  }
}
