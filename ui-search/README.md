# ui-search

## Project setup
npm install
```
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run test suite

Being an end-to-end test suite, the Testcafé test suite requires a frontend endpoint with all of HSA deployed behind it, as well as a populated database with seed data (from hsa/data). Most of the test cases will use the organization API directly to setup data needed in the test. This means that while developing or in certain error situations, data might not be reset properly and must be done manually.

The runner is defined in test/testrunner.js. This file can be run like a node app, and there is a shorthand for it using an npm script:

```
npm run testcafe
```

This invokes the TestCafé test runner which is located in test/testrunner.js. It uses a configuration from the file test/config.js. These values can be overridden from command line, for example:

Running with a specific base URL (default is http://localhost:8080):

```
npm run testcafe -- --baseUrl=http://abc.com
```

##### Running a specific test

Run all tests with a name that includes the word "sök"
```
npm run testcafe -- --testName=sök
```

It is also possible to modify the testcase as such

```
test('Some test', async t => { ... })
// add ".only"
test.only('Some test', async t => { ... })
```

This will only run tests marked this way. **(Make sure you don't commit this!)**

### Run all tests in a specific webbrowser and specific windowsize

npm run testcafe -- --browser="chrome --window-size=400,300"

### WCAG

Tests with a name containing "WCAG" (uppercase) can be run separately. These include a call to axe which using an injection of javascript checks the current page against the WCAG standards.

```
npm run testcafe -- --testName=WCAG
```

### Page-objects

The Selector-object in TestCafé is used to select elements in the DOM. Selectors should only be used in Page-objects (located in test/pages) which should be included in the test-files as needed. This is to make sure there is but one definition of how to find a certain element in the DOM.
The page Objects should also always include a URI-property, defining the path to the page from the baseURL.


### Lints and fixes files
```
npm run lint
```
