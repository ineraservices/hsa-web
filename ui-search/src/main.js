import Vue from 'vue';
import App from './App.vue';
import router from './router';
import { store } from './store/store';
import Storage from 'vue-web-storage';
import { i18n } from './config/UIText';
import VueLodash from 'vue-lodash';
import VueSessionStorage from 'vue-sessionstorage';
import 'bootstrap/dist/js/bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import Multiselect from 'vue-multiselect';
import 'vue-multiselect/dist/vue-multiselect.min.css';

import './assets/css/main.css';

// require('font-awesome/css/font-awesome.min.css')
require('@fortawesome/fontawesome-free/css/solid.min.css');
require('@fortawesome/fontawesome-free/css/fontawesome.min.css');

// register globally
Vue.component('multiselect', Multiselect);
Vue.component(VueLodash);
Vue.use(VueSessionStorage);

Vue.use(Storage, {
  prefix: 'HSA_', // default `app_`
  drivers: ['session', 'local'] // default 'local'
});

const jquery = require('jquery');
window.$ = window.jQuery = jquery;
// load the plugins:
require('./assets/js/functions_jquery.js');

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  Storage,
  VueSessionStorage,
  render: h => h(App)
}).$mount('#app');
