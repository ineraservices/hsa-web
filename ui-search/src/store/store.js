
import Vue from 'vue'
import Vuex from 'vuex'

// Export a Vuex store object using the Vuex.Store() API
Vue.use(Vuex)

export const store = new Vuex.Store({
  // create a state property object to collect some other properties of components
  state: {
    searchInput: '',
    showBox: false,
    showFilter: false,
    resultView: false,
    className: 'hsa-bg-dcd9d6',
    showingDetailedSearch: false,
    message: '',
    loggedInUser: '',
    searchResults: [],
    codeSystemCodes: [],
    exactMatch: false,
    searchLevel: 'countries/Sverige',
    selectedSearchArea: { name: 'Hela Sverige', path: 'countries/Sverige' },
    searchTypes: ['Person', 'Organisation eller enhet', 'Medarbetaruppdrag'],
    selectedObject: {
      name: 'Enhet Eller Person',
      hsaIdentity: 'HsaId här',
      telephoneNumber: ['+47 ss sss ss ss'],
      mail: 'epost här',
      hsaVpwWebPage: 'Klicka här',
      path: ''
    },
    // Detailed search parameter values
    givenName: '',
    lastName: '',
    title: '',
    // Employee and unit
    telephone: '',
    // both street and postal addresses for both Employee and unit
    address: '',
    email: '',
    hsaId: '',
    organizationOrUnitName: '',
    hsaIdentity: '',
    municipality: '',
    unit_org_func: '',
    // person eller enhet, Geografisk plats (l), Kommun (klartext av municipalityCode), Län (klartext av countyCode
    place: '',
    selectedBusinessClassificationCode: '',
    // vårdForm
    hsaBusinessType: '',
    // medarbetareuppdrag, hsaCommissionMember
    HsaCommission: '',
    assignmentGoal: '',
    // medarbetaruppdrags ändamål codes list (hsaCommissionMember)
    assignmentGoalOptions: [],
    // Verksamhet list
    businessClassificationCodeOptions: [],
    selectedHsaCommissionMemberCode: '',
    // end of detailed search parameter values
    accessToken: localStorage.getItem('access_token') || '',
    currentUser: { id: 1, username: 'test', password: 'test', role: 'user', firstName: 'Test', lastName: 'User' }
  },
  // the state cannot be manipulated except by mutations. So, we add a mutation through which components will notify state changes
  mutations: {
    setSearchInput (state, searchInput) {
      state.searchInput = searchInput
    },
    setExactMatch (state, exactMatch) {
      state.exactMatch = exactMatch
    },
    setSearchLevel (state, searchLevel) {
      state.searchLevel = searchLevel
    },
    setSelectedOption (state, selectedSearchArea) {
      state.selectedSearchArea = selectedSearchArea
    },
    setSelectedObject (state, selectedObject) {
      state.selectedObject = selectedObject
    },
    setLoggedInUser (state, user) {
      state.loggedInUser = user
    },
    setShowBox (state, showBox) {
      state.showBox = showBox
      // state.showBox = Object.assign({}, state.showBox, showBox)
    },
    setShowFilter (state, showFilter) {
      state.showFilter = showFilter
      // state.showBox = Object.assign({}, state.showBox, showBox)
    },
    setClassName (state, className) {
      state.className = className
      // state.showBox = Object.assign({}, state.showBox, showBox)
    },
    setShowingDetailedSearch (state, showingDetailedSearch) {
      state.showingDetailedSearch = showingDetailedSearch
    },
    setResultView (state, resultView) {
      state.resultView = resultView
    },
    setMessage (state, message) {
      state.message = message
    },
    setSearchResults (state, data) {
      // console.log('storing data: ' + JSON.stringify(data))
      state.searchResults = data
    },
    setSearchTypes (state, data) {
      // console.log('storing data: ' + JSON.stringify(data))
      state.searchTypes = data
      // console.log('stored data: ' + JSON.stringify(state.searchResults))
    },
    pushToSearchResults (state, data) {
      state.searchResults.push(data)
    },
    setCodeSystemCodes (state, data) {
      state.codeSystemCodes = data
    },
    setAccessToken (state, data) {
      state.codeSystemCodes = data
    },
    setCurrentUser (state, data) {
      state.codeSystemCodes = data
    },
    // detailed searh parameter values
    setGivenName (state, data) {
      state.givenName = data
    },
    setLastName (state, data) {
      state.lastName = data
    },
    setTitle (state, data) {
      state.title = data
    },
    setTelephone (state, data) {
      state.telephone = data
    },
    setAddress (state, data) {
      state.address = data
    },
    setEmail (state, data) {
      state.email = data
    },
    setHsaId (state, data) {
      state.hsaId = data
    },
    setOrganizationOrUnitName (state, data) {
      state.organizationOrUnitName = data
    },
    setHsaIdentity (state, data) {
      state.hsaIdentity = data
    },
    setMunicipality (state, data) {
      state.municipality = data
    },
    setUnit_org_func (state, data) {
      state.municipality = data
    },
    setPlace (state, data) {
      state.place = data
    },
    setSelectedBusinessClassificationCode (state, data) {
      state.selectedBusinessClassificationCode = data
    },
    setHsaBusinessType (state, data) {
      state.hsaBusinessType = data
    },
    setHsaCommission (state, data) {
      state.HsaCommission = data
    },
    setAssignmentGoal (state, data) {
      state.assignmentGoal = data
    },
    setAssignmentGoalOptions (state, data) {
      state.assignmentGoalOptions = data
    },
    setBusinessClassificationCodeOptions (state, data) {
      state.businessClassificationCodeOptions = data
    },
    setSelectedHsaCommissionMemberCode (state, data) {
      state.selectedHsaCommissionMemberCode = data
    }
    // end of detailed search parameters setters
  },
  // To have a look at the states, we need to add getters object (gets properties)
  getters: {
    // searchInput is a property which accepts 'state' as the parameter and returns the searchInput property of the state.
    getSearchInput: state => state.searchInput,
    getSearchFilter: state => state.searchFilter,
    getShowBox: state => state.showBox,
    getShowFilter: state => state.showFilter,
    getClassName: state => state.className,
    getResultView: state => state.resultView,
    getShowingDetailedSearch: state => state.showingDetailedSearch,
    getMessage: state => state.message,
    getSearchResults: state => state.searchResults,
    getCodeSystemCodes: state => state.codeSystemCodes,
    getLoggedInUser: state => state.loggedInUser,
    getExactMatch: state => state.exactMatch,
    getSearchLevel: state => state.searchLevel,
    getSelectedOption: state => state.selectedSearchArea,
    getSelectedObject: state => state.selectedObject,
    getAccessToken: state => state.accessToken,
    getCurrentUser: state => state.currentUser,
    // detailed search parameter getters
    getGivenName: state => state.givenName,
    getLastName: state => state.lastName,
    getTitle: state => state.title,
    getTelephone: state => state.telephone,
    getAddress: state => state.address,
    getEmail: state => state.email,
    getHsaId: state => state.hsaId,
    getOrganizationOrUnitName: state => state.organizationOrUnitName,
    getHsaIdentity: state => state.hsaIdentity,
    getMunicipality: state => state.municipality,
    getUnit_org_func: state => state.unit_org_func,
    getPlace: state => state.place,
    getSelectedBusinessClassificationCode: state => state.selectedBusinessClassificationCode,
    getHsaBusinessType: state => state.hsaBusinessType,
    getHsaCommission: state => state.HsaCommission,
    getAssignmentGoal: state => state.assignmentGoal,
    getAssignmentGoalOptions: state => state.assignmentGoalOptions,
    getBusinessClassificationCodeOptions: state => state.businessClassificationCodeOptions,
    getSelectedHsaCommissionMemberCode: state => state.selectedHsaCommissionMemberCode
    // end of detailed search parameter getters
  }
})
