// If using a module system (e.g. via vue-cli), import Vue and VueI18n and then call Vue.use(VueI18n).
import Vue from 'vue';
import VueI18n from 'vue-i18n';
//
Vue.use(VueI18n);

export const i18n = new VueI18n({
  locale: 'sv', // default locale
  messages: {
    sv: {
      search: 'sök',
      browse: 'bläddra',
      administrate: 'administrera',
      logIn: 'Logga in',
      logout: 'Logga ut',
      unit: 'enhet',
      searchResult: 'Sökresultat',
      searchHere: 'Sök här',
      simpleSearch: 'enkel sökning',
      simpleSearching: 'Enkel sökning',
      moreAboutHsa: 'Mer om HSA',
      help: 'Hjälp',
      support: 'Support',
      questions: 'Frågor',
      language: 'Språk',
      contactYourLocalHSaAdminOrHsaAgent:
        'Kontakta din lokala HSA-förvaltning eller ditt HSA-ombud',
      readMoreAndFilter: 'Läs mer och filtrera',
      person: 'Person',
      persons: 'Personer',
      units: 'enheter',
      detailedSearch: 'Detaljerad sökning',
      detailedSearching: 'detaljerad sökning',
      givenName: 'Tilltalsnamn',
      lastName: 'Efternamn',
      lastNamePlaceHolder: 't.ex. Andersson',
      givenNamePlaceHolder: 't.ex. Eva',
      instead: 'istället',
      title: 'Titel',
      titlePlaceHolder: 't.ex. Sjuksköterska eller Överläkare',
      address: 'Adress',
      addressPlaceHolder: 't.ex. Bondegatan 12',
      email: 'E-post',
      emailPlaceHolder: 't.ex. eva@exampel.se',
      place: 'Plats',
      placePlaceHolder: 't.ex. stad eller Skåne',
      or: 'eller',
      HsaCommission: 'Medarbetaruppdrag',
      businessClassification: 'Verksamhet',
      businessClassificationPlaceHolder:
        'Välj i listan eller skriv de tre första tecknen',
      hsaCommissionMemberPlaceHolder:
        'Välj i listan eller skriv de tre första tecknen',
      telephone: 'Telefonnummer',
      hsaId: 'HSA-id',
      telephonePlaceHolder: 't.ex. 0812345678 eller 070-111 11 11',
      name: 'Namn',
      on: 'på',
      hsaIdPlaceHolder: 't.ex. SE1234567890-ABCD',
      organisation: 'Organisation',
      function: 'funktion'
    },
    en: {
      search: 'Search',
      browse: 'Browse',
      administrate: 'Administer',
      logIn: 'login',
      logout: 'logout',
      unit: 'unit',
      searchResult: 'Search result',
      searchHere: 'Search here',
      moreAboutHsa: 'More about Hsa',
      help: 'Help',
      support: 'Support',
      questions: 'Questions',
      language: 'Language',
      contactYourLocalHSaAdminOrHsaAgent:
        'Contact your local HSA-administration or your HSA-Agent',
      readMoreAndFilter: 'Read more and filter',
      person: 'Person',
      persons: 'Persons',
      units: 'Units',
      detailedSearch: 'Detailed Search',
      detailedSearching: 'Detailed Searching',
      givenName: 'Given Name',
      givenNamePlaceHolder: 't.ex. Eva',
      lastName: 'Last Name',
      lastNamePlaceHolder: 't.ex Andersson',
      instead: 'instead',
      title: 'title',
      titlePlaceHolder: 'eg. nurse or consultant',
      address: 'Address',
      addressPlaceHolder: 'eg. Bondegatan 12',
      email: 'Email',
      emailPlaceHolder: 'eg. eva@example.se',
      place: 'place',
      placePlaceHolder: 'eg. city or Skåne',
      or: 'or',
      HsaCommission: 'Employee Assignment',
      businessClassification: 'Occupation',
      businessClassificationPlaceHolder:
        'Choose from the list or write the first three letters',
      hsaCommissionMemberPlaceHolder:
        'Välj i listan eller skriv de tre första tecknen',
      telephone: 'telephone',
      telephonePlaceHolder: 'eg. 0812345678 or 070-111 11 11',
      name: 'Name',
      on: 'on',
      hsaIdPlaceHolder: 'eg. SE1234567890-ABCD',
      organisation: 'organisation',
      function: 'function'
    }
  }
});
i18n.locale =
  localStorage.getItem('locale') === null ? 'sv'
    : localStorage.getItem('locale');
export const UiTextService = {
  setLocale
};

export function setLocale(locale) {
  localStorage.setItem('locale', locale);
  console.log(localStorage.getItem('locale'));
  i18n.locale = locale;
}
