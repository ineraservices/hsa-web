// REPLACE JQUERY ALTOGETHER? YES!!!!

// Add padlock icon to disabled fields - START

window.$(function ($) {
  window
    .$('.form-control:disabled')
    .after('<i class="fa fa-lock fa-lg disabled-icon" aria-hidden="true"></i>');
  window.$('.disabled-icon').css({
    color: '#ced4da',
    float: 'right',
    padding: '0 .5em',
    position: 'relative',
    top: '-2em'
  });
});

// Add padlock icon to disabled fields - STOP

// WCAG - START

window.$(function ($) {
  $('body').prepend(
    '<a href="#main-content-wrapper" class="sr-only sr-only-focusable"><i class="fa fa-arrow-down" aria-hidden="true"></i>Hoppa till huvudinnehåll</a>'
  );
});

// WCAG - STOP
