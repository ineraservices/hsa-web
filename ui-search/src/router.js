/*
The vue router defines all of the routes for the application, and contains a function that runs before each route change
to prevent unauthenticated users from accessing restricted routes.
 */
import Vue from 'vue';
import Router from 'vue-router';
import simpleSearch from './components/simpleSearch.vue';
import resultsView from './components/resultsView.vue';
import objectDetailsView from './components/objectDetailsView/objectDetailsView.vue';
import detailedSearch from './components/detailedSearch/detailedSearch.vue';
import Login from './components/login';
import Admin from './components/admin';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: simpleSearch,
      props: true,
      meta: {
        breadcrumb: 'Hsa'
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        breadcrumb: 'Login'
      }
    },
    {
      path: '/admin',
      name: 'admin',
      component: Admin,
      meta: {
        breadcrumb: 'Login'
      }
    },
    {
      path: '/simpleSearch',
      name: 'search',
      component: simpleSearch,
      props: true,
      meta: {
        breadcrumb: {
          label: 'search',
          parent: 'Home'
        }
      },
      children: [
        {
          path: 'organizations',
          name: 'organizations',
          meta: { linkText: 'organisationer' }
        }
      ]
    },
    {
      path: '/showResults',
      name: 'showResults',
      component: simpleSearch,
      meta: {
        breadcrumb: {
          label: 'Search Results',
          parent: 'Home'
        }
      }
    },
    {
      path: '/detailedSearch',
      name: 'detailedSearch',
      component: detailedSearch,
      meta: {
        breadcrumb: {
          label: 'Detailed Search',
          parent: 'Home'
        }
      }
    },
    {
      path: '/resultsView',
      name: 'resultsView',
      component: resultsView,
      meta: {
        breadcrumb: {
          label: 'ResultsView',
          parent: 'Home'
        }
      }
    },
    {
      path: '/objectDetailsView',
      name: 'objectDetailsView',
      component: objectDetailsView,
      meta: {
        breadcrumb: {
          label: 'objectDetailsView',
          parent: 'Home'
        }
      }
    }
  ]
});
export default router;

/*
Router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login', '/register']
  const authRequired = !publicPages.includes(to.path)
  const loggedIn = localStorage.getItem('user')

  if (authRequired && !loggedIn) {
    return next('/login')
  }

  next()
})
*/

let isFirstTransition = true;
let LS_ROUTE_KEY = '/';
let LS_LAST_ACTIVITY_AT_KEY = '/';
let MAX_TIME_TO_RETURN = Date.now();

router.beforeEach((to, from, next) => {
  const lastRouteName = localStorage.getItem(LS_ROUTE_KEY);
  const lastActivityAt = localStorage.getItem(LS_LAST_ACTIVITY_AT_KEY);

  const hasBeenActiveRecently = Boolean(
    lastActivityAt && Date.now() - Number(lastActivityAt) < MAX_TIME_TO_RETURN
  );

  const shouldRedirect = Boolean(
    to.name === 'home' &&
      lastRouteName &&
      isFirstTransition &&
      hasBeenActiveRecently
  );

  if (shouldRedirect) next({ name: lastRouteName });
  else next();

  isFirstTransition = false;
});

router.afterEach(to => {
  localStorage.setItem(LS_ROUTE_KEY, to.name);
  localStorage.setItem(LS_LAST_ACTIVITY_AT_KEY, Date.now());
});
