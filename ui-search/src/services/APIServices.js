import axios from 'axios';
import { ErrorHandler } from '@/services/ErrorHandler';

const errorHandler = new ErrorHandler();
const ORG_API_URL = `${process.env.VUE_APP_ORGANIZATION_API_URL}`;
export class APIServices {
  // methods
  getOrganizations() {
    return axios
      .get(`${process.env.VUE_APP_ORGANIZATION_API_URL}`)
      .then(response => response.data)
      .catch(error => {
        // Error
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message);
        }
        console.log(error.config);
      });
  }
  orgApiDetailedSearch(searchParam) {
    console.log(
      'inside org Api' +
        process.env.NODE_ENV +
        encodeURI(ORG_API_URL + '/v0.3/search/free-text/' + searchParam)
    );
    // return axios.get(encodeURI(`${process.env.VUE_APP_ORGANIZATION_API_URL}/v0.3/search/free-text/${searchParam}`))
    return axios
      .get(
        encodeURI(
          `${
            process.env.VUE_APP_ORGANIZATION_API_URL
          }/v0.3/search/free-text/${searchParam}`
        )
      )
      .then(response => response.data)
      .catch(error => {
        // Error
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message);
        }
        console.log(error.config);
      });
  }
  // organizationApi free text search with filters
  orgApiSimpleSearch(searchParam) {
    // replace/escape special characters
    return axios
      .get(
        encodeURI(
          `${
            process.env.VUE_APP_ORGANIZATION_API_URL
          }/v0.3/search/simple/${searchParam}`
        )
      )
      .then(response => response.data)
      .catch(error => {
        // Error
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message);
        }
        console.log(error.config);
      });
  }
  // fetch all employees
  getEmployees() {
    return axios
      .get(`${process.env.VUE_APP_EMPLOYEE_API_URL}`)
      .then(response => response.data);
  }
  // employee free text search
  searchEmployees(text, offset = 0, size = 200) {
    return axios
      .get(
        `${
          process.env.VUE_APP_EMPLOYEE_API_URL
        }/search/${text}?offset=${offset}&size=${size}`
      )
      .then(response => response.data);
  }
  // get codes system codes by oid
  getCodeSystemCodesByOid(oid) {
    return axios
      .get(`${process.env.VUE_APP_CODE_SYSTEM_API_URL}/${oid}/codes`)
      .then(response => response.data)
      .catch(error => {
        errorHandler.handleError(error);
      });
  }
  // graphql query, search in organization api
  async searchViaGraphQl(requestBody) {
    const response = await axios
      .post(
        // GRAPHQL_API,
        process.env.VUE_APP_GRAPHQL_API_URL,
        requestBody,
        {
          headers: {
            'Content-Type': 'application/json'
          }
        }
      )
      .then(response => response.data)
      .catch(error => {
        // errorHandler.handleError(error)
        console.log('Error: ' + error);
      });
    // console.log('inside querybuilder ' + JSON.stringify(response.data));
    return response;
  }
}
