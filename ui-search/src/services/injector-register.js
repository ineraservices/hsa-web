import injector from 'vue-inject'

import setLocale from '@/config/UiText'

injector.service('setLocale', function () {
  return setLocale
})
