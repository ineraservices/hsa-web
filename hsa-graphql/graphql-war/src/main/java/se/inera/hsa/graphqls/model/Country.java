package se.inera.graphqls.model;

import io.leangen.graphql.annotations.GraphQLQuery;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

public class Country  {


    @GraphQLQuery(name = "isoCode", description = "Country isoCode")
    public String isoCode;

    @GraphQLQuery(name = "name", description = "Country name")
    public String name;

    @GraphQLQuery(name = "type", description = "Country type")
    public String type;

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

