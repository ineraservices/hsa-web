package se.inera.graphqls.api;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import io.leangen.graphql.GraphQLSchemaGenerator;
import io.leangen.graphql.metadata.strategy.query.AnnotatedResolverBuilder;
import io.leangen.graphql.metadata.strategy.value.jackson.JacksonValueMapperFactory;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.openapi.annotations.Operation;
import se.inera.graphqls.service.GraphqlService;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.Map;


/**
 * @author Tewelle W.
 */
@Path("/search")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class GraphqlResource {

    @Inject
    GraphqlService service;

    private static GraphQL graphQL;

    @PostConstruct
    void initiateGraphQl() {
        GraphQLSchema schema = new GraphQLSchemaGenerator()
                .withResolverBuilders(
                        //Resolve by annotations
                        new AnnotatedResolverBuilder())
                .withOperationsFromSingleton(service)
                .withValueMapperFactory(new JacksonValueMapperFactory())
                .generate();
        graphQL = GraphQL.newGraphQL(schema).build();
    }

    @GET
    @Operation(summary = "Do nothing",
            description = "Do nothing")
    public void doGet() {
    }

    @POST
    @Operation(summary = "Request graphql structured data",
            description = "A simple query structure looks like:\n" +
                    "{\"query\": \"{ organizations { id name hsaId description route} units { name hsaIdentity countyName } employee (id: 1) {id} employees { id name} }\" , \"variables\": \"{}\", \"operationName\":\"\"} " +
                    "Even more fields as below:" +
                    "{\"query\": \"{ organizations { id name hsaId description route} units { name hsaIdentity countyName } functions { name hsaIdentity countyName } employee (id: 1) {id} employees { id name} search (freetext: \\\"stockholm\\\") { units { name hsaIdentity }  functions { name } organizations {name} countries { name } counties { name} }   } \" , \"variables\": \"{}\", \"operationName\":\"\"}")

    public Map<String, Object> doPost(Map<String, String> request, @Context HttpServletRequest contextObject) {
        System.out.println("request query: " + request);
        ExecutionResult executionResult =  graphQL.execute(ExecutionInput.newExecutionInput()
                .query(request.get("query"))
                .operationName(request.get("operationName"))
                .context(contextObject)
                .build());
        return executionResult.toSpecification();
    }

    @PUT
    @Operation(summary = "Do nothing",
            description = "Do nothing")
    public void doPut() {
    }

}
