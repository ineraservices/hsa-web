package se.inera.graphqls.model;

import io.leangen.graphql.annotations.GraphQLQuery;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.json.bind.JsonbBuilder;
import java.util.List;

/**
 * @author Tewelle W.
 */

public class Employee {
    @GraphQLQuery
    private String uuid ;

    @GraphQLQuery
    private String type;

    @GraphQLQuery(name = "hsaIdentity", description = "Employee's hsaIdentity")
    private String hsaIdentity;

    @GraphQLQuery(name = "organizationId", description = "Employee's organization")
    private Long organizationId;

    @GraphQLQuery(name = "departmentId", description = "Employee's department")
    private Long departmentId;

    @GraphQLQuery(name = "name", description = "Employee name")
    private String name;

    @GraphQLQuery(name = "givenName", description = "Employee givenName")
    private String givenName;

    @GraphQLQuery(name = "middleName", description = "Employee middleName")
    private String middleName;

    @GraphQLQuery(name = "fullName", description = "Employee fullName")
    private String fullName;

    @GraphQLQuery(name = "sn", description = "Employee sn")
    private String sn;

    @GraphQLQuery(name = "age", description = "Employee age")
    private int age;

    @GraphQLQuery(name = "position", description = "Employee position")
    private String position;

    @GraphQLQuery(name = "salary", description = "Employee salary")
    private int salary;
// new 
    @GraphQLQuery
    private List<String> objectClass;
    @GraphQLQuery
    private List<String> cardNumber; // certattribut
    @GraphQLQuery
    private String dn; // obligatoriskt m.a.p. persistence?
    @GraphQLQuery
    private String cn; //obligatoriskt
    @GraphQLQuery
    private String createTimeStamp;
    @GraphQLQuery
    private String creatorsName;
    @GraphQLQuery
    private String description;
    @GraphQLQuery
    private String endDate;
    @GraphQLQuery
    private List<String> groupMembership;
    @GraphQLQuery
    private String hospIdentityNumber;
    @GraphQLQuery
    private String hsaAdminComment;
    @GraphQLQuery
    private String hsaAltText;
    @GraphQLQuery
    private List<String> hsaDestinationIndicator;
    @GraphQLQuery
    private String hsaEmigratedPerson;
    @GraphQLQuery
    private List<String> hsaGroupPrescriptionCode;
    @GraphQLQuery
    private String hsaManagerCode;
    @GraphQLQuery
    private List<String> hsaMifareSerialNumber;
    @GraphQLQuery
    private String hsaPassportBirthDate;
    @GraphQLQuery
    private String hsaPassportNumber;
    @GraphQLQuery
    private String hsaPassportValidThru;
    @GraphQLQuery
    private List<String> hsaPhoneticSearch;
    @GraphQLQuery
    private String hsaProtectedPerson;
    @GraphQLQuery
    private List<String> hsaSimpleSearch;
    @GraphQLQuery
    private List<String> hsaSosNursePrescriptionRight;
    @GraphQLQuery
    private List<String> hsaSosTitleCodeSpeciality;
    @GraphQLQuery
    private List<String> hsaSystemRole;
    @GraphQLQuery
    private List<String> hsaSyncId;
    @GraphQLQuery
    private String hsaSwitchboardNumber;
    @GraphQLQuery
    private List<String> hsaTextTelephoneNumber;
    @GraphQLQuery
    private List<String> hsaTelephoneNumber;
    @GraphQLQuery
    private List<String> hsaTitle;
    @GraphQLQuery
    private String hsaVerifiedIdentityNumber;
    @GraphQLQuery
    private String initials;
    @GraphQLQuery
    private String jpegPhoto;
    @GraphQLQuery
    private String mail;
    @GraphQLQuery
    private List<String> mobile;
    @GraphQLQuery
    private String modifiersName;
    @GraphQLQuery
    private String modifyTimeStamp;
    @GraphQLQuery
    private String nickName;
    @GraphQLQuery
    private List<String> occupationalCode;
    @GraphQLQuery
    private List<String> pager;
    @GraphQLQuery
    private List<String> paTitleName;
    @GraphQLQuery
    private List<String> paTitleCode;
    @GraphQLQuery
    private String personalIdentityNumber; //obligatoriskt om finns (utlaendska personposter har passnummer istaellet)
    @GraphQLQuery
    private String personalPrescriptionCode;
    @GraphQLQuery()
    @JsonDeserialize()
    public PostalAddress postalAddress;
    @GraphQLQuery
    private List<String> seeAlso;
    @GraphQLQuery
    private List<String> serialNumber; //certattribut
    @GraphQLQuery
    private List<String> specialityName;
    @GraphQLQuery
    private List<String> specialityCode;
    @GraphQLQuery
    private String startDate;
    @GraphQLQuery
    private String street;
    @GraphQLQuery
    private List<Hours> telephoneHours;
    @GraphQLQuery
    private List<String> telephoneNumber;
    @GraphQLQuery
    private String title;
    @GraphQLQuery
    private List<String> userCertificate; // certAttribut
    @GraphQLQuery
    private String userPassword;
    @GraphQLQuery
    private String userPrincipalName;
    @GraphQLQuery
    private List<String> validNotBefore; // certAttribut
    @GraphQLQuery
    private List<String> validNotAfter; // certAttribut
    // new
    @GraphQLQuery
    @JsonDeserialize
    private MetaData metaData ;



    public Employee() {
    }


    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String toJson() {
        return JsonbBuilder.create().toJson(this);
    }

    public String getHsaIdentity() {
        return hsaIdentity;
    }

    public void setHsaIdentity(String hsaIdentity) {
        this.hsaIdentity = hsaIdentity;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public List<String> getObjectClass() {
        return objectClass;
    }

    public void setObjectClass(List<String> objectClass) {
        this.objectClass = objectClass;
    }

    public List<String> getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(List<String> cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getDn() {
        return dn;
    }

    public void setDn(String dn) {
        this.dn = dn;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getCreateTimeStamp() {
        return createTimeStamp;
    }

    public void setCreateTimeStamp(String createTimeStamp) {
        this.createTimeStamp = createTimeStamp;
    }

    public String getCreatorsName() {
        return creatorsName;
    }

    public void setCreatorsName(String creatorsName) {
        this.creatorsName = creatorsName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<String> getGroupMembership() {
        return groupMembership;
    }

    public void setGroupMembership(List<String> groupMembership) {
        this.groupMembership = groupMembership;
    }

    public String getHospIdentityNumber() {
        return hospIdentityNumber;
    }

    public void setHospIdentityNumber(String hospIdentityNumber) {
        this.hospIdentityNumber = hospIdentityNumber;
    }

    public String getHsaAdminComment() {
        return hsaAdminComment;
    }

    public void setHsaAdminComment(String hsaAdminComment) {
        this.hsaAdminComment = hsaAdminComment;
    }

    public String getHsaAltText() {
        return hsaAltText;
    }

    public void setHsaAltText(String hsaAltText) {
        this.hsaAltText = hsaAltText;
    }

    public List<String> getHsaDestinationIndicator() {
        return hsaDestinationIndicator;
    }

    public void setHsaDestinationIndicator(List<String> hsaDestinationIndicator) {
        this.hsaDestinationIndicator = hsaDestinationIndicator;
    }

    public String getHsaEmigratedPerson() {
        return hsaEmigratedPerson;
    }

    public void setHsaEmigratedPerson(String hsaEmigratedPerson) {
        this.hsaEmigratedPerson = hsaEmigratedPerson;
    }

    public List<String> getHsaGroupPrescriptionCode() {
        return hsaGroupPrescriptionCode;
    }

    public void setHsaGroupPrescriptionCode(List<String> hsaGroupPrescriptionCode) {
        this.hsaGroupPrescriptionCode = hsaGroupPrescriptionCode;
    }

    public String getHsaManagerCode() {
        return hsaManagerCode;
    }

    public void setHsaManagerCode(String hsaManagerCode) {
        this.hsaManagerCode = hsaManagerCode;
    }

    public List<String> getHsaMifareSerialNumber() {
        return hsaMifareSerialNumber;
    }

    public void setHsaMifareSerialNumber(List<String> hsaMifareSerialNumber) {
        this.hsaMifareSerialNumber = hsaMifareSerialNumber;
    }

    public String getHsaPassportBirthDate() {
        return hsaPassportBirthDate;
    }

    public void setHsaPassportBirthDate(String hsaPassportBirthDate) {
        this.hsaPassportBirthDate = hsaPassportBirthDate;
    }

    public String getHsaPassportNumber() {
        return hsaPassportNumber;
    }

    public void setHsaPassportNumber(String hsaPassportNumber) {
        this.hsaPassportNumber = hsaPassportNumber;
    }

    public String getHsaPassportValidThru() {
        return hsaPassportValidThru;
    }

    public void setHsaPassportValidThru(String hsaPassportValidThru) {
        this.hsaPassportValidThru = hsaPassportValidThru;
    }

    public List<String> getHsaPhoneticSearch() {
        return hsaPhoneticSearch;
    }

    public void setHsaPhoneticSearch(List<String> hsaPhoneticSearch) {
        this.hsaPhoneticSearch = hsaPhoneticSearch;
    }

    public String getHsaProtectedPerson() {
        return hsaProtectedPerson;
    }

    public void setHsaProtectedPerson(String hsaProtectedPerson) {
        this.hsaProtectedPerson = hsaProtectedPerson;
    }

    public List<String> getHsaSimpleSearch() {
        return hsaSimpleSearch;
    }

    public void setHsaSimpleSearch(List<String> hsaSimpleSearch) {
        this.hsaSimpleSearch = hsaSimpleSearch;
    }

    public List<String> getHsaSosNursePrescriptionRight() {
        return hsaSosNursePrescriptionRight;
    }

    public void setHsaSosNursePrescriptionRight(List<String> hsaSosNursePrescriptionRight) {
        this.hsaSosNursePrescriptionRight = hsaSosNursePrescriptionRight;
    }

    public List<String> getHsaSosTitleCodeSpeciality() {
        return hsaSosTitleCodeSpeciality;
    }

    public void setHsaSosTitleCodeSpeciality(List<String> hsaSosTitleCodeSpeciality) {
        this.hsaSosTitleCodeSpeciality = hsaSosTitleCodeSpeciality;
    }

    public List<String> getHsaSystemRole() {
        return hsaSystemRole;
    }

    public void setHsaSystemRole(List<String> hsaSystemRole) {
        this.hsaSystemRole = hsaSystemRole;
    }

    public List<String> getHsaSyncId() {
        return hsaSyncId;
    }

    public void setHsaSyncId(List<String> hsaSyncId) {
        this.hsaSyncId = hsaSyncId;
    }

    public String getHsaSwitchboardNumber() {
        return hsaSwitchboardNumber;
    }

    public void setHsaSwitchboardNumber(String hsaSwitchboardNumber) {
        this.hsaSwitchboardNumber = hsaSwitchboardNumber;
    }

    public List<String> getHsaTextTelephoneNumber() {
        return hsaTextTelephoneNumber;
    }

    public void setHsaTextTelephoneNumber(List<String> hsaTextTelephoneNumber) {
        this.hsaTextTelephoneNumber = hsaTextTelephoneNumber;
    }

    public List<String> getHsaTelephoneNumber() {
        return hsaTelephoneNumber;
    }

    public void setHsaTelephoneNumber(List<String> hsaTelephoneNumber) {
        this.hsaTelephoneNumber = hsaTelephoneNumber;
    }

    public List<String> getHsaTitle() {
        return hsaTitle;
    }

    public void setHsaTitle(List<String> hsaTitle) {
        this.hsaTitle = hsaTitle;
    }

    public String getHsaVerifiedIdentityNumber() {
        return hsaVerifiedIdentityNumber;
    }

    public void setHsaVerifiedIdentityNumber(String hsaVerifiedIdentityNumber) {
        this.hsaVerifiedIdentityNumber = hsaVerifiedIdentityNumber;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getJpegPhoto() {
        return jpegPhoto;
    }

    public void setJpegPhoto(String jpegPhoto) {
        this.jpegPhoto = jpegPhoto;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public List<String> getMobile() {
        return mobile;
    }

    public void setMobile(List<String> mobile) {
        this.mobile = mobile;
    }

    public String getModifiersName() {
        return modifiersName;
    }

    public void setModifiersName(String modifiersName) {
        this.modifiersName = modifiersName;
    }

    public String getModifyTimeStamp() {
        return modifyTimeStamp;
    }

    public void setModifyTimeStamp(String modifyTimeStamp) {
        this.modifyTimeStamp = modifyTimeStamp;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public List<String> getOccupationalCode() {
        return occupationalCode;
    }

    public void setOccupationalCode(List<String> occupationalCode) {
        this.occupationalCode = occupationalCode;
    }

    public List<String> getPager() {
        return pager;
    }

    public void setPager(List<String> pager) {
        this.pager = pager;
    }

    public List<String> getPaTitleName() {
        return paTitleName;
    }

    public void setPaTitleName(List<String> paTitleName) {
        this.paTitleName = paTitleName;
    }

    public List<String> getPaTitleCode() {
        return paTitleCode;
    }

    public void setPaTitleCode(List<String> paTitleCode) {
        this.paTitleCode = paTitleCode;
    }

    public String getPersonalIdentityNumber() {
        return personalIdentityNumber;
    }

    public void setPersonalIdentityNumber(String personalIdentityNumber) {
        this.personalIdentityNumber = personalIdentityNumber;
    }

    public String getPersonalPrescriptionCode() {
        return personalPrescriptionCode;
    }

    public void setPersonalPrescriptionCode(String personalPrescriptionCode) {
        this.personalPrescriptionCode = personalPrescriptionCode;
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(PostalAddress postalAddress) {
        this.postalAddress = postalAddress;
    }

    public List<String> getSeeAlso() {
        return seeAlso;
    }

    public void setSeeAlso(List<String> seeAlso) {
        this.seeAlso = seeAlso;
    }

    public List<String> getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(List<String> serialNumber) {
        this.serialNumber = serialNumber;
    }

    public List<String> getSpecialityName() {
        return specialityName;
    }

    public void setSpecialityName(List<String> specialityName) {
        this.specialityName = specialityName;
    }

    public List<String> getSpecialityCode() {
        return specialityCode;
    }

    public void setSpecialityCode(List<String> specialityCode) {
        this.specialityCode = specialityCode;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public List<Hours> getTelephoneHours() {
        return telephoneHours;
    }

    public void setTelephoneHours(List<Hours> telephoneHours) {
        this.telephoneHours = telephoneHours;
    }

    public List<String> getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(List<String> telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getUserCertificate() {
        return userCertificate;
    }

    public void setUserCertificate(List<String> userCertificate) {
        this.userCertificate = userCertificate;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserPrincipalName() {
        return userPrincipalName;
    }

    public void setUserPrincipalName(String userPrincipalName) {
        this.userPrincipalName = userPrincipalName;
    }

    public List<String> getValidNotBefore() {
        return validNotBefore;
    }

    public void setValidNotBefore(List<String> validNotBefore) {
        this.validNotBefore = validNotBefore;
    }

    public List<String> getValidNotAfter() {
        return validNotAfter;
    }

    public void setValidNotAfter(List<String> validNotAfter) {
        this.validNotAfter = validNotAfter;
    }
}
