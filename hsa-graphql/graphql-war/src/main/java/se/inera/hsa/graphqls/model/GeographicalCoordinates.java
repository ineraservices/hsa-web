package se.inera.graphqls.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;

public class GeographicalCoordinates implements Serializable {
	@JsonDeserialize(as = Coordinate.class)
	public Coordinate coordinate;
}
