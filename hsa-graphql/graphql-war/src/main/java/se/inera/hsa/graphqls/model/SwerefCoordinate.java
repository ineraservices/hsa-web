package se.inera.graphqls.model;

import java.io.Serializable;

/**
 * @author kdaham
 */
public class SwerefCoordinate implements Serializable {
    public String type;
    //  used for SWEREF 99 TM
    public String N;
    public String E;
}
