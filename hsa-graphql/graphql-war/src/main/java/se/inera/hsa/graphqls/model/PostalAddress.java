package se.inera.graphqls.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author kdaham
 */
public class PostalAddress implements Serializable {
    public List<String> getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(List<String> addressLine) {
        this.addressLine = addressLine;
    }

    public List<String> addressLine;
}
