package se.inera.graphqls.model;

import io.leangen.graphql.annotations.GraphQLQuery;

public class Codesystem {

    @GraphQLQuery
    public String codeSystemId ;

    @GraphQLQuery
    public String name;

}
