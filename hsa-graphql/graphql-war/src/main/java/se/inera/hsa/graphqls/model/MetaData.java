package se.inera.graphqls.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.validation.constraints.NotBlank;
import java.time.ZonedDateTime;

public class MetaData {

    public MetaData(){

    }

    @NotBlank
    public String createTimestamp;

    @NotBlank
    public String creatorUUID;

    @NotBlank
    public String creatorPath;

    @NotBlank
    public int numSubordinates;

    @NotBlank
    public int numAllSubordinates;

    @NotBlank
    public String path;

    public String getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(String createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public String getCreatorUUID() {
        return creatorUUID;
    }

    public void setCreatorUUID(String creatorUUID) {
        this.creatorUUID = creatorUUID;
    }

    public String getCreatorPath() {
        return creatorPath;
    }

    public void setCreatorPath(String creatorPath) {
        this.creatorPath = creatorPath;
    }

    public int getNumSubordinates() {
        return numSubordinates;
    }

    public void setNumSubordinates(int numSubordinates) {
        this.numSubordinates = numSubordinates;
    }

    public int getNumAllSubordinates() {
        return numAllSubordinates;
    }

    public void setNumAllSubordinates(int numAllSubordinates) {
        this.numAllSubordinates = numAllSubordinates;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ZonedDateTime getModifyTimestamp() {
        return modifyTimestamp;
    }

    public void setModifyTimestamp(ZonedDateTime modifyTimestamp) {
        this.modifyTimestamp = modifyTimestamp;
    }

    public String getModifierPath() {
        return modifierPath;
    }

    public void setModifierPath(String modifierPath) {
        this.modifierPath = modifierPath;
    }

    public String getModifierUUID() {
        return modifierUUID;
    }

    public void setModifierUUID(String modifierUUID) {
        this.modifierUUID = modifierUUID;
    }

    @JsonDeserialize
    public ZonedDateTime modifyTimestamp;

    public String modifierPath;

    public String modifierUUID;

    public MetaData(String createTimestamp,
                    String creatorUUID,
                    String creatorPath,
                    int numSubordinates,
                    int numAllSubordinates,
                    String path) {
        this.createTimestamp = createTimestamp;
        this.creatorUUID = creatorUUID;
        this.creatorPath = creatorPath;
        this.numSubordinates = numSubordinates;
        this.numAllSubordinates = numAllSubordinates;
        this.path = path;
    }
}
