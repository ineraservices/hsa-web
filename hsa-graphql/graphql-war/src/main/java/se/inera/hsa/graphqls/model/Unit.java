package se.inera.graphqls.model;

import io.leangen.graphql.annotations.GraphQLQuery;

import java.util.List;

public class Unit extends ParentModel {

    @GraphQLQuery()
    public List<String> ouShort; // alternative name/s

    @GraphQLQuery()
    public List<String> unitPrescriptionCode;

    public Unit() {

    }
}
