package se.inera.graphqls.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.leangen.graphql.annotations.GraphQLQuery;

import java.util.List;

public class SearchResult {

    @GraphQLQuery
    @JsonDeserialize
    public  List<Country> countries;
    @GraphQLQuery
    @JsonDeserialize
    public  List<County> counties;
    @GraphQLQuery
    @JsonDeserialize
    public  List<Organization> organizations;
    @GraphQLQuery
    @JsonDeserialize
    public  List<Unit> units;
    @GraphQLQuery
    @JsonDeserialize
    public  List<Function> functions;

    @GraphQLQuery
    @JsonDeserialize
    public List<Employee> employees;

    @GraphQLQuery
    public Integer limit = 200;

    @GraphQLQuery
    public Integer totalHits = 200;

    public SearchResult(){
        this.limit = 200;
        this.totalHits = 200;
    }

}
