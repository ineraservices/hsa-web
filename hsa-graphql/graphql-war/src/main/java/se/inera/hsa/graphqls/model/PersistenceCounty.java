package se.inera.graphqls.model;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

public class PersistenceCounty {

    @NotBlank
    public String uuid;

    @NotBlank
    public String creatorUUID;

    @NotNull
    public ZonedDateTime createTimestamp;

    public String modifierUUID;
    
    public ZonedDateTime modifyTimestamp;

    @Valid
    public County county;

}
