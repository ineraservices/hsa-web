package se.inera.graphqls.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;

public class GeographicalCoordinatesSweref99TM implements Serializable {
    @JsonDeserialize(as = SwerefCoordinate.class)
    public SwerefCoordinate coordinate;
}
