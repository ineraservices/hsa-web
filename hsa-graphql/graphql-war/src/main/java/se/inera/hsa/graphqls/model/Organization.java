package se.inera.graphqls.model;

import io.leangen.graphql.annotations.GraphQLQuery;

import javax.json.bind.JsonbBuilder;
import java.io.Serializable;
import java.util.List;


/**
 * @author Tewelle W.
 */

public class Organization extends ParentModel implements Serializable {

    @GraphQLQuery()
    public String businessClassificationType;
    @GraphQLQuery()
    public String  cn;
    @GraphQLQuery(name = "hsaId", description = "Organization hsaId")
    public String hsaId;
    @GraphQLQuery()
    public String  hsaIdUrlEncoded;
    @GraphQLQuery()
    public String  dn;
    @GraphQLQuery()
    public String  parentDn;
    @GraphQLQuery()
    public String  parentDnUrlEncoded;
    @GraphQLQuery()
    public String  relativeDistinguishedName;
    @GraphQLQuery(name = "c", description = "countryName")
    public String  c;
    @GraphQLQuery()
    public String  l;
    @GraphQLQuery()
    public List<String> objectClass;

    public Organization() {
    }

    public String getBusinessClassificationType() {
        return businessClassificationType;
    }

    public void setBusinessClassificationType(String businessClassificationType) {
        this.businessClassificationType = businessClassificationType;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getHsaId() {
        return hsaId;
    }

    public void setHsaId(String hsaId) {
        this.hsaId = hsaId;
    }

    public String getHsaIdUrlEncoded() {
        return hsaIdUrlEncoded;
    }

    public void setHsaIdUrlEncoded(String hsaIdUrlEncoded) {
        this.hsaIdUrlEncoded = hsaIdUrlEncoded;
    }

    public String getDn() {
        return dn;
    }

    public void setDn(String dn) {
        this.dn = dn;
    }

    public String getParentDn() {
        return parentDn;
    }

    public void setParentDn(String parentDn) {
        this.parentDn = parentDn;
    }

    public String getParentDnUrlEncoded() {
        return parentDnUrlEncoded;
    }

    public void setParentDnUrlEncoded(String parentDnUrlEncoded) {
        this.parentDnUrlEncoded = parentDnUrlEncoded;
    }

    public String getRelativeDistinguishedName() {
        return relativeDistinguishedName;
    }

    public void setRelativeDistinguishedName(String relativeDistinguishedName) {
        this.relativeDistinguishedName = relativeDistinguishedName;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getL() {
        return l;
    }

    public void setL(String l) {
        this.l = l;
    }

    public List<String> getObjectClass() {
        return this.objectClass;
    }

    public void setObjectClass(List<String> objectClass) {
        this.objectClass = objectClass;
    }

    public MetaData getMetaData() {
        return this.metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }


    public String toJson() {
        return JsonbBuilder.create().toJson(this);
    }
}
