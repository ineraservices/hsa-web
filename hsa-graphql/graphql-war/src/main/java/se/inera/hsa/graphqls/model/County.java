package se.inera.graphqls.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.leangen.graphql.annotations.GraphQLQuery;

public class County {


    @GraphQLQuery
    public String uuid ;

    @GraphQLQuery
    public String name;

    @GraphQLQuery
    public String type;

    @GraphQLQuery
    @JsonDeserialize
    public MetaData metaData ;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }
}
