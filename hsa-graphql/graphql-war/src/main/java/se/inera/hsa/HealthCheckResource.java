package se.inera;

import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;

import javax.enterprise.context.ApplicationScoped;

/**
 * @author kdaham
 */
@Health
@ApplicationScoped
public class HealthCheckResource implements HealthCheck {

    // Check if we have a redis connection
    public HealthCheckResponse call() {
        return HealthCheckResponse.named("graphql").up().build();
    }
}
