package se.inera.graphqls.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import se.inera.graphqls.model.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Tewelle W.
 */

public class RestConsumer {

    public SearchResult consumeOrgApiSearchResult(String targetUrl) throws IOException {

        Client client = ClientBuilder.newClient();

        SearchResult searchResult = new SearchResult();
        Response response = client.target(targetUrl).request().get();

        String responseString = response.readEntity(String.class);
        System.out.println ("response string: " + responseString);

        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        // ignore unknown fields
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

        try {
            searchResult = mapper.reader()
                    .forType(SearchResult.class )
                    .readValue(responseString);
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.close();
        client.close();
        System.out.println("total emplopyee result size in consumer: " + searchResult.employees.size());
        return searchResult;

    }

    public List<Codesystem> consumeCodesystemApi(String targetUrl) throws IOException {

        Client client = ClientBuilder.newClient();

        List<Codesystem> results = new ArrayList<>();
        Response response = client.target(targetUrl).request().get();

        String responseString = response.readEntity(String.class);
        System.out.println("result:" + responseString);
        List<Codesystem> codesystems = new ArrayList<>();

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        // ignore unknown fields
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

        try {
            codesystems = mapper.reader()
                    .forType(Codesystem.class)
                    .readValue(responseString);
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.close();
        client.close();

        return codesystems;

    }

    public List<Employee> consumeEmployeeApiSearchResult(String targetUrl) throws IOException {

        Client client = ClientBuilder.newClient();

        List<Employee> searchResult = new ArrayList<>();
        Response response = client.target(targetUrl).request().get();

        String responseString = response.readEntity(String.class);

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        // ignore unknown fields
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

        try {
            searchResult = mapper.reader()
                    .forType(new TypeReference<List<Employee>>() {})
                    .readValue(responseString);
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.close();
        client.close();

        return searchResult;

    }
}
