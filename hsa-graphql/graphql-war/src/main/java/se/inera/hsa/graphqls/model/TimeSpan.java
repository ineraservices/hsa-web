package se.inera.graphqls.model;

import java.io.Serializable;

/**
 * @author kdaham
 */
public class TimeSpan implements Serializable {
   public String fromDay;
   public String toDay;
   public String fromTime2;
   public String toTime2;

   public String getFromDay() {
      return fromDay;
   }

   public void setFromDay(String fromDay) {
      this.fromDay = fromDay;
   }

   public String comment;
   public String fromDate;
   public String toDate;
}
