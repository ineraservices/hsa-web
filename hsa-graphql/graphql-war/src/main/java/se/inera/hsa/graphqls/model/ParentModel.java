package se.inera.graphqls.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.leangen.graphql.annotations.GraphQLQuery;
import jdk.nashorn.internal.ir.annotations.Ignore;

import javax.json.bind.JsonbBuilder;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.ws.rs.DefaultValue;
import java.util.List;
import java.util.UUID;


/**
 * @author Tewelle W.
 */

public class ParentModel {


    // common attributes
    @GraphQLQuery
    @JsonDeserialize()
    public MetaData metaData ;

    @GraphQLQuery(name = "id", description = "Id")
    public String id;
    @GraphQLQuery(name = "name", description = "name")
    @NotBlank
    public String name;

    @Pattern(regexp = "Unit")
    @GraphQLQuery
    public String type;

    @NotBlank
    @GraphQLQuery
    public String hsaIdentity;

    @GraphQLQuery()
    @JsonDeserialize()
    public List<Hours> telephoneHours;

    @GraphQLQuery()
    @JsonDeserialize()
    public List<Hours> dropInHours;

    @GraphQLQuery()
    public String hsaVpwWebPage;

    @GraphQLQuery()
    public String displayOption; // only one value is allowed
    @GraphQLQuery
    public String route; // only one value is allowed

    @GraphQLQuery()
    public String hsaAltText; // only one value is allowed

    @GraphQLQuery
    public String description;

    @GraphQLQuery()
    public String street;  // street(streetAddress);

    @GraphQLQuery()
    public String hsaVisitingRules;

    @GraphQLQuery()
    @JsonDeserialize()
    public List<Hours> visitingHours;

    @GraphQLQuery()
    public String jpegPhoto;

    @GraphQLQuery()
    public List<String> hsaVisitingRuleAge;

    @GraphQLQuery()
    public String hsaJpegLogotype;

    @GraphQLQuery()
    public String countryName;
    @GraphQLQuery
    public String countyName;

    @GraphQLQuery()
    public String countyCode;

    @GraphQLQuery()
    public String hsaVpwInformation1;

    @GraphQLQuery()
    public List<String> pager; //pager(pagerTelephoneNumber);

    @GraphQLQuery()
    public List<String> mobile; //mobile(mobileTelephoneNumber); om det kommer in en add på mobileTelephoneNumber gör om till mobile
    //om det kommer in en fråga på mobileTelephoneNumber gör om mobile till mobileTelephoneNumber

    @GraphQLQuery()
    public String hsaHealthCareArea;

    @GraphQLQuery()
    public String orgNo;

    @GraphQLQuery()
    public List<String> hsaBusinessType;

    @GraphQLQuery()
    public List<String> financingOrganization;

    @GraphQLQuery()
    public String hsaAuthenticationMethod;

    @GraphQLQuery()
    public List<String> hsaSimpleSearch;

    @GraphQLQuery()
    public List<String> hsaPhoneticSearch;

    @GraphQLQuery()
    public String hsaAdminComment;

    /**
     * DN till aktuellt objekt. ex. 'ou=Ortopeden Sabbatsberg 15,ou=Sabbatsbergs Sjukhus,o=Stockholms läns landsting,l=Stockholms län,c=SE'
     */
    @GraphQLQuery()
    public List<String> seeAlso;

    @GraphQLQuery()
    @JsonDeserialize()
    public PostalAddress postalAddress;

    @GraphQLQuery()
    public String postalCode;

    @GraphQLQuery()
    public String hsaVpwInformation3;

    @GraphQLQuery()
    public List<String> hsaVpwNeighbouringObject;

    @GraphQLQuery()
    public String hsaVisitingRuleReferral;

    @GraphQLQuery()
    public List<String> hsaTextTelephoneNumber;

    @GraphQLQuery()
    public String hsaVpwInformation2;

    @GraphQLQuery()
    public List<String> hsaTelephoneNumber;

    @GraphQLQuery()
    public List<String> facsimileTelephoneNumber; // fax

    @GraphQLQuery()
    public List<String> businessClassificationName;

    @GraphQLQuery()
    public List<String> businessClassificationCode;

    @GraphQLQuery()
    public String hsaHealthCareUnitManager;

    @GraphQLQuery()
    public String hsaDestinationIndicator;

    @GraphQLQuery()
    public List<String> careType; // this is multi-value

    @GraphQLQuery()
    public List<String> hsaHealthCareUnitMember;

    @GraphQLQuery()
    public String hsaResponsibleHealthCareProvider;

    @GraphQLQuery()
    public String hsaSwitchboardNumber;

    @GraphQLQuery()
    public String labeledURI;

    @GraphQLQuery()
    public List<String> management;

    @GraphQLQuery()
    @JsonDeserialize()
    public List<Hours> surgeryHours;

    @GraphQLQuery()
    public String hsaVpwInformation4;

    @GraphQLQuery()
    public String hsaDirectoryContact;

    @GraphQLQuery()
    public List<String> hsaSyncId;

    @GraphQLQuery()
    public String endDate;

    @GraphQLQuery()
    public String startDate;

    @GraphQLQuery()
    public String smsTelephoneNumber;

    @GraphQLQuery()
    public List<String> telephoneNumber;

    /**
     * Geografiska koordinater (enligt RT90) som anger enhetens fysiska placering.
     * Exempel på en koordinat i Stockholm (Djurgårdsbron) är: "X:6581164,Y:1630250"
     */
    //@JsonbTypeDeserializer(GeographicalCoordinatesDeserializer.class)
    //public List<GeographicalCoordinates> geographicalCoordinates;
    @GraphQLQuery()
    @JsonDeserialize(as = GeographicalCoordinates.class)
    public GeographicalCoordinates geographicalCoordinates;

    /**
     * Geografiska koordinater enligt SWEREF 99 TM som anger enhetens fysiska placering.
     * Exempel på en koordinat i Linköping (E4-bron över Stångån) är: N:6477155,E:536352
     */
    // @JsonbTypeDeserializer(GeographicalCoordinatesSweref99TMDeserializer.class)
    // public List<GeographicalCoordinatesSweref99TM> geographicalCoordinatesSweref99TM;
    @GraphQLQuery()
    @JsonDeserialize(as = GeographicalCoordinatesSweref99TM.class)
    public GeographicalCoordinatesSweref99TM geographicalCoordinatesSweref99TM;

    /**
     * Uppgift om e-postadress.  E-postadressen för personobjekt ska vara personlig och ska vara en adress som används i tjänsten.
     * E-postadressen för organisations-, enhets- eller funktionsobjekt ska utgöras av en funktionsbrevlåda.
     * E-postadress är synlig för alla som har tillgång till HSA-katalogen och kan användas vid t.ex. aviseringar via e-post
     * från applikationer men ingår inte i publik enhetsinformation.
     */
    @Email
    @GraphQLQuery()
    public String mail; //mail(rfc822Mailbox); // single value e-post


    /**
     * Kodverk (när det gäller objektklassen locality)
     * Namn på plats där enheten är fysiskt placerad. Geografisk plats kan vara detsamma som stad, stadsdel, kommun eller kommundel.
     * Det kan vara annan geografisk plats än det som anges i adressuppgifter.
     */
    @GraphQLQuery()
    public String localityName;

    @GraphQLQuery()
    public String municipalityCode;

    @GraphQLQuery()
    public String municipalityName;

    @GraphQLQuery()
    public String certificateRevocationList;

    @Email
    @GraphQLQuery()
    public String contentResponsibleEmail; // not available in hsa-schema xls file, came from 1177

    @GraphQLQuery()
    public String alternativeName;

    @DefaultValue("false")
    @GraphQLQuery()
    @Ignore
    public boolean hsaHealthCareProvider;

    @DefaultValue("false")
    @GraphQLQuery()
    public boolean hsaHealthCareUnit;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHsaIdentity() {
        return hsaIdentity;
    }

    public void setHsaIdentity(String hsaIdentity) {
        this.hsaIdentity = hsaIdentity;
    }

    public List<Hours> getTelephoneHours() {
        return telephoneHours;
    }


    public List<Hours> getDropInHours() {
        return dropInHours;
    }

    public void setDropInHours(List<Hours> dropInHours) {
        this.dropInHours = dropInHours;
    }

    public String getHsaVpwWebPage() {
        return hsaVpwWebPage;
    }

    public void setHsaVpwWebPage(String hsaVpwWebPage) {
        this.hsaVpwWebPage = hsaVpwWebPage;
    }

    public String getDisplayOption() {
        return displayOption;
    }

    public void setDisplayOption(String displayOption) {
        this.displayOption = displayOption;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getHsaAltText() {
        return hsaAltText;
    }

    public void setHsaAltText(String hsaAltText) {
        this.hsaAltText = hsaAltText;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHsaVisitingRules() {
        return hsaVisitingRules;
    }

    public void setHsaVisitingRules(String hsaVisitingRules) {
        this.hsaVisitingRules = hsaVisitingRules;
    }

    public List<Hours> getVisitingHours() {
        return visitingHours;
    }

    public void setVisitingHours(List<Hours> visitingHours) {
        this.visitingHours = visitingHours;
    }

    public String getJpegPhoto() {
        return jpegPhoto;
    }

    public void setJpegPhoto(String jpegPhoto) {
        this.jpegPhoto = jpegPhoto;
    }

    public List<String> getHsaVisitingRuleAge() {
        return hsaVisitingRuleAge;
    }

    public void setHsaVisitingRuleAge(List<String> hsaVisitingRuleAge) {
        this.hsaVisitingRuleAge = hsaVisitingRuleAge;
    }

    public String getHsaJpegLogotype() {
        return hsaJpegLogotype;
    }

    public void setHsaJpegLogotype(String hsaJpegLogotype) {
        this.hsaJpegLogotype = hsaJpegLogotype;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(String countyCode) {
        this.countyCode = countyCode;
    }

    public String getHsaVpwInformation1() {
        return hsaVpwInformation1;
    }

    public void setHsaVpwInformation1(String hsaVpwInformation1) {
        this.hsaVpwInformation1 = hsaVpwInformation1;
    }

    public List<String> getPager() {
        return pager;
    }

    public void setPager(List<String> pager) {
        this.pager = pager;
    }

    public List<String> getMobile() {
        return mobile;
    }

    public void setMobile(List<String> mobile) {
        this.mobile = mobile;
    }

    public String getHsaHealthCareArea() {
        return hsaHealthCareArea;
    }

    public void setHsaHealthCareArea(String hsaHealthCareArea) {
        this.hsaHealthCareArea = hsaHealthCareArea;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public List<String> getHsaBusinessType() {
        return hsaBusinessType;
    }

    public void setHsaBusinessType(List<String> hsaBusinessType) {
        this.hsaBusinessType = hsaBusinessType;
    }

    public List<String> getFinancingOrganization() {
        return financingOrganization;
    }

    public void setFinancingOrganization(List<String> financingOrganization) {
        this.financingOrganization = financingOrganization;
    }

    public String getHsaAuthenticationMethod() {
        return hsaAuthenticationMethod;
    }

    public void setHsaAuthenticationMethod(String hsaAuthenticationMethod) {
        this.hsaAuthenticationMethod = hsaAuthenticationMethod;
    }

    public List<String> getHsaSimpleSearch() {
        return hsaSimpleSearch;
    }

    public void setHsaSimpleSearch(List<String> hsaSimpleSearch) {
        this.hsaSimpleSearch = hsaSimpleSearch;
    }

    public List<String> getHsaPhoneticSearch() {
        return hsaPhoneticSearch;
    }

    public void setHsaPhoneticSearch(List<String> hsaPhoneticSearch) {
        this.hsaPhoneticSearch = hsaPhoneticSearch;
    }

    public String getHsaAdminComment() {
        return hsaAdminComment;
    }

    public void setHsaAdminComment(String hsaAdminComment) {
        this.hsaAdminComment = hsaAdminComment;
    }

    public List<String> getSeeAlso() {
        return seeAlso;
    }

    public void setSeeAlso(List<String> seeAlso) {
        this.seeAlso = seeAlso;
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(PostalAddress postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getHsaVpwInformation3() {
        return hsaVpwInformation3;
    }

    public void setHsaVpwInformation3(String hsaVpwInformation3) {
        this.hsaVpwInformation3 = hsaVpwInformation3;
    }

    public List<String> getHsaVpwNeighbouringObject() {
        return hsaVpwNeighbouringObject;
    }

    public void setHsaVpwNeighbouringObject(List<String> hsaVpwNeighbouringObject) {
        this.hsaVpwNeighbouringObject = hsaVpwNeighbouringObject;
    }

    public String getHsaVisitingRuleReferral() {
        return hsaVisitingRuleReferral;
    }

    public void setHsaVisitingRuleReferral(String hsaVisitingRuleReferral) {
        this.hsaVisitingRuleReferral = hsaVisitingRuleReferral;
    }

    public List<String> getHsaTextTelephoneNumber() {
        return hsaTextTelephoneNumber;
    }

    public void setHsaTextTelephoneNumber(List<String> hsaTextTelephoneNumber) {
        this.hsaTextTelephoneNumber = hsaTextTelephoneNumber;
    }

    public String getHsaVpwInformation2() {
        return hsaVpwInformation2;
    }

    public void setHsaVpwInformation2(String hsaVpwInformation2) {
        this.hsaVpwInformation2 = hsaVpwInformation2;
    }

    public List<String> getHsaTelephoneNumber() {
        return hsaTelephoneNumber;
    }

    public void setHsaTelephoneNumber(List<String> hsaTelephoneNumber) {
        this.hsaTelephoneNumber = hsaTelephoneNumber;
    }

    public List<String> getFacsimileTelephoneNumber() {
        return facsimileTelephoneNumber;
    }

    public void setFacsimileTelephoneNumber(List<String> facsimileTelephoneNumber) {
        this.facsimileTelephoneNumber = facsimileTelephoneNumber;
    }

    public List<String> getBusinessClassificationName() {
        return businessClassificationName;
    }

    public void setBusinessClassificationName(List<String> businessClassificationName) {
        this.businessClassificationName = businessClassificationName;
    }

    public List<String> getBusinessClassificationCode() {
        return businessClassificationCode;
    }

    public void setBusinessClassificationCode(List<String> businessClassificationCode) {
        this.businessClassificationCode = businessClassificationCode;
    }

    public String getHsaHealthCareUnitManager() {
        return hsaHealthCareUnitManager;
    }

    public void setHsaHealthCareUnitManager(String hsaHealthCareUnitManager) {
        this.hsaHealthCareUnitManager = hsaHealthCareUnitManager;
    }

    public String getHsaDestinationIndicator() {
        return hsaDestinationIndicator;
    }

    public void setHsaDestinationIndicator(String hsaDestinationIndicator) {
        this.hsaDestinationIndicator = hsaDestinationIndicator;
    }

    public List<String> getCareType() {
        return careType;
    }

    public void setCareType(List<String> careType) {
        this.careType = careType;
    }

    public List<String> getHsaHealthCareUnitMember() {
        return hsaHealthCareUnitMember;
    }

    public void setHsaHealthCareUnitMember(List<String> hsaHealthCareUnitMember) {
        this.hsaHealthCareUnitMember = hsaHealthCareUnitMember;
    }

    public String getHsaResponsibleHealthCareProvider() {
        return hsaResponsibleHealthCareProvider;
    }

    public void setHsaResponsibleHealthCareProvider(String hsaResponsibleHealthCareProvider) {
        this.hsaResponsibleHealthCareProvider = hsaResponsibleHealthCareProvider;
    }

    public String getHsaSwitchboardNumber() {
        return hsaSwitchboardNumber;
    }

    public void setHsaSwitchboardNumber(String hsaSwitchboardNumber) {
        this.hsaSwitchboardNumber = hsaSwitchboardNumber;
    }

    public String getLabeledURI() {
        return labeledURI;
    }

    public void setLabeledURI(String labeledURI) {
        this.labeledURI = labeledURI;
    }

    public List<String> getManagement() {
        return management;
    }

    public void setManagement(List<String> management) {
        this.management = management;
    }

    public List<Hours> getSurgeryHours() {
        return surgeryHours;
    }

    public void setSurgeryHours(List<Hours> surgeryHours) {
        this.surgeryHours = surgeryHours;
    }

    public String getHsaVpwInformation4() {
        return hsaVpwInformation4;
    }

    public void setHsaVpwInformation4(String hsaVpwInformation4) {
        this.hsaVpwInformation4 = hsaVpwInformation4;
    }

    public String getHsaDirectoryContact() {
        return hsaDirectoryContact;
    }

    public void setHsaDirectoryContact(String hsaDirectoryContact) {
        this.hsaDirectoryContact = hsaDirectoryContact;
    }

    public List<String> getHsaSyncId() {
        return hsaSyncId;
    }

    public void setHsaSyncId(List<String> hsaSyncId) {
        this.hsaSyncId = hsaSyncId;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getSmsTelephoneNumber() {
        return smsTelephoneNumber;
    }

    public void setSmsTelephoneNumber(String smsTelephoneNumber) {
        this.smsTelephoneNumber = smsTelephoneNumber;
    }

    public List<String> getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(List<String> telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public GeographicalCoordinates getGeographicalCoordinates() {
        return geographicalCoordinates;
    }

    public void setGeographicalCoordinates(GeographicalCoordinates geographicalCoordinates) {
        this.geographicalCoordinates = geographicalCoordinates;
    }

    public GeographicalCoordinatesSweref99TM getGeographicalCoordinatesSweref99TM() {
        return geographicalCoordinatesSweref99TM;
    }

    public void setGeographicalCoordinatesSweref99TM(GeographicalCoordinatesSweref99TM geographicalCoordinatesSweref99TM) {
        this.geographicalCoordinatesSweref99TM = geographicalCoordinatesSweref99TM;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getLocalityName() {
        return localityName;
    }

    public void setLocalityName(String localityName) {
        this.localityName = localityName;
    }

    public String getMunicipalityCode() {
        return municipalityCode;
    }

    public void setMunicipalityCode(String municipalityCode) {
        this.municipalityCode = municipalityCode;
    }

    public String getMunicipalityName() {
        return municipalityName;
    }

    public void setMunicipalityName(String municipalityName) {
        this.municipalityName = municipalityName;
    }

    public String getCertificateRevocationList() {
        return certificateRevocationList;
    }

    public void setCertificateRevocationList(String certificateRevocationList) {
        this.certificateRevocationList = certificateRevocationList;
    }

    public String getContentResponsibleEmail() {
        return contentResponsibleEmail;
    }

    public void setContentResponsibleEmail(String contentResponsibleEmail) {
        this.contentResponsibleEmail = contentResponsibleEmail;
    }

    public String getAlternativeName() {
        return alternativeName;
    }

    public void setAlternativeName(String alternativeName) {
        this.alternativeName = alternativeName;
    }

    public boolean isHsaHealthCareProvider() {
        return hsaHealthCareProvider;
    }

    public void setHsaHealthCareProvider(boolean hsaHealthCareProvider) {
        this.hsaHealthCareProvider = hsaHealthCareProvider;
    }

    public boolean isHsaHealthCareUnit() {
        return hsaHealthCareUnit;
    }

    public void setHsaHealthCareUnit(boolean hsaHealthCareUnit) {
        this.hsaHealthCareUnit = hsaHealthCareUnit;
    }

    public String toJson() {
        return JsonbBuilder.create().toJson(this);
    }
}
