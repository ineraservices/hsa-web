package se.inera.graphqls.model;

import java.io.Serializable;

/**
 * @author kdaham
 * used for RT90
 */
public class Coordinate implements Serializable {
    public String type;
    public String x;
    public String y;
}
