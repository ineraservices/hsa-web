package se.inera.graphqls.service;


import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import se.inera.graphqls.model.*;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import se.inera.graphqls.model.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

/**
 * @author Tewelle W.
 */
@Singleton
public class GraphqlService {

    final private String DEFAULT_OFFSET = "0";
    final private String DEFAULT_SIZE = "200";

    @Inject
    @ConfigProperty(name = "organization.uri")
    String organizationApiURI;

    @Inject
    @ConfigProperty(name = "codesystem.uri")
    String codesystemsApiURI;

    @Inject
    RestConsumer restConsumer;

    @GraphQLQuery(name = "units")
    public List<Unit> getUnits(@GraphQLArgument(name = "offset", defaultValue = DEFAULT_OFFSET) int offset, @GraphQLArgument(name = "size", defaultValue = DEFAULT_SIZE) int size) {
        List<Unit> units = getSearchResult(organizationApiURI + "/search/free-text/@type: unit").units;
        // Validations so that offset & length don't go out of bounds
        return getPaginatedList(units, offset, size);
    }
    @GraphQLQuery(name = "unit")
    public Unit getUnitDetails(@GraphQLArgument(name = "path") String path) {
     Client client = ClientBuilder.newClient();

     Unit unit = new Unit();
     Response response = client.target(organizationApiURI + "/" + path).request().get();

     String responseString = response.readEntity(String.class);

     ObjectMapper mapper = new ObjectMapper();
     mapper.findAndRegisterModules();
     mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
     // ignore unknown fields
     mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
     mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

     try {
         unit = mapper.reader()
                 .forType(Unit.class )
                 .readValue(responseString);
     } catch (IOException e) {
         e.printStackTrace();
     }

     response.close();
     client.close();
     return unit;
    }
    @GraphQLQuery(name = "organization")
    public Organization getOrganizationDetails(@GraphQLArgument(name = "path") String path) {
     Client client = ClientBuilder.newClient();

     Organization org = new Organization();
     Response response = client.target(organizationApiURI + "/" + path).request().get();

     String responseString = response.readEntity(String.class);

     ObjectMapper mapper = new ObjectMapper();
     mapper.findAndRegisterModules();
     mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
     // ignore unknown fields
     mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
     mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

     try {
         org = mapper.reader()
                 .forType(Organization.class )
                 .readValue(responseString);
     } catch (IOException e) {
         e.printStackTrace();
     }

     response.close(
     );
     client.close();
     return org;
    }

    @GraphQLQuery(name = "function")
    public Function getFunctionDetails(@GraphQLArgument(name = "path") String path) {
     Client client = ClientBuilder.newClient();

     Function function = new Function();
     Response response = client.target(organizationApiURI + "/" + path).request().get();

     String responseString = response.readEntity(String.class);

     ObjectMapper mapper = new ObjectMapper();
     mapper.findAndRegisterModules();
     mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
     // ignore unknown fields
     mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
     mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

     try {
         function = mapper.reader()
                 .forType(Function.class )
                 .readValue(responseString);
     } catch (IOException e) {
         e.printStackTrace();
     }

     response.close(
     );
     client.close();
     return function;
    }
    @GraphQLQuery(name = "employee")
    public Employee getEmployeeDetails(@GraphQLArgument(name = "path") String path) {
     Client client = ClientBuilder.newClient();

     Employee employee = new Employee();
     Response response = client.target(organizationApiURI + "/" + path).request().get();

     String responseString = response.readEntity(String.class);

     ObjectMapper mapper = new ObjectMapper();
     mapper.findAndRegisterModules();
     mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
     // ignore unknown fields
     mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
     mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

     try {
         employee = mapper.reader()
                 .forType(Employee.class )
                 .readValue(responseString);
     } catch (IOException e) {
         e.printStackTrace();
     }

     response.close(
     );
     client.close();
     return employee;
    }
    @GraphQLQuery(name = "functions")
    public List<Function> getFunctions(@GraphQLArgument(name = "offset", defaultValue = DEFAULT_OFFSET) int offset, @GraphQLArgument(name = "size", defaultValue = DEFAULT_SIZE) int size) {
        List<Function> functions = getSearchResult(organizationApiURI + "/search/free-text/@type: functions").functions;
        // Validations so that offset & length don't go out of bounds
        return getPaginatedList(functions, offset, size);
    }

    @GraphQLQuery(name = "counties")
    public List<County> getCounties(@GraphQLArgument(name = "offset", defaultValue = DEFAULT_OFFSET) int offset, @GraphQLArgument(name = "size", defaultValue = DEFAULT_SIZE) int size) {

        List<County> counties = getSearchResult(organizationApiURI + "/search/free-text/@type: county").counties;
        // Validations so that offset & length don't go out of bounds
        return getPaginatedList(counties, offset, size);
    }

    @GraphQLQuery(name = "organizations")
    public List<Organization> getOrganizations(@GraphQLArgument(name = "offset", defaultValue = DEFAULT_OFFSET) int offset, @GraphQLArgument(name = "size", defaultValue = DEFAULT_SIZE) int size) {

        List<Organization> organizations = getSearchResult(organizationApiURI + "/search/free-text/@type: organization").organizations;
        // Validations so that offset & length don't go out of bounds
        return getPaginatedList(organizations, offset, size);
    }

    @GraphQLQuery(name = "employees")
    public List<Employee> getEmployees(@GraphQLArgument(name = "offset", defaultValue = DEFAULT_OFFSET) int offset, @GraphQLArgument(name = "size", defaultValue = DEFAULT_SIZE) int size) {

        // Mock a list of employees since Employee Api does not exist yet
         System.out.println("employess: "  + getSearchResult(organizationApiURI + "/search/free-text/@type: employee"));
        List<Employee> employees = getSearchResult(organizationApiURI + "/search/free-text/@type: employee").employees;
        System.out.println("employess: " + employees.size());
        // Validations so that offset & length don't go out of bounds
        return getPaginatedList(employees, offset, size);
    }


    @GraphQLQuery(name = "codesystems")
    public List<Codesystem> getCodeSystems(@GraphQLArgument(name = "offset", defaultValue = DEFAULT_OFFSET) int offset, @GraphQLArgument(name = "size", defaultValue = DEFAULT_SIZE) int size) {

        List<Codesystem> codesystems = null;

        try {
            codesystems = this.restConsumer.consumeCodesystemApi(codesystemsApiURI);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Validations so that offset & length don't go out of bounds
        return getPaginatedList(codesystems, offset, size);
    }


    @GraphQLQuery(name = "search")
    public SearchResult freeTextSearch(@GraphQLArgument(name = "freetext") String freetext, @GraphQLArgument(name = "searchNode", defaultValue = "countries/Sverige") String searchNode, @GraphQLArgument(name = "offset", defaultValue = DEFAULT_OFFSET) int offset, @GraphQLArgument(name = "size", defaultValue = DEFAULT_SIZE) int size) {
        System.out.println("Freetext: " + freetext);
        System.out.println("offset: " + offset + " size: " + size);
        SearchResult searchResult = null;
        String url = organizationApiURI + "/search/free-text/" + freetext.toString().trim().replace(" ", "%20") + "?node=" + searchNode.trim().replace("/", "%2F");

        System.out.println("URL: " + url);
        try {
            searchResult = this.restConsumer.consumeOrgApiSearchResult(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //TODO sort by name before return
        return getPaginatedSearchResult(searchResult != null ? searchResult : new SearchResult(), offset, size);
    }

    @GraphQLQuery(name = "simpleSearch")
    public SearchResult simpleSearch(@GraphQLArgument(name = "freetext") String freetext, @GraphQLArgument(name = "searchNode", defaultValue = "countries/Sverige") String searchNode, @GraphQLArgument(name = "offset", defaultValue = DEFAULT_OFFSET) int offset, @GraphQLArgument(name = "size", defaultValue = DEFAULT_SIZE) int size) {
        // System.out.println("Freetext: " + freetext);
        // System.out.println("offset: " + offset + " size: " + size);
        SearchResult searchResult = null;
        String url = organizationApiURI + "/search/simple/" + freetext.toString().trim().replace(" ", "%20") + "?node=" + searchNode.trim().replace("/", "%2F");

         System.out.println("URL: " + url);
        try {
            searchResult = this.restConsumer.consumeOrgApiSearchResult(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("total emplopyee result size: " + searchResult.employees.size());
        //TODO sort by name before return
        return getPaginatedSearchResult(searchResult != null ? searchResult : new SearchResult(), offset, size);
    }

    private SearchResult getSearchResult(String url) {
        SearchResult result = null;
        try {
            result = this.restConsumer.consumeOrgApiSearchResult(url.replace(" ", "%20"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }


    // Mocking search results
    private SearchResult createSearchResults() {

        SearchResult sr = new SearchResult();

        List<Employee> employees = new ArrayList<Employee>();
        List<Country> countries = new ArrayList<Country>();
        List<County> counties = new ArrayList<County>();


        int NUMBER_OF_SEARCHRESULTS = 50;
        IntStream.range(0, NUMBER_OF_SEARCHRESULTS).forEach((i) -> {
            Employee emp = new Employee();
            emp.setHsaIdentity("1000" + i);
            emp.setName("Employee" + i);
            emp.setSalary(i * 500);
            emp.setOrganizationId(1L);
            emp.setAge(22 + i);
            emp.setDepartmentId(1L);
            emp.setPosition("Position " + i);
            employees.add(emp);
        });


        IntStream.range(0, NUMBER_OF_SEARCHRESULTS).forEach((i) -> {
            Country country = new Country();
            country.name = "Country" + i;
            country.isoCode = "C" + i;
            country.type = "Country";
            countries.add(country);
        });

        IntStream.range(0, NUMBER_OF_SEARCHRESULTS).forEach((i) -> {
            County county = new County();
            county.name = "Län " + i;
            county.type = "County";
            counties.add(county);
        });


        sr.employees = employees;
        sr.countries = countries;
        sr.counties = counties;

        return sr;
    }

    private SearchResult getPaginatedSearchResult(SearchResult searchResult, int offset, int size) {
        searchResult.counties = getPaginatedList(searchResult.counties, offset, size);
        searchResult.counties.sort((o1, o2) -> o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase()));

        searchResult.countries = getPaginatedList(searchResult.countries, offset, size);
        searchResult.countries.sort((o1, o2) -> o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase()));

        searchResult.organizations = getPaginatedList(searchResult.organizations, offset, size);
        searchResult.organizations.sort((o1, o2) -> o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase()));

        searchResult.functions = getPaginatedList(searchResult.functions, offset, size);
        searchResult.functions.sort((o1, o2) -> o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase()));

        searchResult.units = getPaginatedList(searchResult.units, offset, size);
        searchResult.units.sort((o1, o2) -> o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase()));

        searchResult.employees = getPaginatedList(searchResult.employees, offset, size);
        searchResult.employees.sort((o1, o2) -> o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase()));

        searchResult.totalHits = searchResult.totalHits;
        searchResult.limit = searchResult.limit;

        return searchResult;
    }

    private <T> List<T> getPaginatedList(List<T> list, int offset, int size) {
        List<T> retval;


        // if negative offset, set it to zero.
        if (offset < 0) {
            offset = 0;
        }

        // if negative size, turn it positive.
        if (size < 0) {
            size = Math.abs(size);
        }

        if (offset > list.size()) {
            offset = list.size();
        }

        if (offset + size > list.size())
            retval = list.subList(offset, list.size());
        else
            retval = list.subList(offset, offset + size);

        // Collections.sort(retval, compareByName);
        // Collections.sort(employees, compareById.reversed());

        return retval;
    }
    /*Comparator<T> compareByName = new Comparator<T>() {
        @Override
        Comparator<T> compareByName = (T o1, T o2) ->
                o1.getName().compareTo( o2.getName() );
    };
*/
}
