package se.inera.graphqls.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class Hours {
    @JsonDeserialize(as = TimeSpan.class)
    public TimeSpan timeSpan;

    public TimeSpan getTimeSpan() {
        return timeSpan;
    }

    public void setTimeSpan(TimeSpan timeSpan) {
        this.timeSpan = timeSpan;
    }
}
