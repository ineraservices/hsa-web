package se.inera.graphqls.model;

import io.leangen.graphql.annotations.GraphQLQuery;

public class CodesystemCounty {


    @GraphQLQuery
    public String code ;

    @GraphQLQuery
    public String codeDescription;

    @GraphQLQuery
    public String lastModifiedDate;

    @GraphQLQuery
    public String name;

    @GraphQLQuery
    public String parentOid;


}
