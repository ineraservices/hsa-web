package se.inera.graphqls.model;

public class HealthStatus {

    private static final String VALUE_UP = "UP";

    public String status;

    public HealthStatus() {
        status = VALUE_UP;
    }
}
