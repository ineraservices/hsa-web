# Run gradle build
$ gradle build

# Run clean deploy 
$ gradle clean deploy

# Run system/integrationtests against running containers

$ gradle st

# Run docker-compose up and system/integrationtests

$ gradle st -PdockerCompose

# To run system/integrationtests from IDE have running containers up

$ docker-compose up -d
