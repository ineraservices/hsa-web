package se.inera.graphql.api;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.junit5.extension.RestAssuredExtension;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith({RestAssuredExtension.class})
public class graphqlResourceIT {

   // Given text query =
    String query = "{\"query\": \"{ counties (offset: 0, size: 50){name}}\"}";

    @Test
    @DisplayName("Verify that a nonexistent graphql call returns 404")
    public void getNonExistent() {
        given().when()
               .get("nonexistent")
               .then()
               .statusCode(404);
    }

    @Test
    @DisplayName("Verify that a POST call gets successfully executed")
    public void testPostCall() {

        Response response = given().log().all()
                .body(query)
                .when()
                .post(RestAssured.rootPath).thenReturn();

        assertEquals(200, response.getStatusCode());

        String resultString = response.thenReturn().getBody().asString();
        assertTrue(resultString.contains("counties"));
    }
}
