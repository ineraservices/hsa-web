package se.inera.graphql.service;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import se.inera.junit5.extension.RestAssuredExtension;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith({RestAssuredExtension.class})
class graphqlServiceIT {

    private String postUrl = "/graphql/resources/search/";

    @Test
    @DisplayName("Verify that a nonexistent graphql returns 404")
    void getNonExistent() {
        given().when()
                .get("nonexistent")
                .then()
                .statusCode(404);
    }

    @Test
    @DisplayName("Do pagination tests.")
    void doPaginationTest() {
        int offset = 2;
        int size = 7;

        String query = "{\"query\": \"{ employees (offset: " + offset +  ", size: " + size + "){ name}}\"}";
        final String EMPLOYEE_PREFIX = "Employee";

        // Api should be reachable on GET
        given()
                .when()
                .get()
                .then()
                .statusCode(200);

        // check if query includes expected object information
        Response result =  given().body(query)
                .when()
                .post(postUrl);
        //.statusCode(200);

        // assert that expected objects are included in result.
        for (int  i = offset; i < offset + size; i++){
            assertTrue(result.thenReturn().getBody().asString().contains(EMPLOYEE_PREFIX + i));

        }

        // Make sure that searchResults are not out of bounds from pagination.
        assertFalse(result.thenReturn().getBody().asString().contains(EMPLOYEE_PREFIX + (offset-1)));

        assertFalse(result.thenReturn().getBody().asString().contains(EMPLOYEE_PREFIX + (offset+size)));

        System.out.println("finished testing pagination!");
    }


    @Test
    @DisplayName("Check that pagination doesnt crash when using negative values for offset and size.")
    void doNegativeValuesPaginationTest() {
        int offset = -100;
        int size = -10;

        String query = "{\"query\": \"{ employees (offset: " + offset +  ", size: " + size + "){ name}}\"}";

        final String EMPLOYEE_PREFIX = "Employee";

        // Api should be reachable on GET
        given()
                .when()
                .get()
                .then()
                .statusCode(200);

        // check if query includes expected object information
        Response result =  given().body(query)
                .when()
                .post(postUrl);
        //.statusCode(200);

        // Assume we're getting the absolute value of size as size.
        String[] resultSplit = result.thenReturn().getBody().asString().split(EMPLOYEE_PREFIX);
        assertEquals(Math.abs(size), resultSplit.length-1);
        // Assume offset is set to 0 when using negative offset size.
        assertTrue(result.thenReturn().getBody().asString().contains(EMPLOYEE_PREFIX + 0));


        System.out.println("finished testing paginaton and negative values!");
    }


    @Test
    @DisplayName("Check that pagination doesnt crash when trying to access searchResults beyond search result size.")
    void doOutOfBoundsPaginationTest() {
        int numberOfSearchResults = 50;
        int offset = numberOfSearchResults-1;
        int size = 10;

        String query = "{\"query\": \"{ employees (offset: " + offset +  ", size: " + size + "){ name}}\"}";

        final String EMPLOYEE_PREFIX = "Employee";

        // Api should be reachable on GET
        given()
                .when()
                .get()
                .then()
                .statusCode(200);

        Response result =  given().body(query)
                .when()
                .post(postUrl);

        // expect to get 1 result
        String[] resultSplit = result.thenReturn().getBody().asString().split(EMPLOYEE_PREFIX);
        assertEquals(1, resultSplit.length-1);

        System.out.println("finished testing pagination and size reaching beyond search result size!");
    }


    @Test
    @DisplayName("Check that pagination doesnt crash when trying to access searchResults with an offset set to a value beyond the size of the searchResult.")
    void doOutOfboundsOffsetTest() {
        int offset = 1000;
        int size = 10;

        String query = "{\"query\": \"{ employees (offset: " + offset +  ", size: " + size + "){name}}\"}";

        final String EMPLOYEE_PREFIX = "Employee";

        // Api should be reachable on GET
        given()
                .when()
                .get()
                .then()
                .statusCode(200);

        Response result =  given().body(query)
                .when()
                .post(postUrl);

        // Expect to get an empty list of employees.
        assertTrue(result.thenReturn().getBody().asString().contains("[]"));

        System.out.println("finished testing pagination with offset beyond search result size!");
    }

    @Test
    @DisplayName("Units")
    void testUnits() {
        String query = "{\"query\": \"{ units (offset: 0, size: 20){name}}\"}";
        Response response = given().log().all()
                .body(query)
                .when()
                .post(RestAssured.rootPath).thenReturn();

        assertEquals(200, response.getStatusCode());

        String resultString = response.thenReturn().getBody().asString();
        // Make sure counties are returned
        assertTrue(resultString.contains("units"));
        // Make sure that result is not empty
        assertFalse(resultString.contains("[]"));

    }

    @Test
    @DisplayName("Functions")
    void testFunctions() {
        String query = "{\"query\": \"{ functions (offset: 0, size: 20){name}}\"}";
        Response response = given().log().all()
                .body(query)
                .when()
                .post(RestAssured.rootPath).thenReturn();

        assertEquals(200, response.getStatusCode());

        String resultString = response.thenReturn().getBody().asString();
        // Make sure counties are returned
        assertTrue(resultString.contains("functions"));
        // Make sure that result is not empty
        assertFalse(resultString.contains("[]"));

    }

    @Test
    @DisplayName("Counties")
    void testCounties() {
        String query = "{\"query\": \"{ counties (offset: 0, size: 20){name}}\"}";
        Response response = given().log().all()
                .body(query)
                .when()
                .post(RestAssured.rootPath).thenReturn();

        assertEquals(200, response.getStatusCode());

        String resultString = response.thenReturn().getBody().asString();
        // Make sure counties are returned
        assertTrue(resultString.contains("counties"));
        // Make sure that result is not empty
        assertFalse(resultString.contains("[]"));

    }


    @Test
    @DisplayName("Organizations")
    void testOrganizations() {
        String query = "{\"query\": \"{ organizations (offset: 0, size: 20){name}}\"}";
        Response response = given().log().all()
                .body(query)
                .when()
                .post(RestAssured.rootPath).thenReturn();

        assertEquals(200, response.getStatusCode());

        String resultString = response.thenReturn().getBody().asString();
        // Make sure counties are returned
        assertTrue(resultString.contains("organizations"));
        // Make sure that result is not empty
        assertFalse(resultString.contains("[]"));

    }


    @Test
    @DisplayName("Employees")
    void testEmployees() {
        String query = "{\"query\": \"{ employees (offset: 0, size: 20){name}}\"}";
        Response response = given().log().all()
                .body(query)
                .when()
                .post(RestAssured.rootPath).thenReturn();

        assertEquals(200, response.getStatusCode());

        String resultString = response.thenReturn().getBody().asString();
        // Make sure counties are returned
        assertTrue(resultString.contains("employees"));
        // Make sure that result is not empty
        assertFalse(resultString.contains("[]"));

    }


    @Test
    @DisplayName("Free text Search")
    void testSearch() {
        String query = "{\"query\": \"{ search (freetext: \\\"svensk\\\" offset: 0, size: 51){organizations {name}, functions{name}, units{name}, employees{name}, counties{name}}}\"}";
        Response response = given().log().all()
                .body(query)
                .when()
                .post(RestAssured.rootPath).thenReturn();

        assertEquals(200, response.getStatusCode());

        String resultString = response.thenReturn().getBody().asString();
        // Make sure search results contain counties, organizations, employees, units and functions.
        assertTrue(resultString.contains("employees"));
        assertTrue(resultString.contains("counties"));
        assertTrue(resultString.contains("organizations"));
        assertTrue(resultString.contains("units"));
        assertTrue(resultString.contains("functions"));
    }


    @Test
    @DisplayName("Test that queries to CodeSystems via organization can be made with plain text ")
    void testCodeSystem() {
        String searchTerm = "Maximum";
        String query = "{\"query\": \"{ search (freetext: \\\"@objecttype:organization %7c @objecttype:county " + searchTerm + "\\\"){organizations {name}, functions{name}, units{name}, employees{name}, counties{name}}}\"}";
        Response response = given().log().all()
                .body(query)
                .when()
                .post(RestAssured.rootPath).thenReturn();

        assertEquals(200, response.getStatusCode());

        String resultString = response.thenReturn().getBody().asString();
        assertTrue(resultString.contains(searchTerm));

    }




}
