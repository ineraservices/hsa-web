package se.inera.jaxrs.client;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggingFilter implements ClientRequestFilter, ClientResponseFilter {
    private static final Logger LOG = Logger.getLogger(LoggingFilter.class.getName());
    private static final int MAXBODYSIZE = 1024;

    public LoggingFilter() {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$-7s] %5$s %n");
    }

    @Override
    public void filter(ClientRequestContext request) throws IOException {
        LOG.log(Level.INFO, ">> " + request.getMethod() + " " + request.getHeaders().toString() + " " + request.getUri());
        if(request.hasEntity()) {
            LOG.log(Level.INFO, ">> " + request.getEntity().toString());
        } else {
            LOG.log(Level.INFO, ">> " + "No content");
        }
    }

    @Override
    public void filter(ClientRequestContext request,
                       ClientResponseContext response) throws IOException {
        if(response.hasEntity()) {
            StringBuilder logMsg = new StringBuilder();
            response.setEntityStream(logMessageBodyInputStream(logMsg, response.getEntityStream()));
            LOG.log(Level.INFO, "<< " + logMsg.toString());
        } else {
            LOG.log(Level.INFO, "<< " + "No content");
        }
    }

    protected InputStream logMessageBodyInputStream(final StringBuilder logMessage, final InputStream is) throws IOException {
        InputStream resultEntityStream = is;
        if (!is.markSupported()) {
            resultEntityStream = new BufferedInputStream(is);
        }
        resultEntityStream.mark(MAXBODYSIZE + 1);
        final byte[] entityBytes = new byte[MAXBODYSIZE + 1];
        final int size = resultEntityStream.read(entityBytes);
        logMessage.append(new String(entityBytes, 0, Math.min(size, MAXBODYSIZE)));
        if (size > MAXBODYSIZE) {
            logMessage.append(" [additional data truncated]");
        }
        resultEntityStream.reset();
        return resultEntityStream;
    }
}
